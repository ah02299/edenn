## Vanilla PyTorch implementation of the training code for the  ###################################################
#                                                                                                                                                                                                                                 #
# To run using multi-GPU setups use DDP (example command bellow)                                                                                                                                                                  #
# torchrun --standalone --nnodes 1 --nproc_per_node=2 /mnt/fast/nobackup/users/ah0097/fipc_task_aware_compression/train_task_aware_AEs_DDP.py --datasets 5 --wandb 1 --parall 2 --h_dim 10 --models 2 --folder_name (Folder_name) #                               #
#                                                                                                                                                               #       
# or                                                                                                                                                            #
#                                                                                                                                                                #
# /opt/conda/bin/python -m torch.distributed.run --standalone --nnodes 1 --nproc_per_node=2 train_task_aware_AEs_DDP.py --datasets 5 --distributed 2 --wandb 1  #
#                                                                                                                                                               #
#################################################################################################################################################################
from torch import nn
import torch
from torch.utils.data import DataLoader, ConcatDataset
import argparse
import tqdm
import wandb
import numpy as np
from train_utils import save_checkpoint, sigterm_handler
import signal
import os 
import traceback
import random
import torch.distributed as dist
import h5py 
from edenn.datasets.dsec import Sequence, SequenceEDENN
from edenn.models.edenn_plus import Udenn, Udenn_n, Udenn_nDense, Udenn_nopart
from pathlib import Path
import imageio
import math 

INITIALISED = False # Don't run ddp init twice 

# Download freimage library, it isn't downloaded in the docker image for some reason
imageio.plugins.freeimage.download()


# TODO:
#   - Find metric for flow?
#   - Change the naive 80/20 split?


# ==================================================================================================
# Trainer function
def train_model(args, model_id, dataset_id):
    """This function contains the training code corresponding to the edenn++ project
    
    Parameters
    ----------
    model_id:        which of the created models to train -> 0: U-denn
    dataset_id:      wich of the available datasets to use for training -> 0: DSEC-flow (no aug) / 
    """

    # Initialise sigterm handler for condor (to check if job is about to be evicted)
    signal.signal(signal.SIGTERM, sigterm_handler)

    # Set Seeds so all processes are in sync
    if args.parall in [2]: # If using DDP set the seeds for the shuffling of datasets (although I was planning on feeding subsequences in order)
        torch.cuda.manual_seed_all(args.seed)
        np.random.seed(args.seed)
        random.seed(args.seed)

    # ---------------------------------------------------------------------------------------------
    # Dataset
    if dataset_id == 0:  # DSEC-flow
        dataset_name = "DSEC-flow"
        # DSEC root in computing servers.
        root_folder = args.root_folder
        # Names of the sequences that do have GT flow
        seq_names = os.listdir(os.path.join(root_folder, "train_optical_flow"))
        # List full of individual sequence dataset objects. Iterate through sequence names creating datasets.
        seq_datasets = []
        for sample in seq_names:
            print(sample)
            # Take a smaller spatial crop to get more granularity in the temporal dimension. (same crop as in E-RAFT)
            seq_datasets.append(SequenceEDENN(Path(root_folder),sample,num_bins=args.n_bins, crop_window=(288, 384)))

        # Naive 80/20 (change this I guess)
        print(len(seq_datasets[0]))
        train_data = ConcatDataset(seq_datasets[:14])
        val_data = ConcatDataset(seq_datasets[14:])

        print(f"train sequences: {seq_names[:14]}")
        print(f"validation sequences: {seq_names[14:]}")

    else:
        raise Exception(f"Unknown dataset id {dataset_id}")


    # ---------------------------------------------------------------------------------------------  
    # Model
    if int(model_id) == 0: # U-Net like edenn arch for dense prediction. (with new equivalent re-scale value only)
        exp = "Udenn_nr_only"
        if dataset_id in  [0]: # DSEC dataset
            model = Udenn_n(in_channels = 1, out_channels = 2, n_bins= args.n_bins) 
    elif int(model_id) == 1: # U-Net like edenn arch for dense prediction. (with new equivalent re-scale value and new "memory efficient" dense mode)
        exp = "Udenn_nr_nd"
        if dataset_id in  [0]: # DSEC dataset
            model = Udenn_nDense(in_channels = 1, out_channels = 2, n_bins= args.n_bins) 
        
    elif int(model_id) == 2: # U-Net like edenn arch for dense prediction. (with new equivalent re-scale value and no partial conv)
        exp = "Udenn_nr_np"
        if dataset_id in  [0]: # DSEC dataset
            model = Udenn_nopart(in_channels = 1, out_channels = 2, n_bins= args.n_bins)
    else:
        raise Exception(f"Unknown model id code {model_id}")
    
    # ---------------------------------------------------------------------------------------------
    # Set name of experiment and out folder
    folder_name = args.folder_name
    cpt_name = f"{exp}+{dataset_name}={args.lr}.pth"
    cpt_path = os.path.join(args.checkpoint_dir,folder_name,cpt_name)



    # ---------------------------------------------------------------------------------------------
    # CUDA and GPU stuff
    # Check for cuda GPU:
    device = 'cuda' if torch.cuda.is_available() else 'cpu'
    is_master = True

    if torch.cuda.device_count() > 1 and args.parall is not None: # If more than one GPU available use distributed training
        if args.parall in [1]:
            # Send model to device
            model.to(device)
            model = nn.DataParallel(model)
            model = model.module # you need to access the model module there d
            DDP = False
            
        elif args.parall in [2]:
            DDP = True # Using Distributed Data Parallel
            # Get the ID of the GPU running 
            gpu_id = int(os.environ["LOCAL_RANK"]) # Get your gpu ID via environment variable (only with torchrun)
            is_master = gpu_id == 0 # This is used to ensure that logging/saving/loading operations are only performed on the main gpu 
            device = torch.device(gpu_id) # Set device to current process GPU id

            # initialize torch ddp 
            dist.init_process_group(backend="nccl")
              
            torch.cuda.set_device(device)

            # Send model to device (idk if needed but it works with it so I'll keep it)
            model.to(device)
            # Wrap the model in the parallel thingy 
            model = nn.parallel.DistributedDataParallel(model, device_ids=[device], output_device=device)
            model = model.module 

            # Synch all processes after initialising DDP
            dist.barrier()
    else:
        DDP = False
        # Send model to device
        model.to(device)

    # ---------------------------------------------------------------------------------------------
    # Losses and Metrics:
    if dataset_id in [0]: # DSEC-flow
        task_loss = nn.L1Loss() # E-Raft uses l1 loss between predicted and GT flow 
    else: 
        raise Exception(f"Unknown dataset id code {dataset_id}")

    
    # Apparently you also have to send the metric to device
    #train_metric.to(device)
    #val_metric.to(device)


    # ---------------------------------------------------------------------------------------------
    # Set up optimizers
    n_gpus = torch.cuda.device_count()
    if DDP:
        lr = args.lr #* math.sqrt(n_gpus)
        task_optimizer = torch.optim.Adam(model.parameters(), lr=lr)
    else:
        lr = args.lr 
        task_optimizer = torch.optim.Adam(model.parameters(), lr=lr)

    
    # ---------------------------------------------------------------------------------------------
    # Check for available checkpoints
    if os.path.exists(cpt_path): # 1. Checkpoint available
        if DDP is False or (DDP is True and is_master): # Only print on master GPU
            print(f"Checkpoint found, loading")

        # Load checkpoint file 
        checkpoint = torch.load(cpt_path)
        start_epoch = checkpoint['current_epoch']
        wandb_id = checkpoint['wandb_id']

        # Load Model and optimizers checkpoints
        model.load_state_dict(checkpoint['model_checkpoint'])
        task_optimizer.load_state_dict(checkpoint['model_optim'])

        if DDP is False or (DDP is True and is_master): # Only print on master GPU
            print(f"Checkpoint loaded from: {cpt_path}")
    else: # 2. No Checkpoint avaliable
        start_epoch = 0
        if DDP is False or (DDP is True and is_master): # Only print and generate wandbid on master GPU 
            wandb_id = wandb.util.generate_id() # Generate a new id for the run
            print("No checkpoint found, starting from scratch")


    # ---------------------------------------------------------------------------------------------
    # Initialize DataLoaders for Training:
    if DDP: # When using DDP 
        # Set up distributed samplers for both the train and val splits
        train_sampler = torch.utils.data.distributed.DistributedSampler(train_data)
        val_sampler = torch.utils.data.distributed.DistributedSampler(val_data)

        train_loader = DataLoader(train_data, batch_size=args.batch_size, num_workers=10, shuffle= False, pin_memory = True, sampler = train_sampler)
        print(len(train_loader))
        val_loader = DataLoader(val_data, batch_size=args.batch_size, num_workers=10, shuffle = False, pin_memory = True, sampler = val_sampler)
    else:
        train_loader = DataLoader(train_data, batch_size=args.batch_size, num_workers=10, shuffle= True)
        val_loader = DataLoader(val_data, batch_size=args.batch_size, num_workers=10, shuffle = True)


    # ---------------------------------------------------------------------------------------------
    # Track model performance and gradients with wandb:
    if args.wandb == "1" and (DDP is False or (DDP is True and is_master)): # Only track experiment in master gpu
        wandb.init(
            project="edenn++",
            name = f"({folder_name})+{exp}+{dataset_name}={args.lr}",
            id = wandb_id,
            resume = "allow",
            group = folder_name,
            config={
            "learning_rate": lr,
            "architecture": exp,
            "dataset": dataset_name,
            "epochs": args.epochs,
            }
        )
        #wandb.watch(model, train_metric, log="all")

    # Synchronize processes after initialising wandb
    if DDP:
        dist.barrier()

    # ---------------------------------------------------------------------------------------------
    # training

    if DDP is False or (DDP is True and is_master): # Only print on master GPU
        print("-----------------------------------------------------")
        print(f"Running experiment: {folder_name}")
        print(f"Checkpoint folder: {os.path.join(args.checkpoint_dir,folder_name)}")
        print(f"Device: {device}")
        print(f"Number of GPUs: {n_gpus}")
        print(f"Learning rate: {lr}")
        print(f"Batch Size: {args.batch_size}")
        print(f"Batch accumulation: {args.accumulate_grads}")
        print(f"Total number of epochs: {args.epochs}")
        print(f"Start epoch: {start_epoch}")
        print("-----------------------------------------------------")
    # Training Loop
    min_val_loss = 0 # Keep track of the min validation loss score for checkpointing
    torch.autograd.set_detect_anomaly(True)
    try: # Try-Except to catch Sigterm 
        for epoch in range(start_epoch, args.epochs):
            val_loss_epoch = [] # Keep track of the validation loss per iteration to calculate the mean val loss

# ============================================================================================== # 
                                ### TRAINING LOOP ###

            if DDP:
                train_sampler.set_epoch(epoch)

            loop = tqdm.tqdm(train_loader, disable=(DDP is True and not is_master))
            loop.set_description(f"Epoch [{epoch}/{args.epochs}]")


            model.train() # Turn mode into train mode
            for i, batch in enumerate(loop):   
             
                if dataset_id in [0]: # DSEC-flow
                    # Get images and labels and send them to device
                    events, (flow, valid2D) = batch["events"].to(device, non_blocking=True), batch["flow"]
                    flow = flow.to(device, non_blocking=True)
                    valid2D = valid2D.float().to(device, non_blocking=True) # Convert from Boolean tensor to number tensor

                

                # # print(torch.sum(valid2D))
                # print(valid2D.shape)
                # print(events.shape)
                # print(flow.shape)

                # For E-RAFT representation, unsqueeze the channel dimension
                events = events.unsqueeze(1)
                valid2D = valid2D.unsqueeze(3)
                flow = flow.unsqueeze(1)
                
                #Forward pass
                if model_id in[0, 1, 2]: # If U-denn model
                    # Create binary mask of where events are present or not
                    input_mask = torch.heaviside(torch.abs(events), torch.tensor(0, dtype= events.dtype))
                    out, out_mask = model(events,input_mask)

                

                if dataset_id in [0] and args.intermediate: # DSEC-flow with all timesteps
                    # Repeat GT and mask as many times as timesteps available so that intermediate flow predictons
                    # Only calculate loss on those pixels where the flow is valid
                    # are also used for supervision
                    valid2D = valid2D.repeat(1,args.n_bins,1,1,2)
                    flow = flow.repeat(1,args.n_bins,1,1,1) * valid2D
                    out = out.permute(0,2,3,4,1) * valid2D # permute channel dim of output 

                if dataset_id in [0] and not args.intermediate: # DSEC-flow with all timesteps
                    # Only calculate loss on those pixels where the flow is valid
                    # are also used for supervision
                    flow = (flow * valid2D).squeeze(1)
                    out = (out.permute(0,2,3,4,1) * valid2D)[:,-1,:,:,:] # permute channel dim of output and only get last timestep as the final pred

                # print(flow)
                # print(out.shape)
                # Calculate loss
                t_error = task_loss(out, flow) # task loss

                if args.accumulate_grads !=  None and args.accumulate_grads != 0:
                    t_error = t_error / args.accumulate_grads # normalize loss to account for batch accumulation

                # backward
                t_error.backward()

                # If we don't accumulate gradients then just update
                if args.accumulate_grads == None or args.accumulate_grads == 0:
                    # update parameters
                    task_optimizer.step()
                    # clear gradients
                    task_optimizer.zero_grad()
                elif ((i + 1) % args.accumulate_grads == 0) or (i + 1 == len(train_loader)): # Accumulate n batches of gradients before optimizing 
                    print("updating now")
                    # update parameters
                    task_optimizer.step()
                    # clear gradients
                    task_optimizer.zero_grad()


                if DDP is True: # When using DDP, each instance of the model in each GPU will score different losses/metrics. We report on the avg among GPUs
                    # These operations must be performed in all gpus (it didn't work if not )
                    # Sum losses and metrics among gpus
                    dist.all_reduce(t_error)

                    # Calculate AVG loss/metric for all gpus 
                    t_error = t_error.item() / n_gpus 

                else:
                    t_error = t_error.item() 

                
                # Log wandb loss and metrics
                if args.wandb == "1" and (DDP is False or (DDP is True and is_master)): # Only log on master GPU
                    # upscale loss again if training with batch acc:
                    if args.accumulate_grads is not None:
                        wandb.log({'Epoch': epoch, 'TRAIN_task_loss': t_error * args.accumulate_grads})
                    else:
                        wandb.log({'Epoch': epoch, 'TRAIN_task_loss': t_error})
                
                # Synchronize processes after logging 
                if DDP:
                    dist.barrier()

                # Update batch loss in tqdm bar
                if DDP is False or (DDP is True and is_master): # Only print on master GPU
                    loop.set_postfix(task_loss = t_error) 
                
# ============================================================================================== # 
                                ### VALIDATION LOOP ###
                    


            if DDP:
                val_sampler.set_epoch(epoch)
                
            vloop = tqdm.tqdm(val_loader, disable= (DDP is True and not is_master))
            vloop.set_description(f"Validation step [{epoch}/{args.epochs}]")

            model.eval() # Turn mode into eval mode
            with torch.no_grad():
                for i, batch in enumerate(vloop):
                    if dataset_id in [0]: # DSEC-flow
                        # Get events and labels and send them to device
                        events, (flow, valid2D) = batch["events"].to(device, non_blocking=True), batch["flow"]
                        flow = flow.to(device, non_blocking=True)
                        valid2D = valid2D.float().to(device, non_blocking=True) # Convert from Boolean tensor to number tensor


                    # For E-RAFT representation, unsqueeze the channel dimension
                    events = events.unsqueeze(1)
                    valid2D = valid2D.unsqueeze(3)
                    flow = flow.unsqueeze(1)
                    
                    #Forward pass
                    if model_id in[0,1,2]: # If U-denn
                        input_mask = torch.heaviside(torch.abs(events), torch.tensor(0, dtype= events.dtype))
                        out, out_mask = model(events,input_mask)

                    if dataset_id in [0] and args.intermediate: # DSEC-flow
                        # Repeat GT and mask as many times as timesteps available so that intermediate flow predictons
                        # Only calculate loss on those pixels where the flow is valid
                        # are also used for supervision
                        valid2D = valid2D.repeat(1,args.n_bins,1,1,2)
                        flow = flow.repeat(1,args.n_bins,1,1,1) * valid2D
                        out = out.permute(0,2,3,4,1) * valid2D # permute channel dim of output 
                    if dataset_id in [0] and not args.intermediate: # DSEC-flow with all timesteps
                        # Only calculate loss on those pixels where the flow is valid
                        # are also used for supervision
                        flow = (flow * valid2D).squeeze(1)
                        out = (out.permute(0,2,3,4,1) * valid2D)[:,-1,:,:,:] # permute channel dim of output and only get last timestep as the final pred


                    # Calculate loss
                    t_error = task_loss(out, flow) # task loss 
                    if DDP is True: # When using DDP, each copy of the model in each GPU will score different losses/metrics. We report on the avg among GPUs
                        dist.all_reduce(t_error)
                        # Calculate AVG loss/metric for all gpus 
                        t_error = t_error.item() / n_gpus 
                    else:
                        t_error = t_error.item() 

                    # Append batch loss to epoch validation list to calculate the mean 
                    val_loss_epoch.append(t_error)

                    if args.wandb == "1" and (DDP is False or (DDP is True and is_master)): # Only print on master GPU
                        # Log wandb loss and metrics
                        wandb.log({'VAL_task_loss': t_error})

                    # wait for all processes to synch before updating the loss in the tqdm bar
                    if DDP:
                        dist.barrier()

                    # Update batch loss in tqdm bar
                    if DDP is False or (DDP is True and is_master): # Only print on master GPU
                        vloop.set_postfix(task_loss = t_error) 
            

            # Checkpointing only the best models
            current_val_loss = np.mean(np.array(val_loss_epoch))
            print(f"current val loss: {current_val_loss}")
            print(f"min val loss: {min_val_loss}")

            if epoch == 0: # For the first epoch 
                min_val_loss = current_val_loss
                print("Storing checkpoint...")
                if model_id !=  0:
                    save_checkpoint(cpt_name, os.path.join(args.checkpoint_dir,folder_name), model.state_dict(), task_optimizer.state_dict(), wandb_id, epoch + 1)
                else:
                    save_checkpoint(cpt_name, os.path.join(args.checkpoint_dir,folder_name), model.state_dict(), task_optimizer.state_dict(), wandb_id, epoch + 1)
            elif epoch != 0 and (current_val_loss < min_val_loss):
                min_val_loss = current_val_loss 
                if DDP is False or (DDP is True and is_master): # Only save on master GPU

                    print("Storing checkpoint...")
                    if model_id !=  0:
                        save_checkpoint(cpt_name, os.path.join(args.checkpoint_dir,folder_name), model.state_dict(), task_optimizer.state_dict(), wandb_id, epoch + 1)
                    else:
                        save_checkpoint(cpt_name, os.path.join(args.checkpoint_dir,folder_name), model.state_dict(), task_optimizer.state_dict(), wandb_id, epoch + 1)
            # Synchronize processes after saving
            if DDP:
                dist.barrier()
    
    except Exception: # Catch sigterm
        print(traceback.format_exc())
        wandb.finish()
        if DDP is False or (DDP is True and is_master): # Only save on master GPU
            if model_id != 0:
                save_checkpoint(cpt_name, os.path.join(args.checkpoint_dir,folder_name), model.state_dict(), task_optimizer.state_dict(), wandb_id, epoch + 1)
            else:
                save_checkpoint(cpt_name, os.path.join(args.checkpoint_dir,folder_name), model.state_dict(), task_optimizer.state_dict(), wandb_id, epoch + 1)
        # Synchronize processes after saving
        if DDP:
            dist.barrier()


        
                   

                

            

           
# ==================================================================================================
# Main method
if __name__ == "__main__":
    torch.set_printoptions(threshold=10_000)
    # Command line args
    parser = argparse.ArgumentParser(description='Trainning code for the edenn++ project.')
    parser.add_argument('--lr', type=float, 
                        default=3e-4, help='Learning rate')
    parser.add_argument('--n_bins', type=int, 
                        default=15, help=   'Number of time-steps to both divide each voxelized sequence'
                                            'and to consider in dense mode')
    parser.add_argument('--epochs', type=int,
                         default=6, help='Number of epochs to train for')
    parser.add_argument('--datasets', nargs='+', help=  'IDs for the datasets to run on as a space seperated list:'
                                                        '0 = DSEC (no augmentations)',
                        default=[0])
    parser.add_argument('--models', nargs='+', help='Which compression models should we run as a space separated list:'
                                                    '0 = Udenn',
                        default=[0])
    parser.add_argument('--wandb', help='1 if you want this experiment tracked with weigths and biases and 2 if you do not',
                        default="1", type=str)
    parser.add_argument('--checkpoint_dir', help='root directory to store the checkpoints',
                        default="/mnt/fast/nobackup/users/ah02299/edenn_checkpoints/", type=str)
    parser.add_argument('--parall', help='Which parallelisation mechanism to use if multi-GPU is available'
                                                    '1=DataParallel, 2=DDP',
                        default=None, type=int)
    parser.add_argument('--batch_size', help='How many subsequences to include per_batch',
                        default=1, type=int)
    parser.add_argument('--seed', help='Set seed forUdenn ddp or what',
                        default=100, type=int)
    parser.add_argument('--folder_name', help='root folder defining where to save model checkpoints',
                        default="edenn++", type=str)
    parser.add_argument('--accumulate_grads', help= 'How many batches to accumulate gradients for, if None, backward pass will be'
                                                    'calculated each batch',
                        default=6 , type=int)
    parser.add_argument('--intermediate', help= 'calculate L1 loss on all timesteps',
                        default=False , type=bool)
    
    parser.add_argument('--root_folder', help= 'dataset\'s root folder (to train both in weka and storenext)',
                        default="/mnt/fast/datasets/mixedmode/DSEC/" , type=str)
    

    
    args = parser.parse_args()


    for dataset_id in args.datasets:
        dataset_id = int(dataset_id)
        for model_id in args.models:
            model_id = int(model_id)
            model = train_model(args, model_id, dataset_id)

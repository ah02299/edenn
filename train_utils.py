# Utility funtions for checkpointing and running on the university servers
import os 
import torch
import sys
import signal
import numpy as np
import torch 


# ----------------------------------------------------------------------------------------------
def save_checkpoint(model_name: str, folder_path: str, model_checkpoint: torch.nn.Module.state_dict, task_optim, wandb_id, current_epoch):
    """This function saves a checkpoint of the training process to be loaded later. The python dictionary containing the necessary 
        info is saved to /folder_path/model_name.
        Parameters
        ----------
        model_checkpoint:       model's state dictionary
        optimizer_checkpoint:   optimizers's state dictionary
        wandb_id:               id to restart the same run if needed
        current_epoch:          current training epoch
    """
    print("--Saving checkpoint--")
    # Create the checkpoint dictionary
    checkpoint = {
        'model_checkpoint': model_checkpoint,
        'model_optim': task_optim,
        'wandb_id': wandb_id,
        'current_epoch': current_epoch,
    }
    # Save to checkpoint file, use different name for final checkpoint #
    if not os.path.exists(folder_path + "/"):
        os.makedirs(folder_path+"/")
    torch.save(obj=checkpoint, f=os.path.join(folder_path,model_name))
    print("--Checkpoint saved--")


# ---------------------------------------------------------------------------------------------
# For HTCondor
def sigterm_handler(signal,frame):
    """helper function that catches the sigterm signal sent to a Condor job when it is about to be evicted.
        It is normaly used to let the system know it has to save a model checkpoint.
    """
    # catch sigterm and raise system exit 
    print("Sigterm caught!")
    sys.exit(0)
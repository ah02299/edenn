## Vanilla PyTorch implementation of the training code for the  ###################################################
#                                                                                                                                                                                                                                 #
# To run using multi-GPU setups use DDP (example command bellow)                                                                                                                                                                  #
# torchrun --standalone --nnodes 1 --nproc_per_node=2 /mnt/fast/nobackup/users/ah0097/fipc_task_aware_compression/train_task_aware_AEs_DDP.py --datasets 5 --wandb 1 --parall 2 --h_dim 10 --models 2 --folder_name (Folder_name) #                               #
#                                                                                                                                                               #       
# or                                                                                                                                                            #
#                                                                                                                                                                #
# /opt/conda/bin/python -m torch.distributed.run --standalone --nnodes 1 --nproc_per_node=2 train_task_aware_AEs_DDP.py --datasets 5 --distributed 2 --wandb 1  #
#                                                                                                                                                               #
#################################################################################################################################################################
from torch import nn
import torch
from torch.utils.data import DataLoader, ConcatDataset
import argparse
import wandb
import numpy as np
from train_utils import save_checkpoint, sigterm_handler
import signal
import os 
import traceback
import random
import torch.distributed as dist
import h5py 
from edenn.datasets.dsec import Sequence, SequenceEDENN
from edenn.models.edenn_plus import Udenn, Udenn_n, Udenn_nDense, Udenn_npl
from pathlib import Path
import imageio
import math 
import lightning.pytorch as pl
from lightning.pytorch.callbacks import ModelCheckpoint, LearningRateMonitor
from lightning.pytorch import seed_everything

INITIALISED = False # Don't run ddp init twice 

# Download freimage library, it isn't downloaded in the docker image for some reason
imageio.plugins.freeimage.download()


# TODO:
#   - Find metric for flow?
#   - Change the naive 80/20 split?




# ==================================================================================================
# Trainer function
def train_model(args, model_id, dataset_id):
    """This function contains the training code corresponding to the edenn++ project
    
    Parameters
    ----------
    model_id:        which of the created models to train -> 0: U-denn
    dataset_id:      wich of the available datasets to use for training -> 0: DSEC-flow (no aug) / 
    """

    # ---------------------------------------------------------------------------------------------
    # Add seed for reproducibility
    seed = args.seed
    seed_everything(seed, workers=True)


    # ---------------------------------------------------------------------------------------------
    # Dataset
    if dataset_id == 0:  # DSEC-flow
        dataset_name = "DSEC-flow"
        # DSEC root in computing servers.
        root_folder = args.root_folder
        # Names of the sequences that do have GT flow
        #seq_names = os.listdir(os.path.join(root_folder, "train_optical_flow"))
        seq_names = ["zurich_city_10_a","zurich_city_11_a","zurich_city_02_d","zurich_city_11_b",
                        "zurich_city_10_b", 
                        "zurich_city_05_b",
                        "thun_00_a",
                        "zurich_city_11_c",
                        "zurich_city_08_a",
                        "zurich_city_09_a",
                        "zurich_city_03_a",
                        "zurich_city_02_a",
                        "zurich_city_01_a",
                        "zurich_city_05_a",
                        "zurich_city_02_e",
                        "zurich_city_06_a",
                        "zurich_city_07_a"]
        # List full of individual sequence dataset objects. Iterate through sequence names creating datasets.
        seq_datasets = []
        for sample in seq_names:
            print(sample)
            # Take a smaller spatial crop to get more granularity in the temporal dimension. (same crop as in E-RAFT)
            seq_datasets.append(SequenceEDENN(Path(root_folder),sample,num_bins=args.n_bins, crop_window=(288, 384)))

        # Naive 80/20 (change this I guess)
        train_data = ConcatDataset(seq_datasets[:1]) #14
        val_data = ConcatDataset(seq_datasets[:1])

        print(f"train: {seq_names[:14]}")
        print(f"val: {seq_names[14:]}")

        train_loader = DataLoader(train_data, batch_size=args.batch_size, num_workers=5, shuffle= True, pin_memory = True)
        val_loader = DataLoader(val_data, batch_size=args.batch_size, num_workers=5, shuffle = True, pin_memory = True)

    else:
        raise Exception(f"Unknown dataset id {dataset_id}")



    # ---------------------------------------------------------------------------------------------  
    # Model
    if int(model_id) == 0: # U-Net like edenn arch for dense prediction. (with new equivalent re-scale value only)
        exp = "Udenn_nr_only"
        if dataset_id in  [0]: # DSEC dataset
            model = Udenn_npl(in_channels = 1, out_channels = 2, n_bins= args.n_bins, lr = args.lr) 
    elif int(model_id) == 1: # U-Net like edenn arch for dense prediction. (with new equivalent re-scale value and new "memory efficient" dense mode)
        exp = "Udenn_nr_nd"
        if dataset_id in  [0]: # DSEC dataset
            model = Udenn_nDense(in_channels = 1, out_channels = 2, n_bins= args.n_bins, lr = args.lr) 
    else:
        raise Exception(f"Unknown model id code {model_id}")
    


    # ---------------------------------------------------------------------------------------------
    # Set name of experiment and out folder for checkpointing
    folder_name = args.folder_name
    cpt_name = f"{exp}+{dataset_name}={args.lr}"
    cpt_name_full = cpt_name + ".ckpt"
    cpt_path = os.path.join(args.checkpoint_dir,folder_name,cpt_name)
    cpt_path_full = os.path.join(args.checkpoint_dir,folder_name,cpt_name_full)
    print(f"checkpoint path-> {cpt_path_full} exists? ({os.path.exists(cpt_path_full)})")

    # Check for available checkpoints
    if os.path.exists(cpt_path_full): # if checkpoint available, load wandb id to resume run
        print(f"Checkpoint found, loading wandb")
        cpt = torch.load(cpt_path_full)
        wandb_id = cpt["wandb_run_id"]



    # ---------------------------------------------------------------------------------------------
    # Track model performance and gradients with wandb:
    if args.wandb == "1": 
        if os.path.exists(cpt_path_full): # If checkpoint available, re-run 
            wandb_logger = pl.loggers.wandb.WandbLogger(project="edenn++", name= f"({folder_name})+{exp}+{dataset_name}={args.lr}", id =wandb_id)
        else: 
            wandb_logger = pl.loggers.wandb.WandbLogger(project="edenn++", name= f"({folder_name})+{exp}+{dataset_name}={args.lr}")
        # Add wandb id to model to 
        model.add_wandb_id(wandb_logger.version)
        print(f"model wand id: {model.wandb_id}")



    # ---------------------------------------------------------------------------------------------
    # Configure lightning callbacks
    checkpointCall = ModelCheckpoint(
        dirpath=args.checkpoint_dir + folder_name,
        filename=cpt_name,
        monitor='VAL_task_loss',
        mode='min')
            #,every_n_train_steps = 2)
        
    lr_monitor = LearningRateMonitor(logging_interval='step')



    # ---------------------------------------------------------------------------------------------
    # Trainer
    # set trainer variables 
    device = 'cuda' if torch.cuda.is_available() else 'cpu'
    n_gpus = torch.cuda.device_count()
    if n_gpus > 1: # If multiple gpus, adjust accumulate grads
        accumulate_grads = int(args.accumulate_grads/n_gpus) if args.accumulate_grads > 1 else 1
    else: # If no multiple gpus just accumulate whatever grads I want
        accumulate_grads = int(args.accumulate_grads) if args.accumulate_grads > 1 else 1

    if os.path.exists(cpt_path_full): # if checkpoint available, load run
        trainer = pl.Trainer(overfit_batches = 1, accumulate_grad_batches = accumulate_grads, strategy = "ddp" if args.gpus > 1 else "auto", max_epochs = args.epochs, resume_from_checkpoint =cpt_path_full, accelerator = "gpu" if device is "cuda" else "cpu", devices =args.gpus, num_nodes = 1, callbacks = [checkpointCall, lr_monitor],logger = wandb_logger, log_every_n_steps = 1)#, overfit_batches = 1)
    else:
        trainer = pl.Trainer(overfit_batches = 1, accumulate_grad_batches = accumulate_grads, strategy = "ddp" if args.gpus > 1 else "auto", max_epochs = args.epochs, accelerator = "gpu" if device is "cuda" else "cpu", devices =args.gpus, num_nodes = 1, callbacks = [checkpointCall, lr_monitor],logger = wandb_logger, log_every_n_steps= 1)#, overfit_batches= 1)
    
    # Sigterm handler for condor
    # ---------------------------------------------------------------------------------------------
    # For HTCondor
    def sigterm_handler(signal, frame):
        """helper function that catches the sigterm signal sent to a Condor job when it is about to be evicted.
            It is normaly used to let the system know it has to save a model checkpoint.
        """
        # catch sigterm and raise system exit 
        print("Received SIGTERM signal. Saving checkpoint...")
        trainer.save_checkpoint(cpt_path)

    signal.signal(signal.SIGTERM, sigterm_handler)
    
    
    # ---------------------------------------------------------------------------------------------
    # Trainer.fit   
    print("-----------------------------------------------------")
    print(f"Running experiment: {folder_name}")
    print(f"Checkpoint folder: {os.path.join(args.checkpoint_dir,folder_name)}")
    print(f"Device: {device}")
    print(f"Number of GPUs: {n_gpus}")
    print(f"Learning rate: {args.lr}")
    print(f"Batch Size: {args.batch_size}")
    print(f"Batch accumulation: {args.accumulate_grads}")
    print(f"Total number of epochs: {args.epochs}")
    print("-----------------------------------------------------")
    trainer.fit(model, train_loader, val_loader)



        
                   

                

            

           
# ==================================================================================================
# Main method
if __name__ == "__main__":
    torch.set_printoptions(threshold=10_000)
    # Command line args
    parser = argparse.ArgumentParser(description='Trainning code for the edenn++ project.')
    parser.add_argument('--lr', type=float, 
                        default=3e-4, help='Learning rate')
    parser.add_argument('--n_bins', type=int, 
                        default=15, help=   'Number of time-steps to both divide each voxelized sequence'
                                            'and to consider in dense mode')
    parser.add_argument('--epochs', type=int,
                         default=6, help='Number of epochs to train for')
    parser.add_argument('--datasets', nargs='+', help=  'IDs for the datasets to run on as a space seperated list:'
                                                        '0 = DSEC (no augmentations)',
                        default=[0])
    parser.add_argument('--models', nargs='+', help='Which compression models should we run as a space separated list:'
                                                    '0 = Udenn',
                        default=[0])
    parser.add_argument('--wandb', help='1 if you want this experiment tracked with weigths and biases and 2 if you do not',
                        default="1", type=str)
    parser.add_argument('--checkpoint_dir', help='root directory to store the checkpoints',
                        default="/mnt/fast/nobackup/users/ah02299/edenn_checkpoints/", type=str)
    parser.add_argument('--parall', help='Which parallelisation mechanism to use if multi-GPU is available'
                                                    '1=DataParallel, 2=DDP',
                        default=None, type=int)
    parser.add_argument('--batch_size', help='How many subsequences to include per_batch',
                        default=1, type=int)
    parser.add_argument('--seed', help='Set seed forUdenn ddp or what',
                        default=100, type=int)
    parser.add_argument('--folder_name', help='root folder defining where to save model checkpoints',
                        default="edenn++", type=str)
    parser.add_argument('--accumulate_grads', help= 'How many batches to accumulate gradients for, if None, backward pass will be'
                                                    'calculated each batch',
                        default=6 , type=int)
    parser.add_argument('--intermediate', help= 'calculate L1 loss on all timesteps',
                        default=False , type=bool)
    
    parser.add_argument('--root_folder', help= 'dataset\'s root folder (to train both in weka and storenext)',
                        default="/mnt/fast/datasets/mixedmode/DSEC/" , type=str)
    parser.add_argument('--gpus', help= 'how many gpus to use',
                        default=1 , type=int)
    

    
    args = parser.parse_args()


    for dataset_id in args.datasets:
        dataset_id = int(dataset_id)
        for model_id in args.models:
            model_id = int(model_id)
            model = train_model(args, model_id, dataset_id)
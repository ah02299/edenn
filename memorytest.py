import re
import numpy as np
import matplotlib.pyplot as plt
import torch
import torch.nn.functional as F
from edenn.models.utils.decay3dx import Decay3Dx2, partialDecay3Dx, Decay3Dxloop,Decay3Dx2new, Decay3Dx2newdense, Decay3dPartialC # re-Implementation

import time

# class n_to_n(torch.nn.Module):
#     def __init__(self):
#         super().__init__()
#         self.conv1 = Decay3Dx2(2,32,3)
#         self.conv2 = Decay3Dx2(32,64,3)
#         self.conv3 = Decay3Dx2(64,32,3)
#         self.conv4 = Decay3Dx2(32,16,3)
#         self.conv5 = Decay3Dx2(16,5,3)

#     def forward(self, x, mask):
#         x, mask = self.conv1(x, mask)
#         x, mask = self.conv2(x,mask)
#         x, mask = self.conv3(x,mask)
#         x, mask = self.conv4(x,mask)
#         x, mask = self.conv5(x,mask)
#         return x, mask
# ## TESTING EFFICIENT ## 
# print(f"(efficient) GPU mem used at the beginning: \n { torch.cuda.memory_allocated(0)}")
# # Dummy mask and input
# mask = (F.dropout(torch.ones((4,1,6,400,600))) / 2).to("cuda")
# input_tensor = torch.rand((4,2,6,400,600)).to("cuda")
# print(f"(efficient) GPU mem used after variables: \n { torch.cuda.memory_allocated(0) *1e-9}")

# sapenet = n_to_n().to("cuda")
# print(f"(efficient) GPU mem used after initialising model: \n { torch.cuda.memory_allocated(0) * 1e-9}")

# out = sapenet(input_tensor, mask)
# print(f"(efficient) GPU mem used after forward: \n { torch.cuda.memory_allocated(0) * 1e-9}")
# out[0].mean().backward()
# print(f"(efficient) GPU mem used after backward: \n { torch.cuda.memory_allocated(0) * 1e-9}")




# class n_to_n(torch.nn.Module):
#     def __init__(self):
#         super().__init__()
#         self.conv1 = Decay3Dx2new(2,32,3)
#         self.conv2 = Decay3Dx2new(32,64,3)
#         self.conv3 = Decay3Dx2new(64,32,3)
#         self.conv4 = Decay3Dx2new(32,16,3)
#         self.conv5 = Decay3Dx2new(16,5,3)

#     def forward(self, x, mask):
#         x, mask = self.conv1(x, mask)
#         x, mask = self.conv2(x,mask)
#         x, mask = self.conv3(x,mask)
#         x, mask = self.conv4(x,mask)
#         x, mask = self.conv5(x,mask)
#         return x, mask
    

# ## TESTING NEW_RE-WEIGHTING ## 
# print(f"(new_r) GPU mem used at the beginning: \n { torch.cuda.memory_allocated(0)}")
# # Dummy mask and input
# mask = (F.dropout(torch.ones((4,1,15,200,300))) / 2).to("cuda")
# input_tensor = torch.rand((4,2,15,200,300)).to("cuda")
# print(f"(new_r) GPU mem used after variables: \n { torch.cuda.memory_allocated(0) *1e-9}")

# sapenet = n_to_n().to("cuda")
# print(f"(new_r) GPU mem used after initialising model: \n { torch.cuda.memory_allocated(0) * 1e-9}")

# start = time.time()
# out = sapenet(input_tensor, mask)
# end = time.time()
# print(f"(new_r) GPU mem used after forward: \n { torch.cuda.memory_allocated(0) * 1e-9}")
# print(f"(new_r) forward time: \n { end-start}s")
# out[0].mean().backward()
# print(f"(new_r) GPU mem used after backward: \n { torch.cuda.memory_allocated(0) * 1e-9}")







class n_to_n(torch.nn.Module):
    def __init__(self):
        super().__init__()
        self.conv1 = Decay3dPartialC(2,32,3)
        self.conv2 = Decay3dPartialC(32,64,3)
        self.conv3 = Decay3dPartialC(64,32,3)
        self.conv4 = Decay3dPartialC(32,16,3)
        self.conv5 = Decay3dPartialC(16,5,3)

    def forward(self, x, mask):
        x, mask = self.conv1(x)
        x, mask = self.conv2(x,mask)
        x, mask = self.conv3(x,mask)
        x, mask = self.conv4(x,mask)
        x, mask = self.conv5(x,mask)
        return x, mask
    

## TESTING celyn ## 
print(f"(new_r) GPU mem used at the beginning: \n { torch.cuda.memory_allocated(0)}")
# Dummy mask and input
mask = (F.dropout(torch.ones((4,1,15,200,300))) / 2).to("cuda")
input_tensor = torch.rand((4,2,15,200,300)).to("cuda")
print(f"(new_r) GPU mem used after variables: \n { torch.cuda.memory_allocated(0) *1e-9}")

sapenet = n_to_n().to("cuda")
print(f"(new_r) GPU mem used after initialising model: \n { torch.cuda.memory_allocated(0) * 1e-9}")

start = time.time()
out = sapenet(input_tensor, mask)
end = time.time()
print(f"(new_r) GPU mem used after forward: \n { torch.cuda.memory_allocated(0) * 1e-9}")
print(f"(new_r) forward time: \n { end-start}s")
out[0].mean().backward()
print(f"(new_r) GPU mem used after backward: \n { torch.cuda.memory_allocated(0) * 1e-9}")



# class n_to_n(torch.nn.Module):
#     def __init__(self):
#         super().__init__()
#         self.conv1 = Decay3Dx2newdense(2,32,3)
#         self.conv2 = Decay3Dx2newdense(32,64,3)
#         self.conv3 = Decay3Dx2newdense(64,32,3)
#         self.conv4 = Decay3Dx2newdense(32,16,3)
#         self.conv5 = Decay3Dx2newdense(16,5,3)

#     def forward(self, x, mask):
#         x, mask = self.conv1(x, mask)
#         x, mask = self.conv2(x,mask)
#         x, mask = self.conv3(x,mask)
#         x, mask = self.conv4(x,mask)
#         x, mask = self.conv5(x,mask)
#         return x, mask
    

# ## TESTING NEW_RE-WEIGHTING + new dense ## 
# print(f"(new_r + d) GPU mem used at the beginning: \n { torch.cuda.memory_allocated(0)}")
# # Dummy mask and input
# mask = (F.dropout(torch.ones((4,1,15,200,300))) / 2).to("cuda")
# input_tensor = torch.rand((4,2,15,200,300)).to("cuda")
# print(f"(new_r + d) GPU mem used after variables: \n { torch.cuda.memory_allocated(0) *1e-9}")

# sapenet = n_to_n().to("cuda")
# print(f"(new_r + d) GPU mem used after initialising model: \n { torch.cuda.memory_allocated(0) * 1e-9}")

# start = time.time()
# out = sapenet(input_tensor, mask)
# end = time.time()
# print(f"(new_r + d) GPU mem used after forward: \n { torch.cuda.memory_allocated(0) * 1e-9}")
# print(f"(new_r + d) forward time: \n { end-start}s")
# out[0].mean().backward()
# print(f"(new_r + d) GPU mem used after backward: \n { torch.cuda.memory_allocated(0) * 1e-9}")

# # ## TESTING NAIVE ## 
# class n_to_n(torch.nn.Module):
#     def __init__(self):
#         super().__init__()
#         self.conv1 = Decay3Dxloop(2,32,3)
#         self.conv2 = Decay3Dxloop(32,64,3)
#         self.conv3 = Decay3Dxloop(64,32,3)
#         self.conv4 = Decay3Dxloop(32,16,3)
#         self.conv5 = Decay3Dxloop(16,5,3)

#     def forward(self, x, mask):
#         x, mask = self.conv1(x, mask)
#         x, mask = self.conv2(x,mask)
#         x, mask = self.conv3(x,mask)
#         x, mask = self.conv4(x,mask)
#         x, mask = self.conv5(x,mask)
#         return x, mask
# print(f"(naive) GPU mem used at the beginning: \n { torch.cuda.memory_allocated(0)*1e-9}")
# # Dummy mask and input
# mask = (F.dropout(torch.ones((16,1,15,400,600))) / 2).to("cuda:0")
# input_tensor = torch.rand((16,2,15,400,600)).to("cuda")
# print(f"(naive) GPU mem used after variables: \n { torch.cuda.memory_allocated(0) *1e-9}")

# sapenet = n_to_n().to("cuda")
# print(f"(naive) GPU mem used after initialising model: \n { torch.cuda.memory_allocated(0) * 1e-9}")

# out = sapenet(input_tensor, mask)
# print(f"(naive) GPU mem used after forward: \n { torch.cuda.memory_allocated(0) * 1e-9}")
# out[0].mean().backward()
# print(f"(naive) GPU mem used after backward: \n { torch.cuda.memory_allocated(0) * 1e-9}")
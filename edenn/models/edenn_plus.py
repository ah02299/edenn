import math
import collections.abc
from typing import Optional

import torch
import torch.nn as nn

import numpy as np
from .utils.decay3dx import Decay3Dx2, Decay3Dx2test, Decay3Dx2newdense, Decay3Dx2_no_partial, Decay3Dx2new
import torch.nn.functional as F
import lightning.pytorch as pl 



class upconv_agnostic(nn.Module):
    """upscale + convolution that accomodates for skip connections while also being size agnostic.
    
    Parameters
    ----------
    in_channels:    Number of channels to be expected in in the input.
    out_channels:   Number of channels to be outputed.
    n_filters:      Number of spatial filters to have in the decay3dx layer.
    n_bins:         Number of time slices to be expected in the input and throughout the architecture.
    factor:         What factor to increase the spatial dimensions by.
    
    """

    # --------------------------------------------------------------------------------
    def __init__(self, in_channels: int, out_channels: int, n_filters: int, n_bins: int = None, factor: int = 2, layer_type = Decay3Dx2new):
        super().__init__()

        self.decay3dlayer = layer_type(in_channels, out_channels,  3, padding =1, n_bins = n_bins)
        self.upsample = nn.Upsample(scale_factor = (1,factor,factor), mode = "trilinear", align_corners=True) # spatially increase the size by a factor of 2

        self.upsample_mask = nn.Upsample(scale_factor = (1,factor,factor), mode = "nearest")
    
        
    # --------------------------------------------------------------------------------
    def forward(self,x1, x2, mask1, mask2): 

        x1 = self.upsample(x1) # input is (B, C, T, H, W)
        mask1 = self.upsample_mask(mask1)

        diffY = x2.size()[3] - x1.size()[3]
        diffX = x2.size()[4] - x1.size()[4]

        x1 =  F.pad(x1, [diffX // 2, diffX - diffX // 2,
                        diffY // 2, diffY - diffY // 2])
        
        mask1 =  F.pad(mask1, [diffX // 2, diffX - diffX // 2,
                        diffY // 2, diffY - diffY // 2])
        
        

        x = F.relu(torch.cat([x2, x1], dim=1))
        mask =torch.cat([mask2, mask1], dim=1)
        return self.decay3dlayer(x, mask)




# ===============================================================================================
# U-DeNN model (quick and dirty)
class Udenn(nn.Module):
    """Base architecture resembling a conventional Encoder-decoder fully convolutional U-Net (https://github.com/milesial/Pytorch-UNet/tree/master)
    with skip connections, made up of EDENN layers.
    
    Parameters
    ----------
    in_channels:    Number of channels to be expected in in the input.
    out_channels:   Number of channels to be outputed.
    n_bins:         Number of time slices to be expected in the input and throughout the architecture.
    
    """

    # --------------------------------------------------------------------------------
    def __init__(self, in_channels: int, out_channels: int, n_bins: int = None, factor: int = 2):
        super().__init__()

        # Encoder
        self.enc_1 = Decay3Dx2test(in_channels, 64, 3, padding = 1, n_bins = n_bins)
        self.bn1 = nn.BatchNorm3d(64)
        self.enc_2 = Decay3Dx2test(64, 128, 3, padding = 1, n_bins = n_bins )
        self.bn2 = nn.BatchNorm3d(128)
        self.enc_3 = Decay3Dx2test(128, 256, 3, padding = 1, n_bins = n_bins)
        self.bn3 = nn.BatchNorm3d(256)
        self.enc_4 = Decay3Dx2test(256, 512 // factor,  3, padding = 1, n_bins = n_bins) # We concat the channel dim when we do skip connections so divide out_channels by the scalling factor
        self.bn4 = nn.BatchNorm3d(256)

        self.maxpool = nn.MaxPool3d((1,factor,factor)) # spatially reducing the input by a factor of 2.

        # Decoder 
        self.dec_1 = upconv_agnostic(512, 256 // factor, 3, n_bins = n_bins,layer_type=Decay3Dx2test)
        self.bnd1 = nn.BatchNorm3d(256//factor)
        self.dec_2 = upconv_agnostic(256, 128 // factor, 3,  n_bins = n_bins, layer_type=Decay3Dx2test)
        self.bnd2 = nn.BatchNorm3d(128//factor)
        self.dec_3 = upconv_agnostic(128, 64, 3,  n_bins = n_bins, layer_type=Decay3Dx2test)
        self.bnd3 = nn.BatchNorm3d(64)
        self.dec_4 = Decay3Dx2test(64, out_channels, 3, padding =1, n_bins = n_bins)

        self.upsample = nn.Upsample(scale_factor = (1,factor,factor), mode = "trilinear", align_corners=True) 
        self.upsample_mask = nn.Upsample(scale_factor = (1,factor,factor), mode = "nearest")




    # --------------------------------------------------------------------------------
    def forward(self, x, mask):

        # Encoding?
        x1 = self.enc_1(x, mask)

        x2 = self.enc_2(F.relu(self.maxpool(self.bn1(x1[0]))), self.maxpool(x1[1]))

        x3 = self.enc_3(F.relu(self.maxpool(self.bn2(x2[0]))), self.maxpool(x2[1]))

        x4 = self.enc_4(F.relu(self.maxpool(self.bn3(x3[0]))), self.maxpool(x3[1]))

        # Decoding
        x5 = self.dec_1(self.bn4(x4[0]), self.bn3(x3[0]),x4[1], x3[1]) 
        x6 = self.dec_2(self.bnd1(x5[0]), self.bn2(x2[0]), x5[1], x2[1])
        x7 = self.dec_3(self.bnd2(x6[0]), self.bn1(x1[0]), x6[1], x1[1])
        x8 = self.dec_4(self.bnd3(x7[0]),x7[1])

        return x8
    




    








# ===============================================================================================
# U-DeNN model (with new rescalling value)
class Udenn_n(nn.Module):
    """Base architecture resembling a conventional Encoder-decoder fully convolutional U-Net (https://github.com/milesial/Pytorch-UNet/tree/master)
    with skip connections, made up of EDENN layers (with new equivalent ).
    
    Parameters
    ----------
    in_channels:    Number of channels to be expected in in the input.
    out_channels:   Number of channels to be outputed.
    n_bins:         Number of time slices to be expected in the input and throughout the architecture.
    
    """

    # --------------------------------------------------------------------------------
    def __init__(self, in_channels: int, out_channels: int, n_bins: int = None, factor: int = 2):
        super().__init__()

        # Encoder
        self.enc_1 = Decay3Dx2new(in_channels, 64, 3, padding = 1, n_bins = n_bins)
        self.bn1 = nn.BatchNorm3d(64)
        self.enc_2 = Decay3Dx2new(64, 128, 3, padding = 1, n_bins = n_bins )
        self.bn2 = nn.BatchNorm3d(128)
        self.enc_3 = Decay3Dx2new(128, 256, 3, padding = 1, n_bins = n_bins)
        self.bn3 = nn.BatchNorm3d(256)
        self.enc_4 = Decay3Dx2new(256, 512 // factor,  3, padding = 1, n_bins = n_bins) # We concat the channel dim when we do skip connections so divide out_channels by the scalling factor
        self.bn4 = nn.BatchNorm3d(256)

        self.maxpool = nn.MaxPool3d((1,factor,factor)) # spatially reducing the input by a factor of 2.

        # Decoder 
        self.dec_1 = upconv_agnostic(512, 256 // factor, 3, n_bins = n_bins, layer_type= Decay3Dx2new)
        self.bnd1 = nn.BatchNorm3d(256//factor)
        self.dec_2 = upconv_agnostic(256, 128 // factor, 3,  n_bins = n_bins, layer_type= Decay3Dx2new)
        self.bnd2 = nn.BatchNorm3d(128//factor)
        self.dec_3 = upconv_agnostic(128, 64, 3,  n_bins = n_bins,layer_type= Decay3Dx2new)
        self.bnd3 = nn.BatchNorm3d(64)
        self.dec_4 = Decay3Dx2new(64, out_channels, 3, padding =1, n_bins = n_bins)

        self.upsample = nn.Upsample(scale_factor = (1,factor,factor), mode = "trilinear", align_corners=True) 
        self.upsample_mask = nn.Upsample(scale_factor = (1,factor,factor), mode = "nearest")




    # --------------------------------------------------------------------------------
    def forward(self, x, mask):

        # Encoding
        x1 = self.enc_1(x, mask)

        x2 = self.enc_2(F.relu(self.maxpool(x1[0])), self.maxpool(x1[1]))

        x3 = self.enc_3(F.relu(self.maxpool(x2[0])), self.maxpool(x2[1]))

        x4 = self.enc_4(F.relu(self.maxpool(x3[0])), self.maxpool(x3[1]))

        # Decoding
        x5 = self.dec_1(x4[0], x3[0],x4[1], x3[1]) 
        x6 = self.dec_2(x5[0], x2[0], x5[1], x2[1])
        x7 = self.dec_3(x6[0], x1[0], x6[1], x1[1])
        x8 = self.dec_4(x7[0],x7[1])

        return x8
    





# ===============================================================================================
# U-DeNN model (with new rescalling value + new dense mode efficient?)
class Udenn_nDense(nn.Module):
    """Base architecture resembling a conventional Encoder-decoder fully convolutional U-Net (https://github.com/milesial/Pytorch-UNet/tree/master)
    with skip connections, made up of EDENN layers (with new equivalent ).
    
    Parameters
    ----------
    in_channels:    Number of channels to be expected in in the input.
    out_channels:   Number of channels to be outputed.
    n_bins:         Number of time slices to be expected in the input and throughout the architecture.
    
    """

    # --------------------------------------------------------------------------------
    def __init__(self, in_channels: int, out_channels: int, n_bins: int = None, factor: int = 2):
        super().__init__()

        # Encoder
        self.enc_1 = Decay3Dx2newdense(in_channels, 64, 3, padding = 1, n_bins = n_bins)
        #self.bn1 = nn.BatchNorm3d(64)
        self.enc_2 = Decay3Dx2newdense(64, 128, 3, padding = 1, n_bins = n_bins )
        #self.bn2 = nn.BatchNorm3d(128)
        self.enc_3 = Decay3Dx2newdense(128, 256, 3, padding = 1, n_bins = n_bins)
        #self.bn3 = nn.BatchNorm3d(256)
        self.enc_4 = Decay3Dx2newdense(256, 512 // factor,  3, padding = 1, n_bins = n_bins) # We concat the channel dim when we do skip connections so divide out_channels by the scalling factor
        #self.bn4 = nn.BatchNorm3d(256)

        self.maxpool = nn.MaxPool3d((1,factor,factor)) # spatially reducing the input by a factor of 2.

        # Decoder 
        self.dec_1 = upconv_agnostic(512, 256 // factor, 3, n_bins = n_bins, layer_type= Decay3Dx2newdense)
        #self.bnd1 = nn.BatchNorm3d(256//factor)
        self.dec_2 = upconv_agnostic(256, 128 // factor, 3,  n_bins = n_bins, layer_type= Decay3Dx2newdense)
        #self.bnd2 = nn.BatchNorm3d(128//factor)
        self.dec_3 = upconv_agnostic(128, 64, 3,  n_bins = n_bins, layer_type= Decay3Dx2newdense)
        #self.bnd3 = nn.BatchNorm3d(64)
        self.dec_4 = Decay3Dx2newdense(64, out_channels, 3, padding =1, n_bins = n_bins)

        self.upsample = nn.Upsample(scale_factor = (1,factor,factor), mode = "trilinear", align_corners=True) 
        self.upsample_mask = nn.Upsample(scale_factor = (1,factor,factor), mode = "nearest")




    # --------------------------------------------------------------------------------
    def forward(self, x, mask):

        # Encoding?
        x1 = self.enc_1(x, mask)

        x2 = self.enc_2(F.relu(self.maxpool(x1[0])), self.maxpool(x1[1]))

        x3 = self.enc_3(F.relu(self.maxpool(x2[0])), self.maxpool(x2[1]))

        x4 = self.enc_4(F.relu(self.maxpool(x3[0])), self.maxpool(x3[1]))

        # Decoding
        x5 = self.dec_1(x4[0], x3[0],x4[1], x3[1]) 
        x6 = self.dec_2((x5[0]), x2[0], x5[1], x2[1])
        x7 = self.dec_3(x6[0], x1[0], x6[1], x1[1])
        x8 = self.dec_4(x7[0],x7[1])

        return x8
    

    




    
# ===============================================================================================
# U-DeNN model (with new rescalling value) + pytorch light
class Udenn_npl(pl.LightningModule):
    """Base architecture resembling a conventional Encoder-decoder fully convolutional U-Net (https://github.com/milesial/Pytorch-UNet/tree/master)
    with skip connections, made up of EDENN layers (with new equivalent ).
    
    Parameters
    ----------
    in_channels:    Number of channels to be expected in in the input.
    out_channels:   Number of channels to be outputed.
    n_bins:         Number of time slices to be expected in the input and throughout the architecture.
    
    """

    # --------------------------------------------------------------------------------
    def __init__(self, in_channels: int, out_channels: int, n_bins: int = None, factor: int = 2, dataset_id: int = 0, lr: int = 3e-4):
        super().__init__()

        # Encoder
        self.enc_1 = Decay3Dx2new(in_channels, 64, 3, padding = 1, n_bins = n_bins)
        self.bn1 = nn.BatchNorm3d(64)
        self.enc_2 = Decay3Dx2new(64, 128, 3, padding = 1, n_bins = n_bins )
        self.bn2 = nn.BatchNorm3d(128)
        self.enc_3 = Decay3Dx2new(128, 256, 3, padding = 1, n_bins = n_bins)
        self.bn3 = nn.BatchNorm3d(256)
        self.enc_4 = Decay3Dx2new(256, 512 // factor,  3, padding = 1, n_bins = n_bins) # We concat the channel dim when we do skip connections so divide out_channels by the scalling factor
        self.bn4 = nn.BatchNorm3d(256)

        self.maxpool = nn.MaxPool3d((1,factor,factor)) # spatially reducing the input by a factor of 2.

        # Decoder 
        self.dec_1 = upconv_agnostic(512, 256 // factor, 3, n_bins = n_bins, layer_type= Decay3Dx2new)
        self.bnd1 = nn.BatchNorm3d(256//factor)
        self.dec_2 = upconv_agnostic(256, 128 // factor, 3,  n_bins = n_bins, layer_type= Decay3Dx2new)
        self.bnd2 = nn.BatchNorm3d(128//factor)
        self.dec_3 = upconv_agnostic(128, 64, 3,  n_bins = n_bins, layer_type= Decay3Dx2new)
        self.bnd3 = nn.BatchNorm3d(64)
        self.dec_4 = Decay3Dx2new(64, out_channels, 3, padding =1, n_bins = n_bins)

        self.upsample = nn.Upsample(scale_factor = (1,factor,factor), mode = "trilinear", align_corners=True) 
        self.upsample_mask = nn.Upsample(scale_factor = (1,factor,factor), mode = "nearest")


        self.dataset_id = dataset_id # Need this for training_step
        self.lr = lr # need this for configure optimizers
        self.wandb_id = None # need this for checkpointing and wandb

        if dataset_id in [0]: # DSEC-flow
            self.loss = F.l1_loss


    # --------------------------------------------------------------------------------
    def forward(self, x, mask):

        # Encoding
        x1 = self.enc_1(x, mask)

        x2 = self.enc_2(F.relu(self.maxpool(x1[0])), self.maxpool(x1[1]))

        x3 = self.enc_3(F.relu(self.maxpool(x2[0])), self.maxpool(x2[1]))

        x4 = self.enc_4(F.relu(self.maxpool(x3[0])), self.maxpool(x3[1]))

        # Decoding
        x5 = self.dec_1(x4[0], x3[0],x4[1], x3[1]) 
        x6 = self.dec_2(x5[0], x2[0], x5[1], x2[1])
        x7 = self.dec_3(x6[0], x1[0], x6[1], x1[1])
        x8 = self.dec_4(x7[0],x7[1])

        return x8
    

    # --------------------------------------------------------------------------------
    # Lightning funciton that does one full training iteration
    def training_step(self, batch, batch_idx):

        if self.dataset_id in [0]: # DSEC-flow
            # Get images and labels and send them to device

            events, (flow, valid2D) = batch["events"], batch["flow"]
            valid2D = valid2D.float()# Convert from Boolean tensor to number tensor    

        
        events = events.unsqueeze(1)    
        valid2D = valid2D.unsqueeze(3)  
        flow = flow.unsqueeze(1)

        # Create binary mask of where events are present or not
        input_mask = torch.heaviside(torch.abs(events), torch.tensor(0, dtype= events.dtype))
        out, out_mask = self(events,input_mask)


        if self.dataset_id in [0] : # DSEC-flow 
            # Only calculate loss on those pixels where the flow is valid
            # are also used for supervision
            flow = (flow * valid2D).squeeze(1)
            out = (out.permute(0,2,3,4,1) * valid2D)[:,-1,:,:,:] # permute channel dim of output and only get last timestep as the final pred

        t_error = self.loss(out, flow) # task loss

        self.log('TRAIN_task_loss', t_error)
        #self.log("Epoch", self.current_epoch)


        return t_error
    

    
    # --------------------------------------------------------------------------------
    # Lightning funciton that does one full training iteration
    def validation_step(self, batch, batch_idx):

        if self.dataset_id in [0]: # DSEC-flow
                # Get images and labels and send them to device
                events, (flow, valid2D) = batch["events"], batch["flow"]
                valid2D = valid2D.float()# Convert from Boolean tensor to number tensor    


        # For E-RAFT representation, unsqueeze the channel dimension
        events = events.unsqueeze(1)    
        valid2D = valid2D.unsqueeze(3)  
        flow = flow.unsqueeze(1)

        # Create binary mask of where events are present or not
        input_mask = torch.heaviside(torch.abs(events), torch.tensor(0, dtype= events.dtype))
        out, out_mask = self(events,input_mask)

        if self.dataset_id in [0] : # DSEC-flow 
            # Only calculate loss on those pixels where the flow is valid
            # are also used for supervision
            flow = (flow * valid2D).squeeze(1)
            out = (out.permute(0,2,3,4,1) * valid2D)[:,-1,:,:,:] # permute channel dim of output and only get last timestep as the final pred


        t_error = self.loss(out, flow) # task loss

        self.log('VAL_task_loss', t_error, prog_bar = True, sync_dist = True)
        # val step doesn't return anything




    # --------------------------------------------------------------------------------
    # Lightning function that configures optimizers
    def configure_optimizers(self):
        optimizer = torch.optim.Adam(self.parameters(), lr=self.lr)
        return optimizer
    

    # --------------------------------------------------------------------------------
    # helper function to allow the checkpointing and resuming of wandb runs
    def add_wandb_id(self, id):
        self.wandb_id = id

    # --------------------------------------------------------------------------------
    # Lightning function to save wandb id of run
    def on_save_checkpoint(self, checkpoint):
        # Add custom items to the checkpoint dictionary
        checkpoint['wandb_run_id'] = self.wandb_id if self.wandb_id is not None else None # Save wandb run ID

    






# ===============================================================================================
# U-DeNN model (with new rescalling value) + pytorch light - partial convolutions
class Udenn_nopart(pl.LightningModule):
    """Base architecture resembling a conventional Encoder-decoder fully convolutional U-Net (https://github.com/milesial/Pytorch-UNet/tree/master)
    with skip connections, made up of EDENN layerads (with new equivalent ).
    
    Parameters
    ----------
    in_channels:    Number of channels to be expected in in the input.
    out_channels:   Number of channels to be outputed.
    n_bins:         Number of time slices to be expected in the input and throughout the architecture.
    
    """

    # --------------------------------------------------------------------------------
    def __init__(self, in_channels: int, out_channels: int, n_bins: int = None, factor: int = 2, dataset_id: int = 0, lr: int = 3e-4):
        super().__init__()

        # Encoder
        self.enc_1 = Decay3Dx2_no_partial(in_channels, 64, 3, padding = 1, n_bins = n_bins)
        self.bn1 = nn.BatchNorm3d(64)
        self.enc_2 = Decay3Dx2_no_partial(64, 128, 3, padding = 1, n_bins = n_bins )
        self.bn2 = nn.BatchNorm3d(128)
        self.enc_3 = Decay3Dx2_no_partial(128, 256, 3, padding = 1, n_bins = n_bins)
        self.bn3 = nn.BatchNorm3d(256)
        self.enc_4 = Decay3Dx2_no_partial(256, 512 // factor,  3, padding = 1, n_bins = n_bins) # We concat the channel dim when we do skip connections so divide out_channels by the scalling factor
        self.bn4 = nn.BatchNorm3d(256)

        self.maxpool = nn.MaxPool3d((1,factor,factor)) # spatially reducing the input by a factor of 2.

        # Decoder 
        self.dec_1 = upconv_agnostic(512, 256 // factor, 3, n_bins = n_bins, layer_type= Decay3Dx2_no_partial)
        self.bnd1 = nn.BatchNorm3d(256//factor)
        self.dec_2 = upconv_agnostic(256, 128 // factor, 3,  n_bins = n_bins, layer_type= Decay3Dx2_no_partial)
        self.bnd2 = nn.BatchNorm3d(128//factor)
        self.dec_3 = upconv_agnostic(128, 64, 3,  n_bins = n_bins, layer_type= Decay3Dx2_no_partial)
        self.bnd3 = nn.BatchNorm3d(64)
        self.dec_4 = Decay3Dx2_no_partial(64, out_channels, 3, padding =1, n_bins = n_bins)

        self.upsample = nn.Upsample(scale_factor = (1,factor,factor), mode = "trilinear", align_corners=True) 
        self.upsample_mask = nn.Upsample(scale_factor = (1,factor,factor), mode = "nearest")


        self.dataset_id = dataset_id # Need this for training_step
        self.lr = lr # need this for configure optimizers
        self.wandb_id = None # need this for checkpointing and wandb

        if dataset_id in [0]: # DSEC-flow
            self.loss = F.l1_loss


    # --------------------------------------------------------------------------------
    def forward(self, x, mask):

        # Encoding
        x1 = self.enc_1(x, mask)

        x2 = self.enc_2(F.relu(self.maxpool(x1[0])), self.maxpool(x1[1]))

        x3 = self.enc_3(F.relu(self.maxpool(x2[0])), self.maxpool(x2[1]))

        x4 = self.enc_4(F.relu(self.maxpool(x3[0])), self.maxpool(x3[1]))

        # Decoding
        x5 = self.dec_1(x4[0], x3[0],x4[1], x3[1]) 
        x6 = self.dec_2(x5[0], x2[0], x5[1], x2[1])
        x7 = self.dec_3(x6[0], x1[0], x6[1], x1[1])
        x8 = self.dec_4(x7[0],x7[1])

        return x8
    

    # --------------------------------------------------------------------------------
    # Lightning funciton that does one full training iteration
    def training_step(self, batch, batch_idx):

        if self.dataset_id in [0]: # DSEC-flow
                # Get images and labels and send them to device
                events, (flow, valid2D) = batch["events"], batch["flow"]
                valid2D = valid2D.float()# Convert from Boolean tensor to number tensor    

        events = events.unsqueeze(1)    
        valid2D = valid2D.unsqueeze(3)  
        flow = flow.unsqueeze(1)
            
        # Create binary mask of where events are present or not
        input_mask = torch.heaviside(torch.abs(events), torch.tensor(0, dtype= events.dtype))
        out, out_mask = self(events,input_mask)

             

        if self.dataset_id in [0] : # DSEC-flow 
            # Only calculate loss on those pixels where the flow is valid
            # are also used for supervision
            flow = (flow * valid2D).squeeze(1)
            out = (out.permute(0,2,3,4,1) * valid2D)[:,-1,:,:,:] # permute channel dim of output and only get last timestep as the final pred

        t_error = self.loss(out, flow) # task loss

        self.log('TRAIN_task_loss', t_error)
        #self.log("Epoch", self.current_epoch)


        return t_error
    

    
    # --------------------------------------------------------------------------------
    # Lightning funciton that does one full training iteration
    def validation_step(self, batch, batch_idx):

        if self.dataset_id in [0]: # DSEC-flow
                # Get images and labels and send them to device
                events, (flow, valid2D) = batch["events"], batch["flow"]
                valid2D = valid2D.float()# Convert from Boolean tensor to number tensor    

    
        # For E-RAFT representation, unsqueeze the channel dimension
        events = events.unsqueeze(1)    
        valid2D = valid2D.unsqueeze(3)  
        flow = flow.unsqueeze(1)
            
        # Create binary mask of where events are present or not
        input_mask = torch.heaviside(torch.abs(events), torch.tensor(0, dtype= events.dtype))
        out, out_mask = self(events,input_mask)

             

        if self.dataset_id in [0] : # DSEC-flow 
            # Only calculate loss on those pixels where the flow is valid
            # are also used for supervision
            flow = (flow * valid2D).squeeze(1)
            out = (out.permute(0,2,3,4,1) * valid2D)[:,-1,:,:,:] # permute channel dim of output and only get last timestep as the final pred


        t_error = self.loss(out, flow) # task loss

        self.log('VAL_task_loss', t_error, sync_dist= True)
        # val step doesn't return anything




    # --------------------------------------------------------------------------------
    # Lightning function that configures optimizers
    def configure_optimizers(self):
        optimizer = torch.optim.Adam(self.parameters(), lr=self.lr)
        return optimizer
    

    # --------------------------------------------------------------------------------
    # helper function to allow the checkpointing and resuming of wandb runs
    def add_wandb_id(self, id):
        self.wandb_id = id

    # --------------------------------------------------------------------------------
    # Lightning function to save wandb id of run
    def on_save_checkpoint(self, checkpoint):
        # Add custom items to the checkpoint dictionary
        checkpoint['wandb_run_id'] = self.wandb_id if self.wandb_id is not None else None # Save wandb run ID

    
    




    
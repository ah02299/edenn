# 
import math
import collections.abc
from typing import Optional

import torch
import torch.nn.functional as F
import numpy as np


class partialDecay3Dx(torch.nn.modules.Module):
	"""This is alex's pytorch re-implementation of the decay3d layer (with partial convolutions) presented in the paper: "EDeNN: Event Decay Neural Networks for low latency vision". 
	Link: https://arxiv.org/abs/2209.04362

	The layer expects an input of the form (B,C,T,H,W) for [batch, n_channels, n_time_bins, Height, Width]
	
	Parameters
	----------
	in_channels:        Number of channels in the input 
	out_channels:       Number of learned filters in the layer or number of output channels
	kernel_size:        Size of the learned filters in the form (H, W).
	stride:             Stride of the convolution operation
	padding:            Stride of the convolution operation
	bias:               Add learned bias to the layer's filters (default -> True)
	n_bins:             How many slices of time will the decay operation take into account during dense mode (default -> None, the decay filter will be calculated to be of size T in the time dim)
	"""

	# ----------------------------------------------------------------------------------------------
	def __init__(self, in_channels: int, out_channels: int, kernel_size: int or tuple(int, int), 
				 stride: int or tuple(int, int, int) = 1, padding: int or tuple(int, int, int) = 0, bias: bool = True, n_bins: int = None):
		super().__init__()

		self.in_channels = in_channels
		self.out_channels = out_channels
		self.kernel_size = kernel_size if isinstance(kernel_size, collections.abc.Iterable) else (1, kernel_size, kernel_size)
		self.stride = stride if isinstance(stride, collections.abc.Iterable) else (1, stride, stride)
		self.padding = padding if isinstance(padding, collections.abc.Iterable) else (0, padding, padding)
		self.n_bins = n_bins
	
		# # Weights of the layer (both spatial and decay)
		self.spatial_weight = torch.nn.Parameter(torch.Tensor(out_channels, in_channels, *self.kernel_size))
		self.decay_weight = torch.nn.Parameter(torch.Tensor((out_channels)))
		# Add bias if necessary
		if bias:
			self.bias = torch.nn.Parameter(torch.Tensor(out_channels))
		else:
			self.bias = None
			self.register_parameter("bias", None)

		# Initialize parameters (This fixes NaNs?)
		self.reset_parameters()



		# '''
		#     Known weights and biases for testing (comment out above)
		# '''
		# # # Weights of the layer (both spatial and decay)
		# self.spatial_weight = torch.nn.Parameter(torch.full((out_channels, in_channels, *self.kernel_size),2,dtype=torch.float))
		# self.decay_weight = torch.nn.Parameter(torch.full([out_channels], 0.1))

		# # Add bias if necessary
		# if bias:
		#     self.bias = torch.nn.Parameter(torch.ones(out_channels))
		# else:
		#     self.bias = None
		#     #self.register_parameter("bias", None)



		# Initialise the layer's neurons state (for streaming mode)
		self.neuron_state = 0


	# ----------------------------------------------------------------------------------------------
	def reset_parameters(self):
		# Use He initialisation for the layer weights' and biases' (taken directly from old edenn)
		torch.nn.init.kaiming_uniform_(self.spatial_weight, a=math.sqrt(5))
		torch.nn.init.kaiming_uniform_(self.decay_weight.unsqueeze(0), a=math.sqrt(5))
		if self.bias is not None:
			# fan_in, _ = torch.nn.init._calculate_fan_in_and_fan_out(self.spatial_weight)
			# bound = 1 / math.sqrt(fan_in)
			# torch.nn.init.uniform_(self.bias, -bound, bound)
			torch.nn.init.zeros_(self.bias)


	# ----------------------------------------------------------------------------------------------
	def forward(self, x: torch.Tensor, mask: torch.Tensor = None):
		assert len(x.shape) == 5 # (B,C,T,H,W)


		# 0.1 Mask input if mask present
		if mask != None:
			x = x * mask 
		
		# 1. Spatial convolution
		convolved_potential = F.conv3d(x,self.spatial_weight, bias = self.bias, stride= self.stride, padding=self.padding)

		# 1.1 re-weighting
		if mask != None:
			# Each element in the convolved potential tensor should be re-weighted by (sum(kernel)/sum(unmasked_kernel))
			convolved_potential, new_mask = self.re_weighting(convolved_potential, mask)

		# 2. Decay step 

		#== Streaming mode ==#          
		if x.shape[2] == 1: # If only one slice is provided then streaming mode
			if isinstance(self.neuron_state, int):
				# Add current potential to the neuron memory
				output = convolved_potential
				# Decay for next timestep and override the memory (permute to put the C dimension last and multiply each channel/kernel_output by its the decay)
				self.neuron_state = (output.permute(0,2,3,4,1) * (1 - self.decay_weight)).permute(0,4,1,2,3)

			else:
				output = convolved_potential + self.neuron_state
				# Decay for next timestep and override the memory (permute to put the C dimension last and multiply each channel/kernel_output by its the decay)
				self.neuron_state = (output.permute(0,2,3,4,1) * (1 - self.decay_weight)).permute(0,4,1,2,3)
		

		#== Dense mode ==#    
		else: # If more than one slice is provided then dense mode

			if self.n_bins == None: # Get number of timesteps provided and read them all, if not only read n timesteps at a time as specified in the input
				self.n_bins = x.shape[2] 

			# Set how many timesteps will the dense mode take into account. Also reverse them so they go from t0 to T.
			self.timesteps = torch.tensor(np.arange(self.n_bins)[::-1].copy())
			# repeat the same timesteps per out-channel (per-filter)
			dense_timesteps = torch.t(torch.tile(self.timesteps, (self.spatial_weight.shape[0],1)))

			# Decay "propagation" to get decay arrays of [dec^(T-1), dec^(T-2), ..., dec^(1), dec^(0)] per filter
			decay_filters = torch.t(torch.pow((1-self.decay_weight),dense_timesteps.to(self.decay_weight.device))) # Send dense_timesteps to device
			# Tensor with the decay multiplier for each timestep. Reshape to (n_filters, 1, T, 1, 1)
			decay_filter_tensor = decay_filters.reshape(self.spatial_weight.shape[0],1,self.n_bins,1,1) 
			# Pad input 
			padding = (0,0, 0,0, (self.n_bins-1),0, 0,0) # two padding values per dimension, adding padding at the front of T dim = (T - 1)
			padded_potential = F.pad(convolved_potential, padding, value=0)

			# Convolve the output with the decay filter, if more than one conv filter set the groups param to number of conv filters 
			# so each "activation map" is decayed independently with the appropriate decay values 
			output = F.conv3d(padded_potential,decay_filter_tensor, groups=convolved_potential.shape[1])

		return output, new_mask if mask != None else None # If no mask in the input then no mask in the output
	
	# ----------------------------------------------------------------------------------------------
	# Output re-weighting for partial convolutions
	def re_weighting(self, x: torch.Tensor, mask: torch.Tensor = None):   

		# Our definition of re-weighting value is calculated as -> (sum(kernel)/sum(unmasked_kernel))

		with torch.no_grad():
			# Get per-filter sum of all elements
			sum_kernel = torch.sum(self.spatial_weight,dim=[1,2,3,4]) 
			# Convolve the filters by the mask to get the sum of the unmasked elements per position
			sum_unmasked = F.conv3d(mask.repeat(1, self.spatial_weight.shape[1], 1,1,1).to(self.spatial_weight.device), self.spatial_weight) 
			# Perform the division operation to get the re-weighting tensor
			re_weight = (sum_kernel / sum_unmasked.permute(0,2,3,4,1)).permute(0,4,1,2,3)       

			# Update mask (if a value was calculated from at least one unmasked element, mask = 1, else mask = 0)
			new_mask = torch.heaviside(torch.abs(sum_unmasked), torch.tensor(0, dtype= torch.float))
		# multiply the convolved potential with its re-weighting values
		re_weighted_output = x * re_weight

		return re_weighted_output, new_mask
		






# ================================================================================================
class Decay3Dx2(torch.nn.modules.Module):
	"""This is alex's pytorch "decay efficient" re-implementation of the decay3d layer (with partial convolutions) presented in the paper: "EDeNN: Event Decay Neural Networks for low latency vision". 
	Link: https://arxiv.org/abs/2209.04362

	The layer expects an input of the form (B,C,T,H,W) for [batch, n_channels, n_time_bins, Height, Width]
	
	Parameters
	----------
	in_channels:        Number of channels in the input 
	out_channels:       Number of learned filters in the layer or number of output channels
	kernel_size:        Size of the learned filters in the form (H, W).
	stride:             Stride of the convolution operation
	padding:            Stride of the convolution operation
	bias:               Add learned bias to the layer's filters (default -> True)
	n_bins:             How many slices of time will the decay operation take into account during dense mode (default -> None, the decay filter will be calculated to be of size T in the time dim)
	"""

	# ----------------------------------------------------------------------------------------------
	def __init__(self, in_channels: int, out_channels: int, kernel_size: int or tuple(int, int), 
				stride: int or tuple(int, int, int) = 1, padding: int or tuple(int, int, int) = 0, bias: bool = True, n_bins: int = None):
		super().__init__()

		self.in_channels = in_channels
		self.out_channels = out_channels
		self.kernel_size = kernel_size if isinstance(kernel_size, collections.abc.Iterable) else (1, kernel_size, kernel_size)
		self.stride = stride if isinstance(stride, collections.abc.Iterable) else (1, stride, stride)
		self.padding = padding if isinstance(padding, collections.abc.Iterable) else (0, padding, padding)
		self.n_bins = n_bins
	
		# # Weights of the layer (both spatial and decay)
		self.spatial_weight = torch.nn.Parameter(torch.Tensor(out_channels, in_channels, *self.kernel_size))
		self.decay_weight = torch.nn.Parameter(torch.Tensor((out_channels)))

		# Add bias if necessary
		if bias:
			self.bias = torch.nn.Parameter(torch.Tensor(out_channels))
		else:
			self.bias = None
			self.register_parameter("bias", None)

		# Initialize parameters (This fixes NaNs?)
		self.reset_parameters()


		# '''
		#     Known weights and biases for testing (comment out above)
		# '''
		# # # Weights of the layer (both spatial and decay)
		# self.spatial_weight = torch.nn.Parameter(torch.full((out_channels, in_channels, *self.kernel_size),1,dtype=torch.float))
		# self.decay_weight = torch.nn.Parameter(torch.full([out_channels], 0.1))
		# if bias:
		#     self.bias = torch.nn.Parameter(torch.ones(out_channels))
		# else:
		#     self.bias = None
		#     #self.register_parameter("bias", None)
		# '''
		# #     Known weights and biases for testing (comment out above)
		# '''

		# Initialise the layer's neurons and partial convolution re-weighting state (for streaming mode)
		self.neuron_state = 0
		self.sum_unmasked = None
		self.sum_kernel = None


	# ----------------------------------------------------------------------------------------------
	def reset_parameters(self):
		# Use He initialisation for the layer weights' and biases' (taken directly from old edenn)
		#torch.nn.init.kaiming_uniform_(self.spatial_weight, a=math.sqrt(5))
		# torch.nn.init.kaiming_uniform_(self.decay_weight.unsqueeze(0), a=math.sqrt(5))

		torch.nn.init.normal_(self.spatial_weight, mean=0.5, std=0.5)
		torch.nn.init.normal_(self.decay_weight.unsqueeze(0),mean=0.5, std=0.5)
		if self.bias is not None:
			# fan_in, _ = torch.nn.init._calculate_fan_in_and_fan_out(self.spatial_weight)
			# bound = 1 / math.sqrt(fan_in)
			# torch.nn.init.uniform_(self.bias, -bound, bound)
			#torch.nn.init.xavier_uniform_(self.bias.unsqueeze(0), gain=1.0)
			torch.nn.init.ones_(self.bias)
			#torch.nn.init.xavier_uniform_(self.bias, gain=1.0)

		# print(f"nans in the spatial weight: {torch.isnan(self.spatial_weight).any()}")
		# print(f"nans in the decay weight: {torch.isnan(self.decay_weight).any()}")


	# ----------------------------------------------------------------------------------------------
	def forward(self, x: torch.Tensor, mask: torch.Tensor = None):
		assert len(x.shape) == 5 # (B,C,T,H,W)

		# 0.1 Mask input if mask present
		if mask != None:
			x = x * mask 


		#== Dense mode (pre multiply the kernel by the decay value per timestep to only do 1 convolution instead of 2) ==#    
		if x.shape[2] != 1: # If more than one slice is provided then dense mode

			if self.n_bins == None: # Get number of timesteps provided and read them all, if not only read n timesteps at a time as specified in the input
				self.n_bins = x.shape[2] 

			# Set how many timesteps will the dense mode take into account. Also reverse them so they go from t0 to T.
			timesteps = torch.t(torch.flip(torch.arange(self.n_bins),(0,)))

			# repeat the same timesteps per out-channel (per-filter)
			#dense_timesteps = torch.t(torch.tile(timesteps, (self.spatial_weight.shape[0],1)))

			# Decay broadcasting to get decay array of [dec^(T-1), dec^(T-2), ..., dec^(1), dec^(0)].T per filter 
			# Pass the decay weight through a sigmoid to set it between the accepted bounds of [0, 1]
			decay_filters = torch.t(torch.pow((1-torch.sigmoid(self.decay_weight)), timesteps.unsqueeze(1).to(self.decay_weight.device)))

			# Multiply decay values by expanded spatial kernel for efficienty
			# Expand spatial weight on the time dimension to n_bins
			#exp_spatial = torch.tile(self.spatial_weight, (1,1,self.n_bins,1,1))

			#print(self.spatial_weight.shape)
			# Expand the decay filters in the spatial dimensions to match extended kernel
			#exp_dec_filters = torch.tile(decay_filters.reshape(self.out_channels,1,self.n_bins,1,1),(1,1,1,self.spatial_weight.shape[3],self.spatial_weight.shape[4]))
			#print(decay_filters.reshape(self.out_channels,1,self.n_bins,1,1).shape)
			# Multiply the expanded kernels by the expanded decay values
			#decayed_spatial_kernel = (exp_spatial * exp_dec_filters)

			# Use torch.mul to broadcast dimensions?
			decayed_spatial_kernel = torch.mul(self.spatial_weight, decay_filters.reshape(self.out_channels,1,self.n_bins,1,1))

			# Pad input to match with the expected output 
			padded_potential = F.pad(x, (0,0, 0,0, (self.n_bins-1),0, 0,0), value=0)

		# 1. Spatial convolution
		if x.shape[2] != 1: # If dense mode
			output = F.conv3d(padded_potential,decayed_spatial_kernel, bias = None, stride= self.stride, padding=self.padding)


			if mask != None: # If mask present, apply the re-weighting
				output, new_mask = self.re_weighting_dense(output, decayed_spatial_kernel, mask)
			
			# Add bias to the re-weighted output if present
			output = (output + self.bias.view(1, self.out_channels, 1, 1, 1)) if self.bias != None else output
		else: # If Streaming mode (apply spatial convolution without bias and add it after the decay operation and re-weighting) 
			convolved_potential = F.conv3d(x,self.spatial_weight, bias = None, stride= self.stride, padding=self.padding)

		#== Streaming mode ==#  This new streaming mode only adds the bias to the output to match the efficient formulation of dense mode    
		if x.shape[2] == 1: # If only one slice is provided then streaming mode
			if isinstance(self.neuron_state, int): # t = 0

				if mask != None: # If mask present, do re-weighting
					re_scaled_potential, new_mask = self.re_weighting_streaming(convolved_potential, mask)
					# Add current potential to the neuron memory
					output = (re_scaled_potential + self.bias.view(1, self.out_channels, 1, 1, 1)) if self.bias != None else re_scaled_potential
					# Decay for next timestep and override the memory (permute to put the C dimension last and multiply each channel/kernel_output by its the decay)
					# Pass the decay weight through a sigmoid to set it between the accepted bounds of [0, 1]
					self.neuron_state = (convolved_potential.permute(0,2,3,4,1) * (1 - torch.sigmoid(self.decay_weight))).permute(0,4,1,2,3)
				else:
					# Add bias to convolved potential 
					output = (convolved_potential + self.bias.view(1, self.out_channels, 1, 1, 1)) if self.bias != None else convolved_potential
					# Decay for next timestep and override the memory (permute to put the C dimension last and multiply each channel/kernel_output by its the decay)
					# Pass the decay weight through a sigmoid to set it between the accepted bounds of [0, 1]
					self.neuron_state = (convolved_potential.permute(0,2,3,4,1) * (1 - torch.sigmoid(self.decay_weight))).permute(0,4,1,2,3)
				
			else: # t != 0
				# Sum of the spatially convolved input at t and the decayed state of t-1 (pre-adding bias)
				pre_bias = convolved_potential + self.neuron_state 
				
				if mask != None: # If mask present, do re-weighting
					re_scaled_potential, new_mask = self.re_weighting_streaming(pre_bias, mask)
					# Output is the sum of the convolved potential, current neuron state and bias         
					output = (re_scaled_potential + self.bias.view(1, self.out_channels, 1, 1, 1)) if self.bias != None else re_scaled_potential
					# Decay for next timestep and override the memory (permute to put the C dimension last and multiply each channel/kernel_output by its the decay)
					# Pass the decay weight through a sigmoid to set it between the accepted bounds of [0, 1]
					self.neuron_state = (pre_bias.permute(0,2,3,4,1) * (1 - torch.sigmoid(self.decay_weight))).permute(0,4,1,2,3)
				else:
					# Output is the sum of the convolved potential, current neuron state and bias         
					output = (pre_bias + self.bias.view(1, self.out_channels, 1, 1, 1)) if self.bias != None else pre_bias
					# Decay for next timestep and override the memory (permute to put the C dimension last and multiply each channel/kernel_output by its the decay)
					# Pass the decay weight through a sigmoid to set it between the accepted bounds of [0, 1]
					self.neuron_state = (pre_bias.permute(0,2,3,4,1) * (1 - torch.sigmoid(self.decay_weight))).permute(0,4,1,2,3)

		return output, new_mask if mask != None else None # If no mask in the input then no mask in the output. If running on dense mode the correct output is convolved_potential

	
	# ----------------------------------------------------------------------------------------------
	# Output re-weighting for partial convolutions (dense mode)
	def re_weighting_dense(self, x: torch.Tensor, extended_weight: torch.Tensor, mask: torch.Tensor):   
		
		with torch.no_grad(): # No need for gradients in the following operations?
		# Our definition of re-weighting value is calculated as -> (sum(kernel)/sum(unmasked_kernel))

			# Get per-filter sum of all elements (per-timestep) by convolving with a padded tensor of ones.
			padding = (0,0, 0,0, (x.shape[2]-1),0, 0,0) # two padding values per dimension, adding padding at the front of T dim = (n_bins - 1)
			#print(torch.ones(1,extended_weight.shape[1], x.shape[2],extended_weight.shape[3], extended_weight.shape[4]).shape)
			sum_kernel = F.conv3d(F.pad(torch.ones(1,extended_weight.shape[1], x.shape[2],extended_weight.shape[3], extended_weight.shape[4]), padding, value = 0).to(self.spatial_weight.device), extended_weight) 
			# Convolve the filters by the mask to get the sum of the unmasked elements per position (add padding at the beginning so only the right temporal slices are used for the re-weighting at every timestep)
			if mask.shape[1] == 1: # Only repeat the mask in the first timestep
				sum_unmasked = F.conv3d(F.pad(mask.repeat(1, extended_weight.shape[1], 1,1,1), padding, value = 0).to(self.spatial_weight.device), extended_weight, stride=self.stride, padding= self.padding) 
			else:
				sum_unmasked = F.conv3d(F.pad(mask, padding, value = 0).to(self.spatial_weight.device), extended_weight, stride=self.stride, padding= self.padding) 
			
			# Update mask (if a value was calculated from at least one unmasked element, mask = 1, else mask = 0)
			new_mask = torch.heaviside(torch.abs(sum_unmasked), torch.tensor(0, dtype= sum_unmasked.dtype))
			# Perform the division operation to get the re-weighting tensor (sum a really small constant to avoid division by 0)
		re_weight = (sum_kernel / (sum_unmasked + 1e-1)) 
		# Sum the convolved potential with its re-weighting values (maybe this does need gradients? not even sure but too many OOM problemsssss aaaaaaaa)
		re_weighted_output = x * re_weight

		return re_weighted_output, new_mask

	# ----------------------------------------------------------------------------------------------
	# Output re-weighting for partial convolutions (streaming mode) - 
	# Take into account scaling factors of previous timesteps to match the definition of dense mode
	def re_weighting_streaming(self, x: torch.Tensor, mask: torch.Tensor):   
	
		with torch.no_grad(): # No need for gradients in the following operations?
			# Our definition of re-weighting value is calculated as -> (sum(kernel)/sum(unmasked_kernel))
			# Get per-filter sum of all elements
			sum_kernel = torch.sum(self.spatial_weight,dim=[1,2,3,4]) 
			# Convolve the filters by the mask to get the sum of the unmasked elements per position
			if mask.shape[1] == 1: # Only repeat the mask in the first timestep
				sum_unmasked = F.conv3d(mask.repeat(1, self.spatial_weight.shape[1], 1,1,1).to(self.spatial_weight.device), self.spatial_weight,stride=self.stride, padding= self.padding) 
			else:
				sum_unmasked = F.conv3d(mask.to(self.spatial_weight.device), self.spatial_weight, stride=self.stride, padding= self.padding) 

			# Perform the division operation to get the re-weighting tensor
			# At t != 0, sum numerator and denominator with the layer's re-weighting memory to match dense mode, decay it and update layer's memory
			if self.sum_unmasked == None:
				re_weight = (sum_kernel / (sum_unmasked.permute(0,2,3,4,1) + 1e-9)).permute(0,4,1,2,3)       
				self.sum_kernel = (1 - self.decay_weight) * sum_kernel
				self.sum_unmasked = (1 - self.decay_weight.view(1,self.decay_weight.shape[0],1,1,1) )* sum_unmasked
			else:
				self.sum_kernel += sum_kernel
				self.sum_unmasked += sum_unmasked
				re_weight = (self.sum_kernel / (self.sum_unmasked.permute(0,2,3,4,1) + 1e-9)).permute(0,4,1,2,3) 
				self.sum_kernel *= (1 -self.decay_weight)
				self.sum_unmasked *= (1 - self.decay_weight.view(1,self.decay_weight.shape[0],1,1,1))

			# Update mask (if a value was calculated from at least one unmasked element, mask = 1, else mask = 0)
			#new_mask = torch.clip(torch.abs(self.sum_unmasked), 0,1)
			new_mask = torch.heaviside(torch.abs(self.sum_unmasked), torch.tensor(0, dtype= self.sum_unmasked.dtype))            
		
		# Sum the convolved potential with its re-weighting values
		re_weighted_output = x * re_weight

		return re_weighted_output, new_mask








# ================================================================================================
class Decay3Dx2test(torch.nn.modules.Module):
	"""This is alex's pytorch "decay efficient" re-implementation of the decay3d layer (with partial convolutions) presented in the paper: "EDeNN: Event Decay Neural Networks for low latency vision". 
		-   This version contains an alternative (suboptimal) re-weighting value -> mean(abs(kernel))/mean(abs(kernel_unmasked))
	Link: https://arxiv.org/abs/2209.04362

	The layer expects an input of the form (B,C,T,H,W) for [batch, n_channels, n_time_bins, Height, Width]
	
	Parameters
	----------
	in_channels:        Number of channels in the input 
	out_channels:       Number of learned filters in the layer or number of output channels
	kernel_size:        Size of the learned filters in the form (H, W).
	stride:             Stride of the convolution operation
	padding:            Stride of the convolution operation
	bias:               Add learned bias to the layer's filters (default -> True)
	n_bins:             How many slices of time will the decay operation take into account during dense mode (default -> None, the decay filter will be calculated to be of size T in the time dim)
	"""

	# ----------------------------------------------------------------------------------------------
	def __init__(self, in_channels: int, out_channels: int, kernel_size: int or tuple(int, int), 
				stride: int or tuple(int, int, int) = 1, padding: int or tuple(int, int, int) = 0, bias: bool = True, n_bins: int = None):
		super().__init__()

		self.in_channels = in_channels
		self.out_channels = out_channels
		self.kernel_size = kernel_size if isinstance(kernel_size, collections.abc.Iterable) else (1, kernel_size, kernel_size)
		self.stride = stride if isinstance(stride, collections.abc.Iterable) else (1, stride, stride)
		self.padding = padding if isinstance(padding, collections.abc.Iterable) else (0, padding, padding)
		self.n_bins = n_bins
	
		# # Weights of the layer (both spatial and decay)
		self.spatial_weight = torch.nn.Parameter(torch.Tensor(out_channels, in_channels, *self.kernel_size))
		self.decay_weight = torch.nn.Parameter(torch.Tensor((out_channels)))

		# Add bias if necessary
		if bias:
			self.bias = torch.nn.Parameter(torch.Tensor(out_channels))
		else:
			self.bias = None
			self.register_parameter("bias", None)

		# Initialize parameters (This fixes NaNs?)
		self.reset_parameters()


		# '''
		#     Known weights and biases for testing (comment out above)
		# '''
		# # # Weights of the layer (both spatial and decay)
		# self.spatial_weight = torch.nn.Parameter(torch.full((out_channels, in_channels, *self.kernel_size),1,dtype=torch.float))
		# self.decay_weight = torch.nn.Parameter(torch.full([out_channels], 0.1))
		# if bias:
		#     self.bias = torch.nn.Parameter(torch.ones(out_channels))
		# else:
		#     self.bias = None
		#     #self.register_parameter("bias", None)
		# '''
		# #     Known weights and biases for testing (comment out above)
		# '''



		
		# '''
		#     Known weights and biases for testing (comment out above)
		# '''
		# # # Weights of the layer (both spatial and decay)
		# self.spatial_weight = torch.nn.Parameter(torch.full((out_channels, in_channels, *self.kernel_size),1,dtype=torch.float))
		# self.decay_weight = torch.nn.Parameter(torch.full([out_channels], 0.1))
		# if bias:
		#     self.bias = torch.nn.Parameter(torch.ones(out_channels))
		# else:
		#     self.bias = None
		#     #self.register_parameter("bias", None)
		# '''
		# #     Known weights and biases for testing (comment out above)
		# '''

		# Initialise the layer's neurons and partial convolution re-weighting state (for streaming mode)
		self.neuron_state = 0
		self.sum_unmasked = None
		self.sum_kernel = None


	# ----------------------------------------------------------------------------------------------
	def reset_parameters(self):
		# Use He initialisation for the layer weights' and biases' (taken directly from old edenn)
		torch.nn.init.kaiming_uniform_(self.spatial_weight, a=math.sqrt(5))
		torch.nn.init.kaiming_uniform_(self.decay_weight.unsqueeze(0), a=math.sqrt(5))

		#self.spatial_weight = torch.nn.Parameter(torch.abs(self.spatial_weight))

		#torch.nn.init.normal_(self.spatial_weight, mean=0.5, std=0.5)
		#torch.nn.init.normal_(self.decay_weight.unsqueeze(0),mean=0.5, std=0.5)
		if self.bias is not None:
			# fan_in, _ = torch.nn.init._calculate_fan_in_and_fan_out(self.spatial_weight)
			# bound = 1 / math.sqrt(fan_in)
			# torch.nn.init.uniform_(self.bias, -bound, bound)
			#torch.nn.init.xavier_uniform_(self.bias.unsqueeze(0), gain=1.0)
			torch.nn.init.ones_(self.bias)
			#torch.nn.init.xavier_uniform_(self.bias, gain=1.0)

		# print(f"nans in the spatial weight: {torch.isnan(self.spatial_weight).any()}")
		# print(f"nans in the decay weight: {torch.isnan(self.decay_weight).any()}")


	# ----------------------------------------------------------------------------------------------
	def forward(self, x: torch.Tensor, mask: torch.Tensor = None):
		assert len(x.shape) == 5 # (B,C,T,H,W)

		# 0.1 Mask input if mask present
		if mask != None:
			x = x * mask 


		#== Dense mode (pre multiply the kernel by the decay value per timestep to only do 1 convolution instead of 2) ==#    
		if x.shape[2] != 1: # If more than one slice is provided then dense mode

			if self.n_bins == None: # Get number of timesteps provided and read them all, if not only read n timesteps at a time as specified in the input
				self.n_bins = x.shape[2] 

			# Set how many timesteps will the dense mode take into account. Also reverse them so they go from t0 to T.
			timesteps = torch.t(torch.flip(torch.arange(self.n_bins),(0,)))

			# repeat the same timesteps per out-channel (per-filter)
			#dense_timesteps = torch.t(torch.tile(timesteps, (self.spatial_weight.shape[0],1)))

			# Decay broadcasting to get decay array of [dec^(T-1), dec^(T-2), ..., dec^(1), dec^(0)].T per filter 
			# Pass the decay weight through a sigmoid to set it between the accepted bounds of [0, 1]
			decay_filters = torch.t(torch.pow((1-torch.sigmoid(self.decay_weight)), timesteps.unsqueeze(1).to(self.decay_weight.device)))

			# Multiply decay values by expanded spatial kernel for efficienty
			# Expand spatial weight on the time dimension to n_bins
			#exp_spatial = torch.tile(self.spatial_weight, (1,1,self.n_bins,1,1))

			#print(self.spatial_weight.shape)
			# Expand the decay filters in the spatial dimensions to match extended kernel
			#exp_dec_filters = torch.tile(decay_filters.reshape(self.out_channels,1,self.n_bins,1,1),(1,1,1,self.spatial_weight.shape[3],self.spatial_weight.shape[4]))
			#print(decay_filters.reshape(self.out_channels,1,self.n_bins,1,1).shape)
			# Multiply the expanded kernels by the expanded decay values
			#decayed_spatial_kernel = (exp_spatial * exp_dec_filters)

			# Use torch.mul to broadcast dimensions?
			decayed_spatial_kernel = torch.mul(self.spatial_weight, decay_filters.reshape(self.out_channels,1,self.n_bins,1,1))

			# Pad input to match with the expected output 
			padded_potential = F.pad(x, (0,0, 0,0, (self.n_bins-1),0, 0,0), value=0)

		# 1. Spatial convolution
		if x.shape[2] != 1: # If dense mode
			output = F.conv3d(padded_potential,decayed_spatial_kernel, bias = None, stride= self.stride, padding=self.padding)


			if mask != None: # If mask present, apply the re-weighting
				output, new_mask = self.re_weighting_dense(output, decayed_spatial_kernel, mask)
			
			# Add bias to the re-weighted output if present
			output = (output + self.bias.view(1, self.out_channels, 1, 1, 1)) if self.bias != None else output
		else: # If Streaming mode (apply spatial convolution without bias and add it after the decay operation and re-weighting) 
			convolved_potential = F.conv3d(x,self.spatial_weight, bias = None, stride= self.stride, padding=self.padding)

		#== Streaming mode ==#  This new streaming mode only adds the bias to the output to match the efficient formulation of dense mode    
		if x.shape[2] == 1: # If only one slice is provided then streaming mode
			if isinstance(self.neuron_state, int): # t = 0

				if mask != None: # If mask present, do re-weighting
					re_scaled_potential, new_mask = self.re_weighting_streaming(convolved_potential, mask)
					# Add current potential to the neuron memory
					output = (re_scaled_potential + self.bias.view(1, self.out_channels, 1, 1, 1)) if self.bias != None else re_scaled_potential
					# Decay for next timestep and override the memory (permute to put the C dimension last and multiply each channel/kernel_output by its the decay)
					# Pass the decay weight through a sigmoid to set it between the accepted bounds of [0, 1]
					self.neuron_state = (convolved_potential.permute(0,2,3,4,1) * (1 - torch.sigmoid(self.decay_weight))).permute(0,4,1,2,3)
				else:
					# Add bias to convolved potential 
					output = (convolved_potential + self.bias.view(1, self.out_channels, 1, 1, 1)) if self.bias != None else convolved_potential
					# Decay for next timestep and override the memory (permute to put the C dimension last and multiply each channel/kernel_output by its the decay)
					# Pass the decay weight through a sigmoid to set it between the accepted bounds of [0, 1]
					self.neuron_state = (convolved_potential.permute(0,2,3,4,1) * (1 - torch.sigmoid(self.decay_weight))).permute(0,4,1,2,3)
				
			else: # t != 0
				# Sum of the spatially convolved input at t and the decayed state of t-1 (pre-adding bias)
				pre_bias = convolved_potential + self.neuron_state 
				
				if mask != None: # If mask present, do re-weighting
					re_scaled_potential, new_mask = self.re_weighting_streaming(pre_bias, mask)
					# Output is the sum of the convolved potential, current neuron state and bias         
					output = (re_scaled_potential + self.bias.view(1, self.out_channels, 1, 1, 1)) if self.bias != None else re_scaled_potential
					# Decay for next timestep and override the memory (permute to put the C dimension last and multiply each channel/kernel_output by its the decay)
					# Pass the decay weight through a sigmoid to set it between the accepted bounds of [0, 1]
					self.neuron_state = (pre_bias.permute(0,2,3,4,1) * (1 - torch.sigmoid(self.decay_weight))).permute(0,4,1,2,3)
				else:
					# Output is the sum of the convolved potential, current neuron state and bias         
					output = (pre_bias + self.bias.view(1, self.out_channels, 1, 1, 1)) if self.bias != None else pre_bias
					# Decay for next timestep and override the memory (permute to put the C dimension last and multiply each channel/kernel_output by its the decay)
					# Pass the decay weight through a sigmoid to set it between the accepted bounds of [0, 1]
					self.neuron_state = (pre_bias.permute(0,2,3,4,1) * (1 - torch.sigmoid(self.decay_weight))).permute(0,4,1,2,3)

		return output, new_mask if mask != None else None # If no mask in the input then no mask in the output. If running on dense mode the correct output is convolved_potential

	
	# ----------------------------------------------------------------------------------------------
	# Output re-weighting for partial convolutions (dense mode)
	def re_weighting_dense(self, x: torch.Tensor, extended_weight: torch.Tensor, mask: torch.Tensor):   
		
		with torch.no_grad(): # No need for gradients in the following operations?
			# Our definition of re-weighting value is calculated as -> (sum(kernel)/sum(unmasked_kernel))

			# Get per-filter sum of all elements (per-timestep) by convolving with a padded tensor of ones.
			padding = (0,0, 0,0, (x.shape[2]-1),0, 0,0) # two padding values per dimension, adding padding at the front of T dim = (n_bins - 1)
			#print(torch.ones(1,extended_weight.shape[1], x.shape[2],extended_weight.shape[3], extended_weight.shape[4]).shape)
			sum_kernel = F.conv3d(F.pad(torch.ones(1,extended_weight.shape[1], x.shape[2],extended_weight.shape[3], extended_weight.shape[4]), padding, value = 0).to(self.spatial_weight.device), extended_weight.abs()) 
			# Convolve the filters by the mask to get the sum of the unmasked elements per position (add padding at the beginning so only the right temporal slices are used for the re-weighting at every timestep)
			if mask.shape[1] == 1: # Only repeat the mask in the first timestep
				sum_unmasked = F.conv3d(F.pad(mask.repeat(1, extended_weight.shape[1], 1,1,1), padding, value = 0).to(self.spatial_weight.device), extended_weight.abs(), stride=self.stride, padding= self.padding) 
			else:
				sum_unmasked = F.conv3d(F.pad(mask, padding, value = 0).to(self.spatial_weight.device), extended_weight.abs(), stride=self.stride, padding= self.padding) 
			
		# Update mask (if a value was calculated from at least one unmasked element, mask = 1, else mask = 0)
		new_mask = torch.heaviside(torch.abs(sum_unmasked), torch.tensor(0, dtype= sum_unmasked.dtype))
			# Perform the division operation to get the re-weighting tensor (sum a really small constant to avoid division by 0)
		re_weight = (sum_kernel / (sum_unmasked + 1e-9)) 
		# Sum the convolved potential with its re-weighting values (maybe this does need gradients? not even sure but too many OOM problemsssss aaaaaaaa)
		re_weighted_output = x * re_weight

		return re_weighted_output, new_mask

	# ----------------------------------------------------------------------------------------------
	# Output re-weighting for partial convolutions (streaming mode) - 
	# Take into account scaling factors of previous timesteps to match the definition of dense mode
	def re_weighting_streaming(self, x: torch.Tensor, mask: torch.Tensor):   
	
		with torch.no_grad(): # No need for gradients in the following operations?
			# Our definition of re-weighting value is calculated as -> (sum(kernel)/sum(unmasked_kernel))
			# Get per-filter sum of all elements
			sum_kernel = torch.sum(self.spatial_weight,dim=[1,2,3,4]) 
			# Convolve the filters by the mask to get the sum of the unmasked elements per position
			if mask.shape[1] == 1: # Only repeat the mask in the first timestep
				sum_unmasked = F.conv3d(mask.repeat(1, self.spatial_weight.shape[1], 1,1,1).to(self.spatial_weight.device), self.spatial_weight,stride=self.stride, padding= self.padding) 
			else:
				sum_unmasked = F.conv3d(mask.to(self.spatial_weight.device), self.spatial_weight, stride=self.stride, padding= self.padding) 

			# Perform the division operation to get the re-weighting tensor
			# At t != 0, sum numerator and denominator with the layer's re-weighting memory to match dense mode, decay it and update layer's memory
			if self.sum_unmasked == None:
				re_weight = (sum_kernel / (sum_unmasked.permute(0,2,3,4,1) + 1e-9)).permute(0,4,1,2,3)       
				self.sum_kernel = (1 - self.decay_weight) * sum_kernel
				self.sum_unmasked = (1 - self.decay_weight.view(1,self.decay_weight.shape[0],1,1,1) )* sum_unmasked
			else:
				self.sum_kernel += sum_kernel
				self.sum_unmasked += sum_unmasked
				re_weight = (self.sum_kernel / (self.sum_unmasked.permute(0,2,3,4,1) + 1e-9)).permute(0,4,1,2,3) 
				self.sum_kernel *= (1 -self.decay_weight)
				self.sum_unmasked *= (1 - self.decay_weight.view(1,self.decay_weight.shape[0],1,1,1))

			# Update mask (if a value was calculated from at least one unmasked element, mask = 1, else mask = 0)
			#new_mask = torch.clip(torch.abs(self.sum_unmasked), 0,1)
			new_mask = torch.heaviside(torch.abs(self.sum_unmasked), torch.tensor(0, dtype= self.sum_unmasked.dtype))            
		
		# Sum the convolved potential with its re-weighting values
		re_weighted_output = x * re_weight

		return re_weighted_output, new_mask






# ================================================================================================
class Decay3Dx2new(torch.nn.modules.Module):
	"""This is alex's pytorch "decay efficient" re-implementation of the decay3d layer (with partial convolutions) presented in the paper: "EDeNN: Event Decay Neural Networks for low latency vision". 
		-   This version contains the new re-weighting value, equivalent to the original but with better stability -> mean(input) * sum(masked_kernel_items)
	Link: https://arxiv.org/abs/2209.04362

	The layer expects an input of the form (B,C,T,H,W) for [batch, n_channels, n_time_bins, Height, Width]
	
	Parameters
	----------
	in_channels:        Number of channels in the input 
	out_channels:       Number of learned filters in the layer or number of output channels
	kernel_size:        Size of the learned filters in the form (H, W).
	stride:             Stride of the convolution operation
	padding:            Stride of the convolution operation
	bias:               Add learned bias to the layer's filters (default -> True)
	n_bins:             How many slices of time will the decay operation take into account during dense mode (default -> None, the decay filter will be calculated to be of size T in the time dim)

	


	TODO:
		- apply the same conv+concat idea to avoid padding (better memory but maybe slower?)
		- change streaming mode to have the same re-weighting value
	"""

	# ----------------------------------------------------------------------------------------------
	def __init__(self, in_channels: int, out_channels: int, kernel_size: int or tuple(int, int), 
				stride: int or tuple(int, int, int) = 1, padding: int or tuple(int, int, int) = 0, bias: bool = True, n_bins: int = None):
		super().__init__()

		self.in_channels = in_channels
		self.out_channels = out_channels
		self.kernel_size = kernel_size if isinstance(kernel_size, collections.abc.Iterable) else (1, kernel_size, kernel_size)
		self.stride = stride if isinstance(stride, collections.abc.Iterable) else (1, stride, stride)
		self.padding = padding if isinstance(padding, collections.abc.Iterable) else (0, padding, padding)
		self.n_bins = n_bins
	
		# # Weights of the layer (both spatial and decay)
		self.spatial_weight = torch.nn.Parameter(torch.Tensor(out_channels, in_channels, *self.kernel_size))
		self.decay_weight = torch.nn.Parameter(torch.Tensor((out_channels)))

		# Add bias if necessary
		if bias:
			self.bias = torch.nn.Parameter(torch.Tensor(out_channels))
		else:
			self.bias = None
			self.register_parameter("bias", None)

		# Initialize parameters (This fixes NaNs?)
		self.reset_parameters()


		# '''
		#     Known weights and biases for testing (comment out above)
		# '''
		# # # Weights of the layer (both spatial and decay)
		# self.spatial_weight = torch.nn.Parameter(torch.full((out_channels, in_channels, *self.kernel_size),1,dtype=torch.float))
		# self.decay_weight = torch.nn.Parameter(torch.full([out_channels], 0.1))
		# if bias:
		#     self.bias = torch.nn.Parameter(torch.ones(out_channels))
		# else:
		#     self.bias = None
		#     #self.register_parameter("bias", None)
		# '''
		# #     Known weights and biases for testing (comment out above)
		# '''


		# Initialise the layer's neurons and partial convolution re-weighting state (for streaming mode)
		self.neuron_state = 0
		self.sum_unmasked = None
		self.sum_kernel = None


	# ----------------------------------------------------------------------------------------------
	def reset_parameters(self):
		# Use He initialisation for the layer weights' and biases' (taken directly from old edenn)
		torch.nn.init.kaiming_uniform_(self.spatial_weight, a=math.sqrt(5))
		torch.nn.init.kaiming_uniform_(self.decay_weight.unsqueeze(0), a=math.sqrt(5))

		#self.spatial_weight = torch.nn.Parameter(torch.abs(self.spatial_weight))

		#torch.nn.init.normal_(self.spatial_weight, mean=0.5, std=0.5)
		#torch.nn.init.normal_(self.decay_weight.unsqueeze(0),mean=0.5, std=0.5)
		if self.bias is not None:
			# fan_in, _ = torch.nn.init._calculate_fan_in_and_fan_out(self.spatial_weight)
			# bound = 1 / math.sqrt(fan_in)
			# torch.nn.init.uniform_(self.bias, -bound, bound)
			#torch.nn.init.xavier_uniform_(self.bias.unsqueeze(0), gain=1.0)
			torch.nn.init.ones_(self.bias)
			#torch.nn.init.xavier_uniform_(self.bias, gain=1.0)

		# print(f"nans in the spatial weight: {torch.isnan(self.spatial_weight).any()}")
		# print(f"nans in the decay weight: {torch.isnan(self.decay_weight).any()}")


	# ----------------------------------------------------------------------------------------------
	def forward(self, x: torch.Tensor, mask: torch.Tensor = None):
		assert len(x.shape) == 5 # (B,C,T,H,W)

		# 0.1 Mask input if mask present
		if mask != None:
			x = x * mask 


		#== Dense mode (pre multiply the kernel by the decay value per timestep to only do 1 convolution instead of 2) ==#    
		if x.shape[2] != 1: # If more than one slice is provided then dense mode

			if self.n_bins == None: # Get number of timesteps provided and read them all, if not only read n timesteps at a time as specified in the input
				self.n_bins = x.shape[2] 

			# Set how many timesteps will the dense mode take into account. Also reverse them so they go from t0 to T.
			timesteps = torch.t(torch.flip(torch.arange(self.n_bins),(0,)))

			# repeat the same timesteps per out-channel (per-filter)
			#dense_timesteps = torch.t(torch.tile(timesteps, (self.spatial_weight.shape[0],1)))

			# Decay broadcasting to get decay array of [dec^(T-1), dec^(T-2), ..., dec^(1), dec^(0)].T per filter 
			# Pass the decay weight through a sigmoid to set it between the accepted bounds of [0, 1]
			decay_filters = torch.t(torch.pow((1-torch.sigmoid(self.decay_weight)), timesteps.unsqueeze(1).to(self.decay_weight.device)))

			# Multiply decay values by expanded spatial kernel for efficienty
			# Expand spatial weight on the time dimension to n_bins
			#exp_spatial = torch.tile(self.spatial_weight, (1,1,self.n_bins,1,1))

			#print(self.spatial_weight.shape)
			# Expand the decay filters in the spatial dimensions to match extended kernel
			#exp_dec_filters = torch.tile(decay_filters.reshape(self.out_channels,1,self.n_bins,1,1),(1,1,1,self.spatial_weight.shape[3],self.spatial_weight.shape[4]))
			#print(decay_filters.reshape(self.out_channels,1,self.n_bins,1,1).shape)
			# Multiply the expanded kernels by the expanded decay values
			#decayed_spatial_kernel = (exp_spatial * exp_dec_filters)

			# Use torch.mul to broadcast dimensions?
			decayed_spatial_kernel = torch.mul(self.spatial_weight, decay_filters.reshape(self.out_channels,1,self.n_bins,1,1))

			# Pad input to match with the expected output 
			padded_potential = F.pad(x, (0,0, 0,0, (self.n_bins-1),0, 0,0), value=0)

		# 1. Spatial convolution
		if x.shape[2] != 1: # If dense mode
			output = F.conv3d(padded_potential,decayed_spatial_kernel, bias = None, stride= self.stride, padding=self.padding)


			if mask != None: # If mask present, apply the re-weighting
				output, new_mask = self.re_weighting_dense(x, output, decayed_spatial_kernel, mask)
			
			# Add bias to the re-weighted output if present
			output = (output + self.bias.view(1, self.out_channels, 1, 1, 1)) if self.bias != None else output
		else: # If Streaming mode (apply spatial convolution without bias and add it after the decay operation and re-weighting) 
			convolved_potential = F.conv3d(x,self.spatial_weight, bias = None, stride= self.stride, padding=self.padding)

		#== Streaming mode ==#  This new streaming mode only adds the bias to the output to match the efficient formulation of dense mode    
		if x.shape[2] == 1: # If only one slice is provided then streaming mode
			if isinstance(self.neuron_state, int): # t = 0

				if mask != None: # If mask present, do re-weighting
					re_scaled_potential, new_mask = self.re_weighting_streaming(convolved_potential, mask)
					# Add current potential to the neuron memory
					output = (re_scaled_potential + self.bias.view(1, self.out_channels, 1, 1, 1)) if self.bias != None else re_scaled_potential
					# Decay for next timestep and override the memory (permute to put the C dimension last and multiply each channel/kernel_output by its the decay)
					# Pass the decay weight through a sigmoid to set it between the accepted bounds of [0, 1]
					self.neuron_state = (convolved_potential.permute(0,2,3,4,1) * (1 - torch.sigmoid(self.decay_weight))).permute(0,4,1,2,3)
				else:
					# Add bias to convolved potential 
					output = (convolved_potential + self.bias.view(1, self.out_channels, 1, 1, 1)) if self.bias != None else convolved_potential
					# Decay for next timestep and override the memory (permute to put the C dimension last and multiply each channel/kernel_output by its the decay)
					# Pass the decay weight through a sigmoid to set it between the accepted bounds of [0, 1]
					self.neuron_state = (convolved_potential.permute(0,2,3,4,1) * (1 - torch.sigmoid(self.decay_weight))).permute(0,4,1,2,3)
				
			else: # t != 0
				# Sum of the spatially convolved input at t and the decayed state of t-1 (pre-adding bias)
				pre_bias = convolved_potential + self.neuron_state 
				
				if mask != None: # If mask present, do re-weighting
					re_scaled_potential, new_mask = self.re_weighting_streaming(pre_bias, mask)
					# Output is the sum of the convolved potential, current neuron state and bias         
					output = (re_scaled_potential + self.bias.view(1, self.out_channels, 1, 1, 1)) if self.bias != None else re_scaled_potential
					# Decay for next timestep and override the memory (permute to put the C dimension last and multiply each channel/kernel_output by its the decay)
					# Pass the decay weight through a sigmoid to set it between the accepted bounds of [0, 1]
					self.neuron_state = (pre_bias.permute(0,2,3,4,1) * (1 - torch.sigmoid(self.decay_weight))).permute(0,4,1,2,3)
				else:
					# Output is the sum of the convolved potential, current neuron state and bias         
					output = (pre_bias + self.bias.view(1, self.out_channels, 1, 1, 1)) if self.bias != None else pre_bias
					# Decay for next timestep and override the memory (permute to put the C dimension last and multiply each channel/kernel_output by its the decay)
					# Pass the decay weight through a sigmoid to set it between the accepted bounds of [0, 1]
					self.neuron_state = (pre_bias.permute(0,2,3,4,1) * (1 - torch.sigmoid(self.decay_weight))).permute(0,4,1,2,3)

		return output, new_mask if mask != None else None # If no mask in the input then no mask in the output. If running on dense mode the correct output is convolved_potential

	
	# ----------------------------------------------------------------------------------------------
	# Output re-weighting for partial convolutions (dense mode)
	def re_weighting_dense(self, x: torch.Tensor, to_reweight: torch.Tensor, extended_weight: torch.Tensor, mask: torch.Tensor):   

		with torch.no_grad(): # No need for gradients in the following operations?
			# Our definition of re-weighting value is calculated as -> mean(unmasked_input_elements) * sum(masked_kernel_elements)

			# Get per-filter sum of all masked elements (per-timestep) by convolving with a padded mask.
			padding = (0,0, 0,0, (x.shape[2]-1),0, 0,0) # two padding values per dimension, adding padding at the front of T dim = (n_bins - 1)
			# Repeat negated mask for every kernel and pad it temporally 
			if mask.shape[1] == 1: # Only repeat the mask in the first timestep
				sum_masked_kernel = F.conv3d(F.pad((mask - 1).abs().repeat(1,extended_weight.shape[1], 1,1,1), padding, value = 0).to(extended_weight.device), extended_weight, stride=self.stride, padding = self.padding)
				# Update mask (if a value was calculated from at least one unmasked element, mask = 1, else mask = 0)
				new_mask = torch.heaviside(torch.abs(F.conv3d(F.pad(mask.repeat(1,extended_weight.shape[1], 1,1,1), padding, value = 0).to(extended_weight.device), extended_weight, stride=self.stride, padding = self.padding)), torch.tensor(0, dtype= sum_masked_kernel.dtype))
			else:
				sum_masked_kernel = F.conv3d(F.pad((mask - 1).abs(), padding, value = 0).to(extended_weight.device), extended_weight, stride=self.stride, padding = self.padding)
				new_mask = torch.heaviside(torch.abs(F.conv3d(F.pad(mask, padding, value = 0).to(extended_weight.device), extended_weight, stride=self.stride, padding = self.padding)), torch.tensor(0, dtype= sum_masked_kernel.dtype))
			
			# you can calculate the mean of a (C, T, H, W) patch by convolving it with a (C, T, H, W) kernel full of (1/(C x T x H x W))
			# As for every output timestep t, the efficient dense mode takes into account t timesteps at once, we have to ajust the number we use for our kernel 
			mean_list = [1/((extended_weight.shape[3]*extended_weight.shape[4]*x.shape[1])*(1+f)) for f in range(x.shape[2])]

			# calculate the mean of every (3x3xCxn_timesteps) neighbourhood then concatenate result on the t dimension
			outputs_to_concat = []
			for i, mean_val in enumerate(mean_list):
				outputs_to_concat.append(F.conv3d(x[:,:,:i+1,:,:], torch.full([extended_weight.shape[0],x.shape[1],i+1,extended_weight.shape[3],extended_weight.shape[4]],mean_val,device=x.device), stride=self.stride, padding = self.padding)) # cat on the t dim 
			mean_input = torch.cat(outputs_to_concat,dim=2) # Concatenate timesteps
			
		# Perform the multiplication operation to get the re-weighting value
		re_weight = sum_masked_kernel * mean_input
		# Sum the convolved potential with its re-weighting values
		re_weighted_output = to_reweight + re_weight

		return re_weighted_output, new_mask

	# ----------------------------------------------------------------------------------------------
	# Output re-weighting for partial convolutions (streaming mode) - 
	# Take into account scaling factors of previous timesteps to match the definition of dense mode
	def re_weighting_streaming(self, x: torch.Tensor, mask: torch.Tensor):   
	
		with torch.no_grad(): # No need for gradients in the following operations?
			# Our definition of re-weighting value is calculated as -> (sum(kernel)/sum(unmasked_kernel))
			# Get per-filter sum of all elements
			sum_kernel = torch.sum(self.spatial_weight,dim=[1,2,3,4]) 
			# Convolve the filters by the mask to get the sum of the unmasked elements per position
			if mask.shape[1] == 1: # Only repeat the mask in the first timestep
				sum_unmasked = F.conv3d(mask.repeat(1, self.spatial_weight.shape[1], 1,1,1).to(self.spatial_weight.device), self.spatial_weight,stride=self.stride, padding= self.padding) 
			else:
				sum_unmasked = F.conv3d(mask.to(self.spatial_weight.device), self.spatial_weight, stride=self.stride, padding= self.padding) 

			# Perform the division operation to get the re-weighting tensor
			# At t != 0, sum numerator and denominator with the layer's re-weighting memory to match dense mode, decay it and update layer's memory
			if self.sum_unmasked == None:
				re_weight = (sum_kernel / (sum_unmasked.permute(0,2,3,4,1) + 1e-9)).permute(0,4,1,2,3)       
				self.sum_kernel = (1 - self.decay_weight) * sum_kernel
				self.sum_unmasked = (1 - self.decay_weight.view(1,self.decay_weight.shape[0],1,1,1) )* sum_unmasked
			else:
				self.sum_kernel += sum_kernel
				self.sum_unmasked += sum_unmasked
				re_weight = (self.sum_kernel / (self.sum_unmasked.permute(0,2,3,4,1) + 1e-9)).permute(0,4,1,2,3) 
				self.sum_kernel *= (1 -self.decay_weight)
				self.sum_unmasked *= (1 - self.decay_weight.view(1,self.decay_weight.shape[0],1,1,1))

			# Update mask (if a value was calculated from at least one unmasked element, mask = 1, else mask = 0)
			#new_mask = torch.clip(torch.abs(self.sum_unmasked), 0,1)
			new_mask = torch.heaviside(torch.abs(self.sum_unmasked), torch.tensor(0, dtype= self.sum_unmasked.dtype))            
		
		# Sum the convolved potential with its re-weighting values
		re_weighted_output = x * re_weight

		return re_weighted_output, new_mask








#--------------------------------------------------------------------------------------------------------------------------------------------------------------------
#--------------------------------------------------------------------------------------------------------------------------------------------------------------------
#-----------------------------------------------------------------Alternative implementations------------------------------------------------------------------------
#--------------------------------------------------------------------------------------------------------------------------------------------------------------------

class Decay3Dxloop(torch.nn.modules.Module):
	"""This is alex's pytorch "looped-dense" re-implementation of the decay3d layer (with partial convolutions) presented in the paper: "EDeNN: Event Decay Neural Networks for low latency vision". 
	Link: https://arxiv.org/abs/2209.04362

	The layer expects an input of the form (B,C,T,H,W) for [batch, n_channels, n_time_bins, Height, Width]
	
	Parameters
	----------
	in_channels:        Number of channels in the input 
	out_channels:       Number of learned filters in the layer or number of output channels
	kernel_size:        Size of the learned filters in the form (H, W).
	stride:             Stride of the convolution operation
	padding:            Stride of the convolution operation
	bias:               Add learned bias to the layer's filters (default -> True)
	n_bins:             How many slices of time will the decay operation take into account during dense mode (default -> None, the decay filter will be calculated to be of size T in the time dim)
	"""

	# ----------------------------------------------------------------------------------------------
	def __init__(self, in_channels: int, out_channels: int, kernel_size: int or tuple(int, int), 
				stride: int or tuple(int, int, int) = 1, padding: int or tuple(int, int, int) = 0, bias: bool = True, n_bins: int = None):
		super().__init__()

		self.in_channels = in_channels
		self.out_channels = out_channels
		self.kernel_size = kernel_size if isinstance(kernel_size, collections.abc.Iterable) else (1, kernel_size, kernel_size)
		self.stride = stride if isinstance(stride, collections.abc.Iterable) else (1, stride, stride)
		self.padding = padding if isinstance(padding, collections.abc.Iterable) else (0, padding, padding)
		self.n_bins = n_bins
	
		# Weights of the layer (both spatial and decay)
		self.spatial_weight = torch.nn.Parameter(torch.Tensor(out_channels, in_channels, *self.kernel_size))
		self.decay_weight = torch.nn.Parameter(torch.Tensor((out_channels)))

		# # Add bias if necessary
		# if bias:
		#     self.bias = torch.nn.Parameter(torch.Tensor(out_channels))
		# else:
		#     self.bias = None
		#     self.register_parameter("bias", None)

		# # Initialize parameters (This fixes NaNs?)
		# self.reset_parameters()


		'''
			Known weights and biases for testing (comment out above)
		'''
		# # Weights of the layer (both spatial and decay)
		self.spatial_weight = torch.nn.Parameter(torch.full((out_channels, in_channels, *self.kernel_size),2,dtype=torch.float))
		self.decay_weight = torch.nn.Parameter(torch.full([out_channels], 0.1))
		# Add bias if necessary
		if bias:
			self.bias = torch.nn.Parameter(torch.ones(out_channels))
		else:
			self.bias = None
			#self.register_parameter("bias", None)
		'''
			Known weights and biases for testing (comment out above)
		'''

		# Initialise the layer's neurons state (for streaming mode)
		self.neuron_state = 0


	# ----------------------------------------------------------------------------------------------
	def reset_parameters(self):
		# Use He initialisation for the layer weights' and biases' (taken directly from old edenn)
		# torch.nn.init.kaiming_uniform_(self.spatial_weight, a=math.sqrt(5))
		# torch.nn.init.kaiming_uniform_(self.decay_weight.unsqueeze(0), a=math.sqrt(5))

		torch.nn.init.xavier_uniform_(self.spatial_weight, gain=1.0)
		torch.nn.init.xavier_uniform_(self.decay_weight.unsqueeze(0), gain=1.0)
		if self.bias is not None:
			# fan_in, _ = torch.nn.init._calculate_fan_in_and_fan_out(self.spatial_weight)
			# bound = 1 / math.sqrt(fan_in)
			# torch.nn.init.uniform_(self.bias, -bound, bound)
			#torch.nn.init.zeros_(self.bias)
			torch.nn.init.xavier_uniform_(self.bias, gain=1.0)


	# ----------------------------------------------------------------------------------------------
	def forward(self, x: torch.Tensor, mask: Optional[torch.Tensor] = None):
		assert len(x.shape) == 5 # (B,C,T,H,W)

		# 0.1 Mask input if mask present (although if the input mask means that no events are present then the input would be 0 there anyways?)
		if mask != None:
			x = x * mask 
			

		# 1. Spatial convolution
		convolved_potential = F.conv3d(x,self.spatial_weight, bias = None, stride= self.stride, padding=self.padding)

		# 1.1 re-weighting
		if mask != None:
			# Each element in the convolved potential tensor should be re-weighted by (sum(kernel)/sum(unmasked_kernel))
			convolved_potential, new_mask = self.re_weighting(convolved_potential, mask)

		#== Dense mode (pre multiply the kernel by the decay value per timestep to only do 1 convolution instead of 2) ==#    
		if x.shape[2] != 1: # If more than one slice is provided then dense mode
			for i in range(1, list(convolved_potential.shape)[2]):
				convolved_potential[:, :, i] = convolved_potential[:, :, i] + (convolved_potential[:, :, i - 1].permute(0,2,3,1) * (1 - torch.sigmoid(self.decay_weight))).permute(0,3,1,2)

			# Set output equal to the decayed convolved potential + the learned bias
			if self.bias != None:
				output = convolved_potential + self.bias.view(1, self.out_channels, 1, 1, 1)
			else:
				output = convolved_potential


		#== Streaming mode ==#  This new streaming mode only adds the bias to the output to match the efficient formulation of dense mode    
		if x.shape[2] == 1: # If only one slice is provided then streaming mode
			if isinstance(self.neuron_state, int):
				# Add current potential to the neuron memory
				output = (convolved_potential + self.bias.view(1, self.out_channels, 1, 1, 1)) if self.bias != None else convolved_potential
				# Decay for next timestep and override the memory (permute to put the C dimension last and multiply each channel/kernel_output by its the decay)
				self.neuron_state = (convolved_potential.permute(0,2,3,4,1) * (1 - self.decay_weight)).permute(0,4,1,2,3)

			else:
				# Sum of the spatially convolved input at t and the decayed state of t-1 (pre-adding bias)
				pre_bias = convolved_potential + self.neuron_state 
				# Output is the sum of the convolved potential, current neuron state and bias         
				output = (pre_bias + self.bias.view(1, self.out_channels, 1, 1, 1)) if self.bias != None else pre_bias
				# Decay for next timestep and override the memory (permute to put the C dimension last and multiply each channel/kernel_output by its the decay)
				self.neuron_state = (pre_bias.permute(0,2,3,4,1) * (1 - self.decay_weight)).permute(0,4,1,2,3)


		return output , new_mask if mask != None else None # If no mask in the input then no mask in the output. If running on dense mode the correct output is convolved_potential

	
	# ----------------------------------------------------------------------------------------------
	# Output re-weighting for partial convolutions
	def re_weighting(self, x: torch.Tensor, mask: Optional[torch.Tensor] = None):   

		# Pur definition of re-weighting value is calculated as -> (sum(kernel)/sum(unmasked_kernel))

		with torch.no_grad():
			# Get per-filter sum of all elements
			sum_kernel = torch.sum(self.spatial_weight,dim=[1,2,3,4]) 
			# Convolve the filters by the mask to get the sum of the unmasked elements per position
			sum_unmasked = F.conv3d(mask.repeat(1, self.spatial_weight.shape[1], 1,1,1).to(self.spatial_weight.device), self.spatial_weight) 
			# Perform the division operation to get the re-weighting tensor
			re_weight = (sum_kernel / sum_unmasked.permute(0,2,3,4,1)).permute(0,4,1,2,3)       

			# Update mask (if a value was calculated from at least one unmasked element, mask = 1, else mask = 0)
			new_mask = torch.clip(torch.abs(sum_unmasked), 0,1)

		# Sum the convolved potential with its re-weighting values
		re_weighted_output = x * re_weight


		return re_weighted_output, new_mask
	








# ================================================================================================
class Decay3Dx2newdense(torch.nn.modules.Module):
	"""This is alex's pytorch "decay efficient" re-implementation of the decay3d layer (with partial convolutions) presented in the paper: "EDeNN: Event Decay Neural Networks for low latency vision". 
		-   This version contains the new re-weighting value, equivalent to the original but with better stability -> mean(input) * sum(masked_kernel_items)
		-   This version also contains a new reformulation of dense mode with no padding, using the cat function
	Link: https://arxiv.org/abs/2209.04362

	The layer expects an input of the form (B,C,T,H,W) for [batch, n_channels, n_time_bins, Height, Width]
	
	Parameters
	----------
	in_channels:        Number of channels in the input 
	out_channels:       Number of learned filters in the layer or number of output channels
	kernel_size:        Size of the learned filters in the form (H, W).
	stride:             Stride of the convolution operation
	padding:            Stride of the convolution operation
	bias:               Add learned bias to the layer's filters (default -> True)
	n_bins:             How many slices of time will the decay operation take into account during dense mode (default -> None, the decay filter will be calculated to be of size T in the time dim)

	


	TODO:
		- change streaming mode to have the same re-weighting value
	"""

	# ----------------------------------------------------------------------------------------------
	def __init__(self, in_channels: int, out_channels: int, kernel_size: int or tuple(int, int), 
				stride: int or tuple(int, int, int) = 1, padding: int or tuple(int, int, int) = 0, bias: bool = True, n_bins: int = None):
		super().__init__()

		self.in_channels = in_channels
		self.out_channels = out_channels
		self.kernel_size = kernel_size if isinstance(kernel_size, collections.abc.Iterable) else (1, kernel_size, kernel_size)
		self.stride = stride if isinstance(stride, collections.abc.Iterable) else (1, stride, stride)
		self.padding = padding if isinstance(padding, collections.abc.Iterable) else (0, padding, padding)
		self.n_bins = n_bins
	
		# # Weights of the layer (both spatial and decay)
		self.spatial_weight = torch.nn.Parameter(torch.Tensor(out_channels, in_channels, *self.kernel_size))
		self.decay_weight = torch.nn.Parameter(torch.Tensor((out_channels)))

		# Add bias if necessary
		if bias:
			self.bias = torch.nn.Parameter(torch.Tensor(out_channels))
		else:
			self.bias = None
			self.register_parameter("bias", None)

		# Initialize parameters (This fixes NaNs?)
		self.reset_parameters()


		# '''
		#     Known weights and biases for testing (comment out above)
		# '''
		# # # Weights of the layer (both spatial and decay)
		# self.spatial_weight = torch.nn.Parameter(torch.full((out_channels, in_channels, *self.kernel_size),1,dtype=torch.float))
		# self.decay_weight = torch.nn.Parameter(torch.full([out_channels], 0.1))
		# if bias:
		#     self.bias = torch.nn.Parameter(torch.ones(out_channels))
		# else:
		#     self.bias = None
		#     #self.register_parameter("bias", None)
		# '''
		# #     Known weights and biases for testing (comment out above)
		# '''


		# Initialise the layer's neurons and partial convolution re-weighting state (for streaming mode)
		self.neuron_state = 0
		self.sum_unmasked = None
		self.sum_kernel = None


	# ----------------------------------------------------------------------------------------------
	def reset_parameters(self):
		# Use He initialisation for the layer weights' and biases' (taken directly from old edenn)
		torch.nn.init.kaiming_uniform_(self.spatial_weight, a=math.sqrt(5))
		torch.nn.init.kaiming_uniform_(self.decay_weight.unsqueeze(0), a=math.sqrt(5))

		#self.spatial_weight = torch.nn.Parameter(torch.abs(self.spatial_weight))

		#torch.nn.init.normal_(self.spatial_weight, mean=0.5, std=0.5)
		#torch.nn.init.normal_(self.decay_weight.unsqueeze(0),mean=0.5, std=0.5)
		if self.bias is not None:
			# fan_in, _ = torch.nn.init._calculate_fan_in_and_fan_out(self.spatial_weight)
			# bound = 1 / math.sqrt(fan_in)
			# torch.nn.init.uniform_(self.bias, -bound, bound)
			#torch.nn.init.xavier_uniform_(self.bias.unsqueeze(0), gain=1.0)
			torch.nn.init.ones_(self.bias)
			#torch.nn.init.xavier_uniform_(self.bias, gain=1.0)

		# print(f"nans in the spatial weight: {torch.isnan(self.spatial_weight).any()}")
		# print(f"nans in the decay weight: {torch.isnan(self.decay_weight).any()}")


	# ----------------------------------------------------------------------------------------------
	def forward(self, x: torch.Tensor, mask: torch.Tensor = None):
		assert len(x.shape) == 5 # (B,C,T,H,W)

		# 0.1 Mask input if mask present
		if mask != None:
			x = x * mask 


		#== Dense mode (pre multiply the kernel by the decay value per timestep to only do 1 convolution instead of 2) ==#    
		if x.shape[2] != 1: # If more than one slice is provided then dense mode

			if self.n_bins == None: # Get number of timesteps provided and read them all, if not only read n timesteps at a time as specified in the input
				self.n_bins = x.shape[2] 

			# Set how many timesteps will the dense mode take into account. Also reverse them so they go from t0 to T.
			timesteps = torch.t(torch.flip(torch.arange(self.n_bins),(0,)))

			# repeat the same timesteps per out-channel (per-filter)
			#dense_timesteps = torch.t(torch.tile(timesteps, (self.spatial_weight.shape[0],1)))

			# Decay broadcasting to get decay array of [dec^(T-1), dec^(T-2), ..., dec^(1), dec^(0)].T per filter 
			# Pass the decay weight through a sigmoid to set it between the accepted bounds of [0, 1]
			decay_filters = torch.t(torch.pow((1-torch.sigmoid(self.decay_weight)), timesteps.unsqueeze(1).to(self.decay_weight.device)))

			# Multiply decay values by expanded spatial kernel for efficienty
			# Expand spatial weight on the time dimension to n_bins
			#exp_spatial = torch.tile(self.spatial_weight, (1,1,self.n_bins,1,1))

			#print(self.spatial_weight.shape)
			# Expand the decay filters in the spatial dimensions to match extended kernel
			#exp_dec_filters = torch.tile(decay_filters.reshape(self.out_channels,1,self.n_bins,1,1),(1,1,1,self.spatial_weight.shape[3],self.spatial_weight.shape[4]))
			#print(decay_filters.reshape(self.out_channels,1,self.n_bins,1,1).shape)
			# Multiply the expanded kernels by the expanded decay values
			#decayed_spatial_kernel = (exp_spatial * exp_dec_filters)

			# Use torch.mul to broadcast dimensions?
			decayed_spatial_kernel = torch.mul(self.spatial_weight, decay_filters.reshape(self.out_channels,1,self.n_bins,1,1))

			# # Pad input to match with the expected output 
			# padded_potential = F.pad(x, (0,0, 0,0, (self.n_bins-1),0, 0,0), value=0)

		# 1. Spatial convolution
		if x.shape[2] != 1: # If dense mode

			output_to_concat = []
			for i in range(self.n_bins):
				output_to_concat.append(F.conv3d(x[:,:,:i+1,:,:],decayed_spatial_kernel[:,:,:i+1,:,:], bias = None, stride= self.stride, padding=self.padding))
			output = torch.cat(output_to_concat,dim=2) # Concatenate all output timesteps in the t dim

			if mask != None: # If mask present, apply the re-weighting
				output, new_mask = self.re_weighting_dense(x, output, decayed_spatial_kernel, mask)
			
			# Add bias to the re-weighted output if present
			output = (output + self.bias.view(1, self.out_channels, 1, 1, 1)) if self.bias != None else output
		else: # If Streaming mode (apply spatial convolution without bias and add it after the decay operation and re-weighting) 
			convolved_potential = F.conv3d(x,self.spatial_weight, bias = None, stride= self.stride, padding=self.padding)

		#== Streaming mode ==#  This new streaming mode only adds the bias to the output to match the efficient formulation of dense mode    
		if x.shape[2] == 1: # If only one slice is provided then streaming mode
			if isinstance(self.neuron_state, int): # t = 0

				if mask != None: # If mask present, do re-weighting
					re_scaled_potential, new_mask = self.re_weighting_streaming(convolved_potential, mask)
					# Add current potential to the neuron memory
					output = (re_scaled_potential + self.bias.view(1, self.out_channels, 1, 1, 1)) if self.bias != None else re_scaled_potential
					# Decay for next timestep and override the memory (permute to put the C dimension last and multiply each channel/kernel_output by its the decay)
					# Pass the decay weight through a sigmoid to set it between the accepted bounds of [0, 1]
					self.neuron_state = (convolved_potential.permute(0,2,3,4,1) * (1 - torch.sigmoid(self.decay_weight))).permute(0,4,1,2,3)
				else:
					# Add bias to convolved potential 
					output = (convolved_potential + self.bias.view(1, self.out_channels, 1, 1, 1)) if self.bias != None else convolved_potential
					# Decay for next timestep and override the memory (permute to put the C dimension last and multiply each channel/kernel_output by its the decay)
					# Pass the decay weight through a sigmoid to set it between the accepted bounds of [0, 1]
					self.neuron_state = (convolved_potential.permute(0,2,3,4,1) * (1 - torch.sigmoid(self.decay_weight))).permute(0,4,1,2,3)
				
			else: # t != 0
				# Sum of the spatially convolved input at t and the decayed state of t-1 (pre-adding bias)
				pre_bias = convolved_potential + self.neuron_state 
				
				if mask != None: # If mask present, do re-weighting
					re_scaled_potential, new_mask = self.re_weighting_streaming(pre_bias, mask)
					# Output is the sum of the convolved potential, current neuron state and bias         
					output = (re_scaled_potential + self.bias.view(1, self.out_channels, 1, 1, 1)) if self.bias != None else re_scaled_potential
					# Decay for next timestep and override the memory (permute to put the C dimension last and multiply each channel/kernel_output by its the decay)
					# Pass the decay weight through a sigmoid to set it between the accepted bounds of [0, 1]
					self.neuron_state = (pre_bias.permute(0,2,3,4,1) * (1 - torch.sigmoid(self.decay_weight))).permute(0,4,1,2,3)
				else:
					# Output is the sum of the convolved potential, current neuron state and bias         
					output = (pre_bias + self.bias.view(1, self.out_channels, 1, 1, 1)) if self.bias != None else pre_bias
					# Decay for next timestep and override the memory (permute to put the C dimension last and multiply each channel/kernel_output by its the decay)
					# Pass the decay weight through a sigmoid to set it between the accepted bounds of [0, 1]
					self.neuron_state = (pre_bias.permute(0,2,3,4,1) * (1 - torch.sigmoid(self.decay_weight))).permute(0,4,1,2,3)

		return output, new_mask if mask != None else None # If no mask in the input then no mask in the output. If running on dense mode the correct output is convolved_potential

	
	# ----------------------------------------------------------------------------------------------
	# Output re-weighting for partial convolutions (dense mode)
	def re_weighting_dense(self, x: torch.Tensor, to_reweight: torch.Tensor, extended_weight: torch.Tensor, mask: torch.Tensor):   

		with torch.no_grad(): # No need for gradients in the following operations?
			# Our definition of re-weighting value is calculated as -> mean(unmasked_input_elements) * sum(masked_kernel_elements)

			# Get per-filter sum of all masked elements (per-timestep) by convolving with a padded mask.
			padding = (0,0, 0,0, (x.shape[2]-1),0, 0,0) # two padding values per dimension, adding padding at the front of T dim = (n_bins - 1)
			# Repeat negated mask for every kernel and pad it temporally 
			if mask.shape[1] == 1: # Only repeat the mask in the first timestep
				sum_masked_kernel = F.conv3d(F.pad((mask - 1).abs().repeat(1,extended_weight.shape[1], 1,1,1), padding, value = 0).to(extended_weight.device), extended_weight, stride=self.stride, padding = self.padding)
				# Update mask (if a value was calculated from at least one unmasked element, mask = 1, else mask = 0)
				new_mask = torch.heaviside(torch.abs(F.conv3d(F.pad(mask.repeat(1,extended_weight.shape[1], 1,1,1), padding, value = 0).to(extended_weight.device), extended_weight, stride=self.stride, padding = self.padding)), torch.tensor(0, dtype= sum_masked_kernel.dtype))
			else:
				sum_masked_kernel = F.conv3d(F.pad((mask - 1).abs(), padding, value = 0).to(extended_weight.device), extended_weight, stride=self.stride, padding = self.padding)
				new_mask = torch.heaviside(torch.abs(F.conv3d(F.pad(mask, padding, value = 0).to(extended_weight.device), extended_weight, stride=self.stride, padding = self.padding)), torch.tensor(0, dtype= sum_masked_kernel.dtype))
			
			# you can calculate the mean of a (C, T, H, W) patch by convolving it with a (C, T, H, W) kernel full of (1/(C x T x H x W))
			# As for every output timestep t, the efficient dense mode takes into account t timesteps at once, we have to ajust the number we use for our kernel 
			mean_list = [1/((extended_weight.shape[3]*extended_weight.shape[4]*x.shape[1])*(1+f)) for f in range(x.shape[2])]

			# calculate the mean of every (3x3xCxn_timesteps) neighbourhood then concatenate result on the t dimension
			outputs_to_concat = []
			for i, mean_val in enumerate(mean_list):
				outputs_to_concat.append(F.conv3d(x[:,:,:i+1,:,:], torch.full([extended_weight.shape[0],x.shape[1],i+1,extended_weight.shape[3],extended_weight.shape[4]],mean_val,device=x.device), stride=self.stride, padding = self.padding)) # cat on the t dim 
			mean_input = torch.cat(outputs_to_concat,dim=2) # Concatenate timesteps
			
		# Perform the multiplication operation to get the re-weighting value
		re_weight = sum_masked_kernel * mean_input
		# Sum the convolved potential with its re-weighting values
		re_weighted_output = to_reweight + re_weight

		return re_weighted_output, new_mask

	# ----------------------------------------------------------------------------------------------
	# Output re-weighting for partial convolutions (streaming mode) - 
	# Take into account scaling factors of previous timesteps to match the definition of dense mode
	def re_weighting_streaming(self, x: torch.Tensor, mask: torch.Tensor):   
	
		with torch.no_grad(): # No need for gradients in the following operations?
			# Our definition of re-weighting value is calculated as -> (sum(kernel)/sum(unmasked_kernel))
			# Get per-filter sum of all elements
			sum_kernel = torch.sum(self.spatial_weight,dim=[1,2,3,4]) 
			# Convolve the filters by the mask to get the sum of the unmasked elements per position
			if mask.shape[1] == 1: # Only repeat the mask in the first timestep
				sum_unmasked = F.conv3d(mask.repeat(1, self.spatial_weight.shape[1], 1,1,1).to(self.spatial_weight.device), self.spatial_weight,stride=self.stride, padding= self.padding) 
			else:
				sum_unmasked = F.conv3d(mask.to(self.spatial_weight.device), self.spatial_weight, stride=self.stride, padding= self.padding) 

			# Perform the division operation to get the re-weighting tensor
			# At t != 0, sum numerator and denominator with the layer's re-weighting memory to match dense mode, decay it and update layer's memory
			if self.sum_unmasked == None:
				re_weight = (sum_kernel / (sum_unmasked.permute(0,2,3,4,1) + 1e-9)).permute(0,4,1,2,3)       
				self.sum_kernel = (1 - self.decay_weight) * sum_kernel
				self.sum_unmasked = (1 - self.decay_weight.view(1,self.decay_weight.shape[0],1,1,1) )* sum_unmasked
			else:
				self.sum_kernel += sum_kernel
				self.sum_unmasked += sum_unmasked
				re_weight = (self.sum_kernel / (self.sum_unmasked.permute(0,2,3,4,1) + 1e-9)).permute(0,4,1,2,3) 
				self.sum_kernel *= (1 -self.decay_weight)
				self.sum_unmasked *= (1 - self.decay_weight.view(1,self.decay_weight.shape[0],1,1,1))

			# Update mask (if a value was calculated from at least one unmasked element, mask = 1, else mask = 0)
			#new_mask = torch.clip(torch.abs(self.sum_unmasked), 0,1)
			new_mask = torch.heaviside(torch.abs(self.sum_unmasked), torch.tensor(0, dtype= self.sum_unmasked.dtype))            
		
		# Sum the convolved potential with its re-weighting values
		re_weighted_output = x * re_weight

		return re_weighted_output, new_mask
	





# ================================================================================================
class Decay3Dx2_no_partial(torch.nn.modules.Module):
	"""This is alex's pytorch "decay efficient" re-implementation of the decay3d layer (WITHOUT partial convolutions) presented in the paper: "EDeNN: Event Decay Neural Networks for low latency vision". 
		-   This version contains the new re-weighting value, equivalent to the original but with better stability -> mean(input) * sum(masked_kernel_items)
	Link: https://arxiv.org/abs/2209.04362

	The layer expects an input of the form (B,C,T,H,W) for [batch, n_channels, n_time_bins, Height, Width]
	
	Parameters
	----------
	in_channels:        Number of channels in the input 
	out_channels:       Number of learned filters in the layer or number of output channels
	kernel_size:        Size of the learned filters in the form (H, W).
	stride:             Stride of the convolution operation
	padding:            Stride of the convolution operation
	bias:               Add learned bias to the layer's filters (default -> True)
	n_bins:             How many slices of time will the decay operation take into account during dense mode (default -> None, the decay filter will be calculated to be of size T in the time dim)

	


	TODO:
		- apply the same conv+concat idea to avoid padding (better memory but maybe slower?)
		- change streaming mode to have the same re-weighting value
	"""

	# ----------------------------------------------------------------------------------------------
	def __init__(self, in_channels: int, out_channels: int, kernel_size: int or tuple(int, int), 
				stride: int or tuple(int, int, int) = 1, padding: int or tuple(int, int, int) = 0, bias: bool = True, n_bins: int = None):
		super().__init__()

		self.in_channels = in_channels
		self.out_channels = out_channels
		self.kernel_size = kernel_size if isinstance(kernel_size, collections.abc.Iterable) else (1, kernel_size, kernel_size)
		self.stride = stride if isinstance(stride, collections.abc.Iterable) else (1, stride, stride)
		self.padding = padding if isinstance(padding, collections.abc.Iterable) else (0, padding, padding)
		self.n_bins = n_bins
	
		# # Weights of the layer (both spatial and decay)
		self.spatial_weight = torch.nn.Parameter(torch.Tensor(out_channels, in_channels, *self.kernel_size))
		self.decay_weight = torch.nn.Parameter(torch.Tensor((out_channels)))

		# Add bias if necessary
		if bias:
			self.bias = torch.nn.Parameter(torch.Tensor(out_channels))
		else:
			self.bias = None
			self.register_parameter("bias", None)

		# Initialize parameters (This fixes NaNs?)
		self.reset_parameters()


		# '''
		#     Known weights and biases for testing (comment out above)
		# '''
		# # # Weights of the layer (both spatial and decay)
		# self.spatial_weight = torch.nn.Parameter(torch.full((out_channels, in_channels, *self.kernel_size),1,dtype=torch.float))
		# self.decay_weight = torch.nn.Parameter(torch.full([out_channels], 0.1))
		# if bias:
		#     self.bias = torch.nn.Parameter(torch.ones(out_channels))
		# else:
		#     self.bias = None
		#     #self.register_parameter("bias", None)
		# '''
		# #     Known weights and biases for testing (comment out above)
		# '''


		# Initialise the layer's neurons and partial convolution re-weighting state (for streaming mode)
		self.neuron_state = 0
		self.sum_unmasked = None
		self.sum_kernel = None


	# ----------------------------------------------------------------------------------------------
	def reset_parameters(self):
		# Use He initialisation for the layer weights' and biases' (taken directly from old edenn)
		torch.nn.init.kaiming_uniform_(self.spatial_weight, a=math.sqrt(5))
		torch.nn.init.kaiming_uniform_(self.decay_weight.unsqueeze(0), a=math.sqrt(5))

		#self.spatial_weight = torch.nn.Parameter(torch.abs(self.spatial_weight))

		#torch.nn.init.normal_(self.spatial_weight, mean=0.5, std=0.5)
		#torch.nn.init.normal_(self.decay_weight.unsqueeze(0),mean=0.5, std=0.5)
		if self.bias is not None:
			# fan_in, _ = torch.nn.init._calculate_fan_in_and_fan_out(self.spatial_weight)
			# bound = 1 / math.sqrt(fan_in)
			# torch.nn.init.uniform_(self.bias, -bound, bound)
			#torch.nn.init.xavier_uniform_(self.bias.unsqueeze(0), gain=1.0)
			torch.nn.init.ones_(self.bias)
			#torch.nn.init.xavier_uniform_(self.bias, gain=1.0)

		# print(f"nans in the spatial weight: {torch.isnan(self.spatial_weight).any()}")
		# print(f"nans in the decay weight: {torch.isnan(self.decay_weight).any()}")


	# ----------------------------------------------------------------------------------------------
	def forward(self, x: torch.Tensor, mask: torch.Tensor = None):
		assert len(x.shape) == 5 # (B,C,T,H,W)



		#== Dense mode (pre multiply the kernel by the decay value per timestep to only do 1 convolution instead of 2) ==#    
		if x.shape[2] != 1: # If more than one slice is provided then dense mode

			if self.n_bins == None: # Get number of timesteps provided and read them all, if not only read n timesteps at a time as specified in the input
				self.n_bins = x.shape[2] 

			# Set how many timesteps will the dense mode take into account. Also reverse them so they go from t0 to T.
			timesteps = torch.t(torch.flip(torch.arange(self.n_bins),(0,)))

			# repeat the same timesteps per out-channel (per-filter)
			#dense_timesteps = torch.t(torch.tile(timesteps, (self.spatial_weight.shape[0],1)))

			# Decay broadcasting to get decay array of [dec^(T-1), dec^(T-2), ..., dec^(1), dec^(0)].T per filter 
			# Pass the decay weight through a sigmoid to set it between the accepted bounds of [0, 1]
			decay_filters = torch.t(torch.pow((1-torch.sigmoid(self.decay_weight)), timesteps.unsqueeze(1).to(self.decay_weight.device)))

			# Multiply decay values by expanded spatial kernel for efficienty
			# Expand spatial weight on the time dimension to n_bins
			#exp_spatial = torch.tile(self.spatial_weight, (1,1,self.n_bins,1,1))

			#print(self.spatial_weight.shape)
			# Expand the decay filters in the spatial dimensions to match extended kernel
			#exp_dec_filters = torch.tile(decay_filters.reshape(self.out_channels,1,self.n_bins,1,1),(1,1,1,self.spatial_weight.shape[3],self.spatial_weight.shape[4]))
			#print(decay_filters.reshape(self.out_channels,1,self.n_bins,1,1).shape)
			# Multiply the expanded kernels by the expanded decay values
			#decayed_spatial_kernel = (exp_spatial * exp_dec_filters)

			# Use torch.mul to broadcast dimensions?
			decayed_spatial_kernel = torch.mul(self.spatial_weight, decay_filters.reshape(self.out_channels,1,self.n_bins,1,1))

			# Pad input to match with the expected output 
			padded_potential = F.pad(x, (0,0, 0,0, (self.n_bins-1),0, 0,0), value=0)

		# 1. Spatial convolution
		if x.shape[2] != 1: # If dense mode
			output = F.conv3d(padded_potential,decayed_spatial_kernel, bias = None, stride= self.stride, padding=self.padding)


			# if mask != None: # If mask present, apply the re-weighting
			# 	output, new_mask = self.re_weighting_dense(x, output, decayed_spatial_kernel, mask)
			
			# Add bias to the re-weighted output if present
			output = (output + self.bias.view(1, self.out_channels, 1, 1, 1)) if self.bias != None else output
		else: # If Streaming mode (apply spatial convolution without bias and add it after the decay operation and re-weighting) 
			convolved_potential = F.conv3d(x,self.spatial_weight, bias = None, stride= self.stride, padding=self.padding)

		#== Streaming mode ==#  This new streaming mode only adds the bias to the output to match the efficient formulation of dense mode    
		if x.shape[2] == 1: # If only one slice is provided then streaming mode
			if isinstance(self.neuron_state, int): # t = 0

				if mask != None: # If mask present, do re-weighting
					# re_scaled_potential, new_mask = self.re_weighting_streaming(convolved_potential, mask)
					# Add current potential to the neuron memory
					output = (convolved_potential + self.bias.view(1, self.out_channels, 1, 1, 1)) if self.bias != None else convolved_potential
					# Decay for next timestep and override the memory (permute to put the C dimension last and multiply each channel/kernel_output by its the decay)
					# Pass the decay weight through a sigmoid to set it between the accepted bounds of [0, 1]
					self.neuron_state = (convolved_potential.permute(0,2,3,4,1) * (1 - torch.sigmoid(self.decay_weight))).permute(0,4,1,2,3)
				else:
					# Add bias to convolved potential 
					output = (convolved_potential + self.bias.view(1, self.out_channels, 1, 1, 1)) if self.bias != None else convolved_potential
					# Decay for next timestep and override the memory (permute to put the C dimension last and multiply each channel/kernel_output by its the decay)
					# Pass the decay weight through a sigmoid to set it between the accepted bounds of [0, 1]
					self.neuron_state = (convolved_potential.permute(0,2,3,4,1) * (1 - torch.sigmoid(self.decay_weight))).permute(0,4,1,2,3)
				
			else: # t != 0
				# Sum of the spatially convolved input at t and the decayed state of t-1 (pre-adding bias)
				pre_bias = convolved_potential + self.neuron_state 
				
				if mask != None: # If mask present, do re-weighting
					# re_scaled_potential, new_mask = self.re_weighting_streaming(pre_bias, mask)
					# Output is the sum of the convolved potential, current neuron state and bias         
					output = (pre_bias + self.bias.view(1, self.out_channels, 1, 1, 1)) if self.bias != None else pre_bias
					# Decay for next timestep and override the memory (permute to put the C dimension last and multiply each channel/kernel_output by its the decay)
					# Pass the decay weight through a sigmoid to set it between the accepted bounds of [0, 1]
					self.neuron_state = (pre_bias.permute(0,2,3,4,1) * (1 - torch.sigmoid(self.decay_weight))).permute(0,4,1,2,3)
				else:
					# Output is the sum of the convolved potential, current neuron state and bias         
					output = (pre_bias + self.bias.view(1, self.out_channels, 1, 1, 1)) if self.bias != None else pre_bias
					# Decay for next timestep and override the memory (permute to put the C dimension last and multiply each channel/kernel_output by its the decay)
					# Pass the decay weight through a sigmoid to set it between the accepted bounds of [0, 1]
					self.neuron_state = (pre_bias.permute(0,2,3,4,1) * (1 - torch.sigmoid(self.decay_weight))).permute(0,4,1,2,3)

		return output, mask if mask != None else None # If no mask in the input then no mask in the output. If running on dense mode the correct output is convolved_potential

	
	



#--------------------------------------------------------------------------------------------------------------------------------------------------------------------
#--------------------------------------------------------------------------------------------------------------------------------------------------------------------
#-----------------------------------------------------------------Celyn's code------------------------------------------------------------------------
#--------------------------------------------------------------------------------------------------------------------------------------------------------------------


# ==================================================================================================
class Decay3d(torch.nn.modules.Module):
	# https://towardsdatascience.com/how-to-build-your-own-pytorch-neural-network-layer-from-scratch-842144d623f6
	# ----------------------------------------------------------------------------------------------
	def __init__(self, in_channels: int, out_channels: int, kernel_size: int or  tuple(int, int) = 3,
			stride: int or tuple(int, int, int) = 1, padding: int or tuple(int, int, int) = 0,
			bias: bool = True, spatial: int or tuple(int, int) = 1):
		super().__init__()
		self.in_channels = in_channels
		self.out_channels = out_channels
		self.kernel_size = kernel_size if isinstance(kernel_size, collections.abc.Iterable) else (1, kernel_size, kernel_size)
		self.stride = stride if isinstance(stride, collections.abc.Iterable) else (1, stride, stride)
		self.padding = padding if isinstance(padding, collections.abc.Iterable) else (0, padding, padding)
		assert self.kernel_size[0] == 1
		assert self.stride[0] == 1
		if bias:
			self.bias = torch.nn.Parameter(torch.Tensor(out_channels))
		else:
			self.register_parameter("bias", None)

		self.conv_weight = torch.nn.Parameter(torch.Tensor(out_channels, in_channels, *self.kernel_size))
		self.decay_weight = torch.nn.Parameter(torch.Tensor(out_channels))

		self.reset_parameters()

	# ----------------------------------------------------------------------------------------------
	def reset_parameters(self):
		torch.nn.init.kaiming_uniform_(self.conv_weight, a=math.sqrt(5))
		torch.nn.init.kaiming_uniform_(self.decay_weight.unsqueeze(0), a=math.sqrt(5))
		# TODO fan_in or fan_out? based on which weights?
		# https://towardsdatascience.com/understand-kaiming-initialization-and-implementation-detail-in-pytorch-f7aa967e9138
		if self.bias is not None:
			fan_in, _ = torch.nn.init._calculate_fan_in_and_fan_out(self.conv_weight)
			bound = 1 / math.sqrt(fan_in)
			torch.nn.init.uniform_(self.bias, -bound, bound)

	# ----------------------------------------------------------------------------------------------
	def extra_repr(self):
		return f"in_channels={self.in_channels}, out_channels={self.out_channels}, bias={self.bias is not None}"

	# ----------------------------------------------------------------------------------------------
	def forward(self, input: torch.Tensor) -> torch.Tensor:
		assert len(input.shape) == 5 # NCDHW
		# First, propagate values from previous layer using 2D convolution
		# Use first part of weights
		output = F.conv3d(input, self.conv_weight, bias=None, stride=self.stride, padding=self.padding)

		# Now, propagate decay values from resulting tensor
		output = output.permute(2, 0, 1, 3, 4) # NCDHW -> DNCHW
		# Combine signals from previous layers, and residual signals from current layer but earlier in time
		# TODO add self.bias back in
		# TODO condense and optimise this
		# Only want positive values for the decay factor
		# decay = self.decay_weight.exp()
		decay = self.decay_weight / (1 + abs(self.decay_weight))

		for i, d in enumerate(output):
			if i == 0:
				continue
			output[i] = output[i].clone() + output[i - 1].clone() * decay
		# multiplier = torch.zeros_like(output)
		# multiplier[1:] = decay
		# output[1:] = output[1:] + output[:-1] * multiplier[:-1]

		output = output.permute(1, 2, 0, 3, 4) # DNCHW -> NCDHW
		if self.bias is not None:
			output = output + self.bias.view(1, self.out_channels, 1, 1, 1)

		return output














# ==================================================================================================
class Decay3dPartialC(Decay3d):
	# ----------------------------------------------------------------------------------------------
	def __init__(self, in_channels: int, out_channels: int, kernel_size: int or tuple(int, int) = 3,
			stride: int or tuple(int, int, int) = 1, padding: int or tuple(int, int, int) = 0,
			bias: bool = True, multi_channel: bool = False, return_mask: bool = True, kernel_ratio: bool = False,
			spatial: int or tuple(int, int) = 1, scale_factor: int= 1):
		super().__init__(in_channels, out_channels, kernel_size, stride, padding, bias, spatial)
		self.multi_channel = multi_channel
		self.return_mask = return_mask
		self.kernel_ratio = kernel_ratio
		self.scale_factor = scale_factor
		if self.multi_channel:
			self.weight_maskUpdater = torch.ones(self.out_channels, self.in_channels, self.kernel_size[0], self.kernel_size[1], self.kernel_size[2])
		else:
			self.weight_maskUpdater = torch.ones(1, 1, self.kernel_size[0], self.kernel_size[1], self.kernel_size[2])
		self.slide_winsize = self.weight_maskUpdater.shape[1] * self.weight_maskUpdater.shape[2] * self.weight_maskUpdater.shape[3] * self.weight_maskUpdater.shape[4]
		self.last_size = (None, None, None, None, None)
		self.update_mask = None
		self.mask_ratio = None

	# ----------------------------------------------------------------------------------------------
	def reset_parameters(self):
		torch.nn.init.kaiming_uniform_(self.conv_weight, a=math.sqrt(5))
		torch.nn.init.kaiming_uniform_(self.decay_weight.unsqueeze(0), a=math.sqrt(5))
		# TODO fan_in or fan_out? based on which weights?
		# https://towardsdatascience.com/understand-kaiming-initialization-and-implementation-detail-in-pytorch-f7aa967e9138
		if self.bias is not None:
			fan_in, _ = torch.nn.init._calculate_fan_in_and_fan_out(self.conv_weight)
			bound = 1 / math.sqrt(fan_in)
			torch.nn.init.uniform_(self.bias, -bound, bound)

	# ----------------------------------------------------------------------------------------------
	def forward(self, input: torch.Tensor, mask_in: torch.Tensor = None):
		# First, propagate values from previous layer using 2D convolution
		# Use first part of weights
		# Partial convolutions
		assert len(input.shape) == 5
		if self.scale_factor != 1:
			mask_in = torch.nn.Upsample(scale_factor=self.scale_factor, mode="nearest")(mask_in)
			assert self.scale_factor == (1, 2, 2) # TODO implement for other scale factors..!
			# mask_in = torch.nn.Upsample(scale_factor=self.scale_factor, mode="nearest")(mask_in)[:, :, 1:-1, 1:-1, :]
		if mask_in is not None:
			try:
				assert input.shape == mask_in.shape
			except AssertionError:
				#error(f"Input/mask mismatch in partial convolution: {input.shape} vs. {mask_in.shape}")
				raise

		if mask_in is not None or self.last_size != tuple(input.shape):
			self.last_size = tuple(input.shape)

			with torch.no_grad():
				if self.weight_maskUpdater.type() != input.type():
					self.weight_maskUpdater = self.weight_maskUpdater.to(input)

				if mask_in is None:
					# if mask is not provided, create a mask
					if self.multi_channel:
						mask = torch.ones(input.data.shape[0], input.data.shape[1], input.data.shape[2], input.data.shape[3], input.data.shape[4]).to(input)
					else:
						mask = torch.ones(1, 1, input.data.shape[2], input.data.shape[3], input.data.shape[4]).to(input)
				else:
					mask = mask_in

				self.update_mask = F.conv3d(mask, self.weight_maskUpdater, bias=None, stride=self.stride, padding=self.padding, groups=1)

				if self.kernel_ratio:
					# Normal behaviour:
					# 1. Multiply by self.slide_winsize (the maximum number of cells I could see)
					# 2. Divide by self.update_mask (the number of cells I can see; not masked)
					# Instead:
					# 1. Multiply by sum of kernel
					# 2. Then divide by sum of unmasked kernel
					ratio = F.conv3d(torch.ones_like(input) * mask, self.conv_weight, bias=None, stride=self.stride, padding=self.padding, groups=1)
					# For mixed precision training, change 1e-8 to 1e-6 (???)
					print(self.conv_weight.shape)
					print( self.conv_weight.sum() )
					self.mask_ratio = self.conv_weight.sum() / (ratio + 1e-8)
					self.update_mask = torch.clamp(self.update_mask, 0, 1)
				else:
					# For mixed precision training, change 1e-8 to 1e-6
					self.mask_ratio = self.slide_winsize / (self.update_mask + 1e-8)
					self.update_mask = torch.clamp(self.update_mask, 0, 1)
					self.mask_ratio = self.mask_ratio * self.update_mask

		raw_out = F.conv3d(input, self.conv_weight, self.bias, self.stride, self.padding)

		if self.bias is not None:
			bias_view = self.bias.view(1, self.out_channels, 1, 1, 1)
			output = ((raw_out - bias_view) * self.mask_ratio) + bias_view
			output = output * self.update_mask
		else:
			output = raw_out * self.mask_ratio

		# Now, propagate decay values from resulting tensor
		output = output.permute(2, 0, 1, 3, 4) # NCDHW -> DNCHW
		# Combine signals from previous layers, and residual signals from current layer but earlier in time
		# TODO add self.bias back in
		# TODO condense and optimise this
		# Only want positive values for the decay factor

		decay = self.decay_weight / (1 + abs(self.decay_weight))
		
		#print(decay.shape)
		for i, d in enumerate(output):
			if i == 0:
				continue
			#print(output[i - 1].clone().shape)
			#print(decay.view(1, self.out_channels,1,1,1).shape)
			
			#print(torch.mul(output[i - 1].clone() ,decay))
			output[i] = output[i].clone() + (output[i - 1].clone().permute(0,2,3,1) * decay).permute(0,3,1,2)
		# multiplier = torch.zeros_like(output)
		# multiplier[1:] = decay
		# output[1:] = output[1:] + output[:-1] * multiplier[:-1]

		output = output.permute(1, 2, 0, 3, 4) # DNCHW -> NCDHW
		if self.bias is not None:
			output = output + self.bias.view(1, self.out_channels, 1, 1, 1)

		if self.return_mask:
			return output, self.update_mask
		else:
			return output

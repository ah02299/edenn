############################################################################################################
#                                                                                                           
# - Pytorch dataset classes to use with DSEC event sequences.
#   Written or adapted by Alejandro Hernández Díaz
#
#   Link: https://github.com/uzh-rpg/DSEC/tree/main
#
############################################################################################################
import math
from pathlib import Path
from typing import Dict, Tuple
import weakref

import cv2
import h5py
from numba import jit
import numpy as np
import torch
from torch.utils.data import Dataset, DataLoader
from matplotlib import pyplot as plt
import os
import imageio
from .utils import VoxelGrid, EventSlicer, flow_16bit_to_float
from random import randint
from torchvision.transforms.functional import crop

#TODO:
#   - Clean docstrings for dataset classes and directory structure 
#   - Look into applying the found augmentation techniques (see notion).


# ==================================================================================================
# ==================================================================================================
# Simple class for a single sequence of the DSEC dataset (during training, all sequences will be loaded and concatenated into one). 
# In the case of E-RAFT (where this code originally comes from), the authors separate the event stream in between 
# flow GTs in two consecutive subsequences. In this case we will take the full event stream between flow GT to feed into 
# the EDENN arch as a base case.
class SequenceEDENN(Dataset):
    def __init__(self, root: Path, seq_name: str, mode: str='train', delta_t_ms: int=100,
                 num_bins: int=5, crop_window=None, normalize: bool=True):
        
        """Dataset class to load one single sequence from the DSEC collection. As we have multiple 
        flow GTs for each sequence, the common practice is to train/test on subsequences between GT 
        'readings'. For now it will only read forward flow. 
        Parameters
        ----------
        seq_path:    
        ts_end_us:      end time in microseconds
        """
        assert num_bins >= 1
        assert delta_t_ms == 100
        assert root.is_dir()
        assert mode in {'train', 'test', 'val'}

        '''
        Directory Structure:

        Dataset
        |── train_events
        |   ├── thun_00_a
        |   │   ├── events_left
        |   │   │   ├── events.h5
        |   │   │   └── rectify_map.h5
        |   │   ├── image_timestamps.txt
        |   │   └── test_forward_flow_timestamps.csv
        |   train_optical_flow
        |   ├── thun_00_a
        |       ├── flow
        |           ├── backward
        |           |   └── rectify_map.h5
        |           ├── backward_timestamps.txt
        |           ├── forward_timestamps.txt
        |           └── forward
        '''

        # TODO: once I add more transforms change this to be more general but for now it'll do
        self.crop_window = crop_window

        self.mode = mode
        self.split = "train" # maybe change this??
        # Get Timestamp File. example path is -> /vol/research/datasets/mixedmode/DSEC/train_optical_flow/thun_00_a/flow/forward_timestamps.txt
        self.flow_root = os.path.join(root, self.split + "_optical_flow",seq_name , 'flow')
        flow_timestamp_file = os.path.join(self.flow_root, 'forward_timestamps.txt')

        # Load txt timestamps file. It contains the subsequences timestamps.
        self.flow_timestamps = np.genfromtxt(
            flow_timestamp_file,
            delimiter=','
        )

        # Save output dimensions
        self.height = 480
        self.width = 640
        self.num_bins = num_bins

        # Just for now, we always train with num_bins=15 
        #assert self.num_bins==15

        # Set event representation
        self.voxel_grid = VoxelGrid((self.num_bins, self.height, self.width), normalize=normalize)

        # Save delta timestamp in ms (multiply by 1000 to convert between them) delta timestamp is the subsequence length
        self.delta_t_us = delta_t_ms * 1000

        #Load and compute timestamps and indices
        # #timestamps_images = np.loadtxt(os.path.join(root,'images', 'timestamps.txt'), dtype='int64')
        # timestamps_images = np.loadtxt(os.path.join(root, mode + "_images", seq_name ,'images', 'timestamps.txt'), dtype='int64')
        # image_indices = np.arange(len(timestamps_images))
        # # But only use every second one because we train at 10 Hz, and we leave away the 1st & last one
        # # self.timestamps_flow = timestamps_images[::2][1:-1] # Commented out from original E-RAFT
        # self.indices = image_indices[::2][1:-1]

        # Left events only (for now I guess?)
        #ev_dir_location = os.path.join(root, 'events/left')
        ev_dir_location = os.path.join(root, self.split + "_events", seq_name , 'events/left')
        ev_data_file = os.path.join(ev_dir_location, 'events.h5')
        ev_rect_file = os.path.join(ev_dir_location, 'rectify_map.h5') 

        # Open event stream file and load it
        h5f_location = h5py.File(str(ev_data_file), 'r')
        self.h5f = h5f_location
        self.event_slicer = EventSlicer(h5f_location)

        # Open rectify map and save as class variable to use later 
        with h5py.File(str(ev_rect_file), 'r') as h5_rect:
            self.rectify_ev_map = h5_rect['rectify_map'][()]

        # Load flow GT filenames and sort them numerically to match event volumes
        self.flow_names = [os.path.join(root, name) for root, dirs, names in os.walk(os.path.join(root, self.split + "_optical_flow/" + seq_name +"/flow/forward")) for name in names]
        self.flow_names.sort()

        # Idk what this does tbf
        self._finalizer = weakref.finalize(self, self.close_callback, self.h5f)




    # ----------------------------------------------------------------------------------------------
    def events_to_voxel_grid(self, p, t, x, y, device: str='cpu'):
        t = (t - t[0]).astype('float32')
        t = (t/t[-1])
        x = x.astype('float32')
        y = y.astype('float32')
        pol = p.astype('float32')
        event_data_torch = {
            'p': torch.from_numpy(pol),
            't': torch.from_numpy(t),
            'x': torch.from_numpy(x),
            'y': torch.from_numpy(y),
        }
        return self.voxel_grid.convert(event_data_torch)
    
    # ----------------------------------------------------------------------------------------------
    def getHeightAndWidth(self):
        return self.height, self.width

    # ----------------------------------------------------------------------------------------------
    @staticmethod
    def get_disparity_map(filepath: Path):
        assert filepath.is_file()
        disp_16bit = cv2.imread(str(filepath), cv2.IMREAD_ANYDEPTH)
        return disp_16bit.astype('float32')/256
    
    # ----------------------------------------------------------------------------------------------
    @staticmethod
    def load_flow(flowfile: Path):
        assert flowfile.exists()
        assert flowfile.suffix == '.png'
        flow_16bit = imageio.imread(str(flowfile), format='PNG-FI')
        flow, valid2D = flow_16bit_to_float(flow_16bit)
        return torch.tensor(flow), torch.tensor(valid2D)
    
    # ----------------------------------------------------------------------------------------------
    @staticmethod
    def close_callback(h5f):
        h5f.close()

    # ----------------------------------------------------------------------------------------------
    def get_image_width_height(self):
        return self.height, self.width
    
    # ----------------------------------------------------------------------------------------------
    def __len__(self):
        return len(self.flow_timestamps)
    
    # ----------------------------------------------------------------------------------------------
    def rectify_events(self, x: np.ndarray, y: np.ndarray):
        # assert location in self.locations
        # From distorted to undistorted
        rectify_map = self.rectify_ev_map
        assert rectify_map.shape == (self.height, self.width, 2), rectify_map.shape
        assert x.max() < self.width
        assert y.max() < self.height
        return rectify_map[y, x]
    
    # ----------------------------------------------------------------------------------------------
    def get_data_sample(self, index, crop_window=None, flip=None):
        # Get full event stream in between flow GT readings.

        ts_start = self.flow_timestamps[index][0]
        ts_end =  self.flow_timestamps[index][1]

        # Get specific event slice
        event_data = self.event_slicer.get_events(ts_start, ts_end)

        p = event_data['p']
        t = event_data['t']
        x = event_data['x']
        y = event_data['y']

        # Rectify the events to undistort them.
        xy_rect = self.rectify_events(x, y)
        x_rect = xy_rect[:, 0]
        y_rect = xy_rect[:, 1]

        # I guess this is like getting a center crop of the event volume? I'll do my own crop with torchvision bc I guess it works just fine
        # if crop_window is not None:
        #     # Cropping (+- 2 for safety reasons)
        #     x_mask = (x_rect >= crop_window['start_x']-2) & (x_rect < crop_window['start_x']+crop_window['crop_width']+2)
        #     y_mask = (y_rect >= crop_window['start_y']-2) & (y_rect < crop_window['start_y']+crop_window['crop_height']+2)
        #     mask_combined = x_mask & y_mask
        #     p = p[mask_combined]
        #     t = t[mask_combined]
        #     x_rect = x_rect[mask_combined]
        #     y_rect = y_rect[mask_combined]

        if self.voxel_grid is None:
            raise NotImplementedError
        else:
            # Convert events to voxel grid
            event_representation = self.events_to_voxel_grid(p, t, x_rect, y_rect)
            flow_GT, Valid2D = self.load_flow(Path(self.flow_names[index]))
        
        if crop_window is not None: # Lets expect crop window as a tuple of (H, W)
            event_representation, flow_GT, Valid2D = self.get_random_crop(event_representation, flow_GT, Valid2D, crop_window[0], crop_window[1])

        return event_representation, (flow_GT, Valid2D)
    
    # ----------------------------------------------------------------------------------------------
    def get_random_crop(self, voxel_grid: torch.tensor, flow_GT: torch.tensor, valid2D: torch.tensor, height: int, width: int):
        # Get a valid random crop of the chosen dimensions 
        assert height <= self.height and width <= self.width # Check that the random crop chosen is smaller than the data

        # Get max x and y coordinates for the crop 
        max_x = self.height - height
        max_y = self.width - width

        if self.mode != "train":
            x_left = max_x // 2
            y_left = max_y// 2
        else:
            # Get x and y coordinates of left top corner of crop 
            x_left = randint(0, max_x)
            y_left = randint(0, max_y)

        # get the same crop for voxels, flow and valid mask
        voxel_grid = crop(voxel_grid, y_left, x_left, height, width)
        valid2D = crop(valid2D, y_left, x_left, height, width)
        # flow GT is in the form of [H, W, C] so permute twice to match crop expectations, then return to normal.
        flow_GT = crop(flow_GT.permute(2,0,1), y_left, x_left, height, width).permute(1,2,0)


        return voxel_grid, flow_GT, valid2D

    # ----------------------------------------------------------------------------------------------
    def __getitem__(self, idx):
        # Sample contains ground truth flow if trainning, else None
        data = self.get_data_sample(idx, self.crop_window) # Return voxel_grid, (flow_gt, valid2D)
        sample = {
            "events": data[0],
            "flow": data[1]
        }
        return sample









# ==================================================================================================
# ==================================================================================================
# Dataset class for a single sequence of the DSEC dataset, Adapted and commented from E-RAFT. In this
# case, each event sequence between flow readings is separated into two smaller subsequences (original E-RAFT)
class Sequence(Dataset):
    def __init__(self, root: Path, seq_name: str, mode: str='test', delta_t_ms: int=100,
                 num_bins: int=15, transforms=None, name_idx=0, visualize=False, normalize: bool=True):
        
        """Dataset class to load one single sequence from the DSEC collection. As we have multiple 
        flow GTs for each sequence, the common practice is to train/test on subsequences between GT 
        'readings'. For now it will only read forward flow. 
        Parameters
        ----------
        seq_path:    
        ts_end_us:      end time in microseconds
        """
        assert num_bins >= 1
        assert delta_t_ms == 100
        assert root.is_dir()
        assert mode in {'train', 'test'}

        '''
        Directory Structure:

        Dataset
        |── train_events
        |   ├── thun_00_a
        |   │   ├── events_left
            │   │   ├── events.h5
            │   │   └── rectify_map.h5
            │   ├── image_timestamps.txt
            │   └── test_forward_flow_timestamps.csv
            train_optical_flow
            ├── thun_00_a
                ├── flow
                    ├── backward
                    |   └── rectify_map.h5
                    ├── backward_timestamps.txt
                    ├── forward_timestamps.txt
                    └── forward
        '''

        self.mode = mode
        self.name_idx = name_idx
        self.visualize_samples = visualize
        # Get Timestamp File. example path is -> /vol/research/datasets/mixedmode/DSEC/train_optical_flow/thun_00_a/flow/forward_timestamps.txt
        #flow_root = os.path.join(root, mode + "optical_flow",seq_name , 'flow')
        #flow_timestamp_file = os.path.join(flow_root, 'forward_timestamps.txt')
        flow_timestamp_file = Path(os.path.join(root, 'flow/forward_timestamps.txt'))
        #assert flow_timestamp_file.is_file()
        # Load txt timestamps file. It contains the subsequences timestamps.
        self.flow_timestamps = np.genfromtxt(
            flow_timestamp_file,
            delimiter=','
        )

        #self.idx_to_visualize = file[:,2] # Idk what this is for yet

        # Save output dimensions
        self.height = 480
        self.width = 640
        self.num_bins = num_bins

        # TODO: change this??
        # Just for now, we always train with num_bins=15 
        assert self.num_bins==15

        # Set event representation

        self.voxel_grid = VoxelGrid((self.num_bins, self.height, self.width), normalize=normalize)


        # Save delta timestamp in ms (multiply by 1000 to convert between them) delta timestamp is the subsequence length
        self.delta_t_us = delta_t_ms * 1000

        #Load and compute timestamps and indices
        timestamps_images = np.loadtxt(os.path.join(root,'images', 'timestamps.txt'), dtype='int64')
        #timestamps_images = np.loadtxt(os.path.join(root, mode + "images", seq_name ,'images', 'timestamps.txt'), dtype='int64')
        image_indices = np.arange(len(timestamps_images))
        # But only use every second one because we train at 10 Hz, and we leave away the 1st & last one
        self.timestamps_flow = timestamps_images[::2][1:-1]
        self.indices = image_indices[::2][1:-1]

        # Left events only (for now I guess?)
        ev_dir_location = os.path.join(root, 'events/left')
        #ev_dir_location = os.path.join(root, mode + "events", seq_name , 'events/left')
        ev_data_file = ev_dir_location / 'events.h5'
        ev_rect_file = ev_dir_location / 'rectify_map.h5'

        h5f_location = h5py.File(str(ev_data_file), 'r')
        self.h5f = h5f_location
        self.event_slicer = EventSlicer(h5f_location)

        # Open rectify map and save as class variable to use later 
        with h5py.File(str(ev_rect_file), 'r') as h5_rect:
            self.rectify_ev_map = h5_rect['rectify_map'][()]
        # Idk what this does tbf
        self._finalizer = weakref.finalize(self, self.close_callback, self.h5f)

    # ----------------------------------------------------------------------------------------------
    def events_to_voxel_grid(self, p, t, x, y, device: str='cpu'):
        t = (t - t[0]).astype('float32')
        t = (t/t[-1])
        x = x.astype('float32')
        y = y.astype('float32')
        pol = p.astype('float32')
        event_data_torch = {
            'p': torch.from_numpy(pol),
            't': torch.from_numpy(t),
            'x': torch.from_numpy(x),
            'y': torch.from_numpy(y),
        }
        return self.voxel_grid.convert(event_data_torch)
    
    # ----------------------------------------------------------------------------------------------
    def getHeightAndWidth(self):
        return self.height, self.width

    # ----------------------------------------------------------------------------------------------
    @staticmethod
    def get_disparity_map(filepath: Path):
        assert filepath.is_file()
        disp_16bit = cv2.imread(str(filepath), cv2.IMREAD_ANYDEPTH)
        return disp_16bit.astype('float32')/256
    
    # ----------------------------------------------------------------------------------------------
    @staticmethod
    def load_flow(flowfile: Path):
        assert flowfile.exists()
        assert flowfile.suffix == '.png'
        flow_16bit = imageio.imread(str(flowfile), format='PNG-FI')
        flow, valid2D = flow_16bit_to_float(flow_16bit)
        return flow, valid2D
    
    # ----------------------------------------------------------------------------------------------
    @staticmethod
    def close_callback(h5f):
        h5f.close()

    # ----------------------------------------------------------------------------------------------
    def get_image_width_height(self):
        return self.height, self.width
    
    # ----------------------------------------------------------------------------------------------
    def __len__(self):
        return len(self.timestamps_flow)
    
    # ----------------------------------------------------------------------------------------------
    def rectify_events(self, x: np.ndarray, y: np.ndarray):
        # assert location in self.locations
        # From distorted to undistorted
        rectify_map = self.rectify_ev_map
        assert rectify_map.shape == (self.height, self.width, 2), rectify_map.shape
        assert x.max() < self.width
        assert y.max() < self.height
        return rectify_map[y, x]
    
    # ----------------------------------------------------------------------------------------------
    def get_data_sample(self, index, crop_window=None, flip=None):
        # First entry corresponds to all events BEFORE the flow map
        # Second entry corresponds to all events AFTER the flow map (corresponding to the actual fwd flow)
        names = ['event_volume_old', 'event_volume_new']
        ts_start = [self.timestamps_flow[index] - self.delta_t_us, self.timestamps_flow[index]]
        ts_end = [self.timestamps_flow[index], self.timestamps_flow[index] + self.delta_t_us]

        file_index = self.indices[index]

        output = {
            'file_index': file_index,
            'timestamp': self.timestamps_flow[index]
        }
        # Save sample for benchmark submission
        output['save_submission'] = file_index in self.idx_to_visualize
        output['visualize'] = self.visualize_samples

        for i in range(len(names)):
            event_data = self.event_slicer.get_events(ts_start[i], ts_end[i])

            p = event_data['p']
            t = event_data['t']
            x = event_data['x']
            y = event_data['y']

            xy_rect = self.rectify_events(x, y)
            x_rect = xy_rect[:, 0]
            y_rect = xy_rect[:, 1]

            if crop_window is not None:
                # Cropping (+- 2 for safety reasons)
                x_mask = (x_rect >= crop_window['start_x']-2) & (x_rect < crop_window['start_x']+crop_window['crop_width']+2)
                y_mask = (y_rect >= crop_window['start_y']-2) & (y_rect < crop_window['start_y']+crop_window['crop_height']+2)
                mask_combined = x_mask & y_mask
                p = p[mask_combined]
                t = t[mask_combined]
                x_rect = x_rect[mask_combined]
                y_rect = y_rect[mask_combined]

            if self.voxel_grid is None:
                raise NotImplementedError
            else:
                event_representation = self.events_to_voxel_grid(p, t, x_rect, y_rect)
                output[names[i]] = event_representation
            output['name_map']=self.name_idx
        return output

    # ----------------------------------------------------------------------------------------------
    def __getitem__(self, idx):
        sample =  self.get_data_sample(idx)
        return sample






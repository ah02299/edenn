############################################################################################################
#                                                                                                           
# - Utility functions to load and pre-process raw event-based data. Adapted from multiple sources.
#   
############################################################################################################

import math
from pathlib import Path
from typing import Dict, Tuple
import weakref

import cv2
import h5py
from numba import jit
import numpy as np
import torch
from torch.utils.data import Dataset, DataLoader#
from matplotlib import pyplot as plt
import os
import imageio


# ==================================================================================================
# Adapted from https://github.com/uzh-rpg/snn_angular_velocity
class SpikeRepresentationGenerator:
	# ----------------------------------------------------------------------------------------------
	def __init__(self, width: int, height: int, num_time_bins: int):
		"""
		Class to generate spikes from event tensors.

		Args:
			width (int): Width of event image
			height (int): Height of event image
			num_time_bins (int): Number of time bins within the window
		"""
		self.width = width
		self.height = height
		self.num_time_bins = num_time_bins

	# ----------------------------------------------------------------------------------------------
	def getSlayerSpikeTensor(self, polarities: torch.Tensor, locations: torch.Tensor,
			timestamps: torch.Tensor) -> torch.Tensor:
		"""
		Generate spikes from event tensors.
		All arguments must be of the same image shape.

		Args:
			polarities (torch.Tensor): Event polarities
			locations (torch.Tensor): Event XY coordinates
			timestamps (torch.Tensor): Event timestamps in microseconds

		Returns:
			torch.Tensor: Spike train tensor
		"""
		spike_tensor = torch.zeros((2, self.height, self.width, self.num_time_bins))
		if len(timestamps) < 2:
			return spike_tensor # Empty tensor, don't raise an error

		binDuration = (timestamps[-1] - timestamps[0]) / self.num_time_bins
		if binDuration == 0:
			return spike_tensor
		time_idx = ((timestamps - timestamps[0]) / binDuration) # Assumed that events are sorted ascending!
		# Handle time stamps that are not floored and would exceed the allowed index after to-index conversion
		time_idx[time_idx >= self.num_time_bins] = self.num_time_bins - 1

		spike_tensor[polarities.long(), locations[:, 1].long(), locations[:, 0].long(), time_idx.long()] = 1

		return spike_tensor
     

# ==================================================================================================
# Code adapted and commented from the DSEC codebase for better understanding: https://dsec.ifi.uzh.ch
class EventSlicer:
    def __init__(self, h5f: h5py.File):
        self.h5f = h5f # sequence file 

        self.events = dict()
        for dset_str in ['p', 'x', 'y', 't']: # "unpack" the event data -> polarity, x, y, and timestamps (in microseconds?)
            self.events[dset_str] = self.h5f['events/{}'.format(dset_str)]

        # This is the mapping from milliseconds to event index:
        # It is defined such that
        # (1) t[ms_to_idx[ms]] >= ms*1000
        # (2) t[ms_to_idx[ms] - 1] < ms*1000
        # ,where 'ms' is the time in milliseconds and 't' the event timestamps in microseconds.
        #
        # As an example, given 't' and 'ms':
        # t:    0     500    2100    5000    5000    7100    7200    7200    8100    9000
        # ms:   0       1       2       3       4       5       6       7       8       9
        #
        # we get
        #
        # ms_to_idx:
        #       0       2       2       3       3       3       5       5       8       9
        self.ms_to_idx = np.asarray(self.h5f['ms_to_idx'], dtype='int64') # Each element in this array is the index of the t array corresponding to the start of each 1ms event group. i.e. if ms_to_idx[1] = 40000, it means that all the events from t = 0  to t = 1ms are in between the 0 and 40000 index.

        self.t_offset = int(h5f['t_offset'][()]) # temporal offset to add to the event timestamps in microseconds so that they are in the same clock as the image timestamps
        self.t_final = int(self.events['t'][-1]) + self.t_offset # final timestamp of the sequence in microsecs?

	# ----------------------------------------------------------------------------------------------
    def get_final_time_us(self):
        return self.t_final
    
	# ----------------------------------------------------------------------------------------------
    def get_events(self, t_start_us: int, t_end_us: int) -> Dict[str, np.ndarray]:
        """Get events (p, x, y, t) within the specified time window
        Parameters
        ----------
        t_start_us: start time in microseconds
        t_end_us: end time in microseconds
        Returns
        -------
        events: dictionary of (p, x, y, t) or None if the time window cannot be retrieved
        """
        assert t_start_us < t_end_us

        # We assume that the times are top-off-day, hence subtract offset:
        t_start_us -= self.t_offset
        t_end_us -= self.t_offset


        # Convert from microseconds to miliseconds for the time window
        t_start_ms, t_end_ms = self.get_conservative_window_ms(t_start_us, t_end_us)
        # Get the idx for the start and end events of the chosen time window
        t_start_ms_idx = self.ms2idx(t_start_ms)
        t_end_ms_idx = self.ms2idx(t_end_ms)

        if t_start_ms_idx is None or t_end_ms_idx is None:
            # Cannot guarantee window size anymore
            return None

        # Dictionary object to hold the events
        events = dict()
        # Get the timesteps for the chosen window
        time_array_conservative = np.asarray(self.events['t'][t_start_ms_idx:t_end_ms_idx])
        idx_start_offset, idx_end_offset = self.get_time_indices_offsets(time_array_conservative, t_start_us, t_end_us) # This deals with corner cases?
        t_start_us_idx = t_start_ms_idx + idx_start_offset # Adding the offset to get gps time
        t_end_us_idx = t_start_ms_idx + idx_end_offset
        # Again add t_offset to get gps time
        events['t'] = time_array_conservative[idx_start_offset:idx_end_offset] + self.t_offset
        # Getting p, x and y for the time window selected
        for dset_str in ['p', 'x', 'y']: 
            events[dset_str] = np.asarray(self.events[dset_str][t_start_us_idx:t_end_us_idx])
            assert events[dset_str].size == events['t'].size
        return events

	# ----------------------------------------------------------------------------------------------
    @staticmethod
    def get_conservative_window_ms(ts_start_us: int, ts_end_us) -> Tuple[int, int]:
        """Compute a conservative time window of time with millisecond resolution.
        We have a time to index mapping for each millisecond. Hence, we need
        to compute the lower and upper millisecond to retrieve events.
        Parameters
        ----------
        ts_start_us:    start time in microseconds
        ts_end_us:      end time in microseconds
        Returns
        -------
        window_start_ms:    conservative start time in milliseconds
        window_end_ms:      conservative end time in milliseconds
        """
        assert ts_end_us > ts_start_us
        window_start_ms = math.floor(ts_start_us/1000)
        window_end_ms = math.ceil(ts_end_us/1000)
        return window_start_ms, window_end_ms
    
	# ----------------------------------------------------------------------------------------------
    @staticmethod
    @jit(nopython=True)
    def get_time_indices_offsets(
            time_array: np.ndarray,
            time_start_us: int,
            time_end_us: int) -> Tuple[int, int]:
        """Compute index offset of start and end timestamps in microseconds
        Parameters
        ----------
        time_array:     timestamps (in us) of the events
        time_start_us:  start timestamp (in us)
        time_end_us:    end timestamp (in us)
        Returns
        -------
        idx_start:  Index within this array corresponding to time_start_us
        idx_end:    Index within this array corresponding to time_end_us
        such that (in non-edge cases)
        time_array[idx_start] >= time_start_us
        time_array[idx_end] >= time_end_us
        time_array[idx_start - 1] < time_start_us
        time_array[idx_end - 1] < time_end_us
        this means that
        time_start_us <= time_array[idx_start:idx_end] < time_end_us
        """

        assert time_array.ndim == 1

        idx_start = -1
        if time_array[-1] < time_start_us:
            # This can happen in extreme corner cases. E.g.
            # time_array[0] = 1016
            # time_array[-1] = 1984
            # time_start_us = 1990
            # time_end_us = 2000

            # Return same index twice: array[x:x] is empty.
            return time_array.size, time_array.size
        else:
            for idx_from_start in range(0, time_array.size, 1):
                if time_array[idx_from_start] >= time_start_us:
                    idx_start = idx_from_start
                    break
        assert idx_start >= 0

        idx_end = time_array.size
        for idx_from_end in range(time_array.size - 1, -1, -1):
            if time_array[idx_from_end] >= time_end_us:
                idx_end = idx_from_end
            else:
                break

        assert time_array[idx_start] >= time_start_us
        if idx_end < time_array.size:
            assert time_array[idx_end] >= time_end_us
        if idx_start > 0:
            assert time_array[idx_start - 1] < time_start_us
        if idx_end > 0:
            assert time_array[idx_end - 1] < time_end_us
        return idx_start, idx_end
    
	# ----------------------------------------------------------------------------------------------
    def ms2idx(self, time_ms: int) -> int:
        # Assert we dont use negative timesteps
        assert time_ms >= 0
        if time_ms >= self.ms_to_idx.size:
            return None
        return self.ms_to_idx[time_ms]





# ==================================================================================================
# utility function from the DSEC codebase: https://dsec.ifi.uzh.ch
# The optical flow GT are in 16bit pngs comprised of 3 channels (flow_x, flow_y, mask). The mask indicates if the flow in that pixel is valid
def flow_16bit_to_float(flow_16bit: np.ndarray):
    # This is necessary, read the DSEC documentation to know why
    assert flow_16bit.dtype == np.uint16
    assert flow_16bit.ndim == 3
    h, w, c = flow_16bit.shape
    assert c == 3

    valid2D = flow_16bit[..., 2] == 1
    assert valid2D.shape == (h, w)
    # ~ means bit inverting
    assert np.all(flow_16bit[~valid2D, -1] == 0)
    valid_map = np.where(valid2D)

    # to actually compute something useful:
    flow_16bit = flow_16bit.astype('float')

    # Only convert to 
    flow_map = np.zeros((h, w, 2))
    flow_map[valid_map[0], valid_map[1], 0] = (flow_16bit[valid_map[0], valid_map[1], 0] - 2 ** 15) / 128
    flow_map[valid_map[0], valid_map[1], 1] = (flow_16bit[valid_map[0], valid_map[1], 1] - 2 ** 15) / 128
    return flow_map, valid2D
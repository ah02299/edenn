############################################################################################################
#                                                                                                           
# - Python classes corresponding to different event representations for the training of ML architectures. 
#   "Adapted from multiple sources."
#   
############################################################################################################

import torch
import numpy as np 


# ==================================================================================================
# ==================================================================================================
# Voxel Grid event representation, Adapted and commented from E-RAFT https://github.com/uzh-rpg/E-RAFT/blob/main/utils/dsec_utils.py
class VoxelGrid():
    def __init__(self, input_size: tuple, normalize: bool):
        assert len(input_size) == 3
        
		# Create voxel grid full of 0s of shape (n_bins, H, W)
        self.voxel_grid = torch.zeros((input_size), dtype=torch.float, requires_grad=False)
        self.nb_channels = input_size[0]
        self.normalize = normalize
        
	# ----------------------------------------------------------------------------------------------
    def convert(self, events):
        C, H, W = self.voxel_grid.shape
        with torch.no_grad():
            # Send voxel grid to the events device
            self.voxel_grid = self.voxel_grid.to(events['p'].device)
            voxel_grid = self.voxel_grid.clone()

			# Quantize the events' timestamps into C number of bins?
            t_norm = events['t']
            t_norm = (C - 1) * (t_norm-t_norm[0]) / (t_norm[-1]-t_norm[0])

            #print(t_norm)

            x0 = events['x'].int()
            y0 = events['y'].int()
            t0 = t_norm.int()

            
            #print(y0)

			# Change polarity of events from 0 and 1 to -1 and 1
            value = 2*events['p']-1

            for xlim in [x0,x0+1]:
                #print(f"xlim: {xlim}")
                for ylim in [y0,y0+1]:
                    for tlim in [t0,t0+1]:

                        mask = (xlim < W) & (xlim >= 0) & (ylim < H) & (ylim >= 0) & (tlim >= 0) & (tlim < self.nb_channels)
                        interp_weights = value * (1 - (xlim-events['x']).abs()) * (1 - (ylim-events['y']).abs()) * (1 - (tlim - t_norm).abs())
                        
                        # The put function treats the tensor to fill as a 1D tensor for indexing so get total list of indexes this way.
                        index = H * W * tlim.long() + \
                                W * ylim.long() + \
                                xlim.long()
                        
                        # This function accumulates the valid values (interp_weights) into their corresponding valid indexes of voxel_grid
                        voxel_grid.put_(index[mask], interp_weights[mask], accumulate=True)

            # Normalize the volume to have mean of 0 and std of 1
            if self.normalize:
                mask = torch.nonzero(voxel_grid, as_tuple=True)
                if mask[0].size()[0] > 0:
                    mean = voxel_grid[mask].mean()
                    std = voxel_grid[mask].std()
                    if std > 0:
                        voxel_grid[mask] = (voxel_grid[mask] - mean) / std
                    else:
                        voxel_grid[mask] = voxel_grid[mask] - mean

        return voxel_grid


import argparse
from pathlib import Path
from typing import Optional

from torch.utils.data.dataset import Dataset

from edenn.datasets.utils import SpikeRepresentationGenerator
from edenn.utils import Step

# ==================================================================================================
class Base(Dataset):
	width: Optional[int] = None
	height: Optional[int] = None
	input_shape: int
	output_shape: int
	# ----------------------------------------------------------------------------------------------
	def __init__(self, args: argparse.Namespace, step: Step) -> None:
		super().__init__()
		self.args = args
		self.step = step
		self.generator = SpikeRepresentationGenerator(width=self.width, height=self.height, num_time_bins=self.args.n_bins) # type: ignore

	# ----------------------------------------------------------------------------------------------
	@staticmethod
	def add_argparse_args(parser: argparse.ArgumentParser) -> argparse.ArgumentParser:
		"""
		Add model-specific arguments to parser.

		Args:
			parser (argparse.ArgumentParser): Main parser

		Returns:
			argparse.ArgumentParser: Modified parser
		"""
		group = parser.add_argument_group("Dataset")
		group.add_argument("dataset_path", type=Path, help="Dataset directory")
		group.add_argument("-b", "--batch_size", type=int, help="Batch size", default=4)
		group.add_argument("-a", "--batch_accumulation", type=int, help="Perform batch accumulation", default=1)
		group.add_argument("-w", "--workers", type=int, help="Dataset workers (can use 0)", default=12)

		return parser

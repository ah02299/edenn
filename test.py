#!/usr/bin/env python3
import argparse
import colored_traceback.auto
from pathlib import Path
from typing import Optional

import lightning.pytorch as pl
from lightning.pytorch.loggers import WandbLogger
from lightning.pytorch.callbacks import RichProgressBar
import torch
from rich import print

import edenn

# ==================================================================================================
def main():
	parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
	parser.add_argument("name", type=str, help="Name of run")
	parser.add_argument("checkpoint_path", type=Path, help="Path to trained checkpoint (.ckpt)")
	parser.add_argument("--limit_test", type=str, help="Use this test set proportion (float) or batches (int) each epoch (still randomised over entire dataset)", default=None)
	parser.add_argument("--nolog", action="store_true", help="Don't log to wandb")
	args, _ = parser.parse_known_args()
	args.limit_test = edenn.utils.limit_float_int(args.limit_test)
	if args.limit_test is not None:
		print("Warning: limiting test dataset is non-deterministic")
	checkpoint = torch.load(args.checkpoint_path)
	args = edenn.utils.merge_args(checkpoint["hyper_parameters"], args)

	model = init_model(args, args.checkpoint_path)
	trainer = init_trainer(args)

	trainer.test(model)


# ==================================================================================================
def init_model(args: argparse.Namespace, checkpoint_path: Optional[Path] = None) -> pl.LightningModule:
	print("Initialising model... ", end="")
	Model = getattr(edenn.models, args.model)
	model = Model.load_from_checkpoint(checkpoint_path)
	torch.compile(model)
	print("Done")

	return model


# ==================================================================================================
def init_trainer(args: argparse.Namespace) -> pl.Trainer:
	logger = WandbLogger(project="EDeNN",
			name=args.name,
			config=args,
		) if not args.nolog else False
	trainer = pl.Trainer(
		logger=logger,
		callbacks=[RichProgressBar()],
		limit_test_batches=args.limit_test,
	)

	return trainer


# ==================================================================================================
def init_script():
	torch.set_printoptions(precision=16, sci_mode=False)
	torch.set_float32_matmul_precision("medium") # or "high"


# ==================================================================================================
if __name__ == "__main__":
	init_script()
	main()

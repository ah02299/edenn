#!/usr/bin/env python3
"""
NOTE: Needed to use their version of SLAYER included in the repository!
- https://github.com/uzh-rpg/snn_angular_velocity/tree/master/slayerpytorch
Also had to change line 61 in slayer.py to include `.float()` on the return.
Also had to import SLAYER differently in the SNN model file!
"""
import argparse
import colored_traceback.auto
from pathlib import Path
from typing import Optional

import lightning.pytorch as pl
from lightning.pytorch.loggers import WandbLogger
from lightning.pytorch.callbacks import RichProgressBar
import torch
from rich import print

import edenn.utils
from edenn.models import SNN

# ==================================================================================================
def main():
	parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
	parser.add_argument("name", type=str, help="Name of run")
	parser.add_argument("checkpoint_path", type=Path, help="Path to provided checkpoint (cnn5-avgp-fc1.pt)")
	parser.add_argument("dataset_path", type=Path, help="Dataset directory")
	parser.add_argument("--limit_test", type=str, help="Use this test set proportion (float) or batches (int) each epoch (still randomised over entire dataset)", default=None)
	parser.add_argument("--nolog", action="store_true", help="Don't log to wandb")
	parser.add_argument("-a", "--batch_accumulation", type=int, help="Perform batch accumulation", default=1)
	parser.add_argument("-w", "--workers", type=int, help="Dataset workers (can use 0)", default=12)
	args = parser.parse_args()
	args.limit_test = edenn.utils.limit_float_int(args.limit_test)
	if args.limit_test is not None:
		print("Warning: limiting test dataset is non-deterministic")

	model = init_model(args, args.checkpoint_path)
	trainer = init_trainer(args)

	trainer.test(model)


# ==================================================================================================
def init_model(args: argparse.Namespace, checkpoint_path: Optional[Path] = None) -> pl.LightningModule:
	print("Initialising model... ", end="")
	args.n_bins = 100
	args.window_length = 1e-3 * args.n_bins
	args.loss_ignore_t = 50
	args.batch_size = 8
	model = SNN(args)
	checkpoint = torch.load(checkpoint_path, map_location="cuda")
	model.load_state_dict(checkpoint["model_state_dict"])
	torch.compile(model)
	print("Done")

	return model


# ==================================================================================================
def init_trainer(args: argparse.Namespace) -> pl.Trainer:
	logger = WandbLogger(project="EDeNN",
			name=args.name,
			config=args,
		) if not args.nolog else False
	trainer = pl.Trainer(
		logger=logger,
		callbacks=[RichProgressBar()],
		limit_test_batches=args.limit_test,
	)

	return trainer


# ==================================================================================================
def init_script():
	torch.set_printoptions(precision=16, sci_mode=False)
	torch.set_float32_matmul_precision("medium") # or "high"


# ==================================================================================================
if __name__ == "__main__":
	init_script()
	main()
